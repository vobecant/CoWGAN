import torch
import matplotlib
import sys
from mpl_toolkits.mplot3d import Axes3D

matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
from torchvision import transforms
from models.dataset import GeomDataset3d, GeomDataset
from libs.plot import Plotter
from libs.save import Saver
from timeit import default_timer as timer
from models.CNN import CNN
from torch import nn, optim
from sklearn.cluster import DBSCAN, KMeans
from sklearn.decomposition import TruncatedSVD
from models.types import DATASET_types
import hdbscan

# ======= GENERAL SETTINGS =======
PLOT = True
output_type = 'fc'
out_fname = open('./result/noGAN/log-noGAN_HDBSCAN.txt', 'w')
NUM_CLASSES = 4

# ======= CNN SETTINGS =========
# Device configuration
cuda_available = torch.cuda.is_available()
device = torch.device('cuda' if cuda_available else 'cpu')
cnn_output = 'both'  # 'fc1'
bn_momentum = 0.2
cnn = CNN(num_classes=NUM_CLASSES, bn_momentum=bn_momentum)
cnn.to(device)
BATCH_SIZE = 64
OUTPUT_PATH_CNN = './result/geom3D/noGAN/CNN/'
LR_CNN = 1e-4
N_EPOCHS_CNN = 20
PLOT_FREQ_CNN = 1
SAVE_FREQ_CNN = 1
MAKE_SAVES_CNN = True
# ___ optimizers, losses, etc. ___
optimizer_cnn = optim.Adam(cnn.parameters(), lr=LR_CNN)
criterion_cnn = nn.CrossEntropyLoss()

# ====== DATA FOLDERS ======
IMG_FOLDER = DATASET_types.GEOM_3D_LARGE.value
CSV_SPECIF_TRAIN = 'bb-lb'
CSV_SPECIF_TEST = 'bb-rt'
CSV_TRAIN = './datasets/{}_train.csv'.format(CSV_SPECIF_TRAIN)
CSV_VAL_TRAIN = './datasets/{}_val.csv'.format(CSV_SPECIF_TRAIN)
CSV_TEST = './datasets/{}_train.csv'.format(CSV_SPECIF_TEST)


# ======= MODIFIED PRINT FUNCTION ========
def print(text):
    sys.stdout.write('{}\n'.format(text))
    out_fname.write('{}\n'.format(text))
    out_fname.flush()


# ======= LOADING FUNCTIONS ==========

def load_my_data(path_to_folder, csvfile, batch_size=BATCH_SIZE, shuffle=True, drop_last=True, is3d=False):
    data_transform = transforms.Compose([
        transforms.Resize(64),
        transforms.CenterCrop(64),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
    ])
    if is3d:
        dataset = GeomDataset3d(csvfile, path_to_folder, data_transform)
    else:
        dataset = GeomDataset(csvfile, path_to_folder, data_transform)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=shuffle, num_workers=5,
                                                 drop_last=drop_last, pin_memory=True)
    print('created dataset loader from {}, number of samples: {}, shuffle: {}'
          .format(path_to_folder, len(dataset), shuffle))
    return dataset_loader


# create loaders
TRAIN_LOADER_TRAIN = load_my_data(IMG_FOLDER, CSV_TRAIN, drop_last=False, is3d=True)
VAL_LOADER_TRAIN = load_my_data(IMG_FOLDER, CSV_VAL_TRAIN, drop_last=False, is3d=True)
TRAIN_LOADER_TEST = load_my_data(IMG_FOLDER, CSV_TEST, drop_last=False, is3d=True)


# ======= TRAINING FUNCTIONS ========

def train_cnn_only():
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver = Saver('val_loss', better_value_fnc, OUTPUT_PATH_CNN)
    plotter_cnn = Plotter(OUTPUT_PATH_CNN)
    print('start training CNN only, used device: {}'.format(device))
    optimizer = optimizer_cnn  # optim.Adam(cnn.parameters(), lr=LEARNING_RATE)
    for epoch in range(N_EPOCHS_CNN):
        start_time = timer()
        # print('epoch {}\ttrain start'.format(epoch))
        ep_start = timer()
        # ------ training -------
        train_losses = np.asarray([])
        total_train, correct_train = 0, 0
        cnn.train()
        for i, batch in enumerate(TRAIN_LOADER_TRAIN):
            # if i == 5: break
            # Forward pass
            images = batch[0]
            labels = batch[1].long()
            # print(labels)
            images = images.to(device)
            labels = labels.to(device)
            # print(labels)
            outputs = cnn(images)
            _, predicted = torch.max(outputs.data, 1)

            # forward pass
            loss = criterion_cnn(outputs, labels)
            train_losses = np.append(train_losses, loss.item())

            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            total_train += labels.size(0)
            correct_train += (predicted == labels).sum().item()

        train_acc = 100 * correct_train / total_train
        train_loss = np.mean(train_losses)

        # ------ validation ------
        # print('epoch {}\tvalidation start'.format(epoch))
        val_loss, val_acc = validate_cnn_only(VAL_LOADER_TRAIN)

        # ------ create plots ------
        plotter_cnn.plot('time', timer() - start_time)
        plotter_cnn.plot('train_loss', train_loss)
        plotter_cnn.plot('val_loss', val_loss)
        plotter_cnn.plot('train_acc', train_acc)
        plotter_cnn.plot('val_acc', val_acc)
        if epoch % PLOT_FREQ_CNN == (PLOT_FREQ_CNN - 1):
            plotter_cnn.flush()
        plotter_cnn.tick()

        # ------ save -------
        if MAKE_SAVES_CNN:
            saver.update('cnn', cnn)
            saver.update('cnn_w', cnn.state_dict())
            saver.update('cnn_opt', optimizer.state_dict())
            saver.update('train_loss', train_loss)
            saver.update('val_loss', val_loss)
            saver.update('train_acc', train_acc)
            saver.update('val_acc', val_acc)
            saver.update('epoch', epoch)
            if epoch % SAVE_FREQ_CNN == (SAVE_FREQ_CNN - 1):
                saver.flush()
            saver.tick()

        # ----- print info ------
        print('Epoch [{}/{}], Train Loss: {:.6f}, Train Acc: {:.2f}, Val Loss: {:.6f}, Val Acc: {:.2f}, time: {:.3f}'
              .format(epoch + 1, N_EPOCHS_CNN, train_loss, train_acc, val_loss, val_acc, timer() - ep_start))


def validate_cnn_only(val_loader):
    # switch to evaluate mode
    cnn.eval()
    losses = np.asarray([])
    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(val_loader):
            input = batch[0]
            target = batch[1].long()
            input = input.to(device)
            target = target.to(device)
            # compute output
            output = cnn(input)
            _, predicted = torch.max(output.data, 1)
            loss = criterion_cnn(output, target)
            losses = np.append(losses, loss.cpu().data.numpy())
            total += target.size(0)
            correct += (predicted == target).sum().item()
    val_loss = np.mean(losses)
    val_acc = 100 * correct / total
    return val_loss, val_acc


def validate(train_loader, test_loader, print_info=True):
    train_loss, train_acc = validate_cnn_only(train_loader)
    test_loss, test_acc = validate_cnn_only(test_loader)
    if print_info:
        print('Train set results:')
        print('\tacc:\t{:.5f}'.format(train_acc))
        print('\tloss:\t{:.5f}'.format(train_loss))
        print('Test set results:')
        print('\tacc:\t{:.5f}'.format(test_acc))
        print('\tloss:\t{:.5f}'.format(test_loss))
    return train_loss, train_acc, test_loss, test_acc


def find_clusters(data, alg, n_clusters=None):
    print('Choosen clustering algorithm: {}'.format(alg))
    if alg == 'DBSCAN':
        return find_clusters_DBSCAN(data)
    if alg == 'HDBSCAN':
        return find_clusters_HDBSCAN(data)
    elif alg == 'Kmeans':
        return find_clusters_Kmeans(data, n_clusters)
    else:
        raise Exception('Unknown clustering algorithm')


def find_clusters_HDBSCAN(data):
    clusterer = hdbscan.HDBSCAN()
    clusterer.fit(data)
    labels = clusterer.labels_
    n_clusters = len(np.unique(labels))
    if -1 in labels: n_clusters -= 1
    centers = []
    in_cluster = []
    for cluster in range(n_clusters):
        idx = np.where(labels == cluster)
        data_cluster = data[idx]
        center = np.mean(data_cluster, axis=0)
        centers.append(center)
        in_cluster.append(len(idx[0]))
    without_cluster = len(np.where(labels == -1)[0])
    print('HDBSCAN found {} clusters with {} samples ({} without cluster).'
          .format(n_clusters, in_cluster, without_cluster))
    centers = np.array(centers)
    if len(centers.shape) == 1:
        centers = centers.reshape([-1, 1])
    return centers


def find_clusters_DBSCAN(data):
    '''
    Finds cluster centers in data with DBSCAN
    :param data: given data
    :return: clusters' centers
    '''
    n_samples = len(data)
    data = np.reshape(data, [n_samples, -1])
    print('Data shape for clustering: {}'.format(data.shape))
    db = DBSCAN(metric='l1', eps=10).fit(data)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_
    centers = data[db.core_sample_indices_]

    # Number of clusters in labels, ignoring noise if present.
    n_samples = len(data)
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    print('Number of samples: {}, number of clusters: {}'.format(n_samples, n_clusters_))
    return centers


def find_clusters_Kmeans(data, n_clusters=100):
    kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(data)
    centers = kmeans.cluster_centers_
    return centers


def sample_distance(sample, centers):
    dist = centers - sample
    dist = np.abs(dist)
    dist = np.sum(dist, axis=1)
    closest = np.min(dist)
    return closest


def dataset_distance(data, centers):
    distances = []
    for sample in data:
        dist = sample_distance(sample, centers)
        # print('sample distance: {}({}), distances type: {}'.format(dist, type(dist), type(distances)))
        distances.append(dist)
    mean = np.mean(distances)
    var = np.var(distances)
    min, max = np.min(distances), np.max(distances)
    return distances, mean, var, min, max


def print_dist_info(name, mean, var, min, max):
    print('Statisctics of {} dataset:'.format(name))
    print('\tmean:\t{}'.format(mean))
    print('\tvar:\t{}'.format(var))
    print('\tmin:\t{}'.format(min))
    print('\tmax:\t{}'.format(max))


def get_train_outputs():
    outputs, targets = [], []
    outputs_conv, outputs_fc = [], []
    with torch.no_grad():
        for i, batch in enumerate(TRAIN_LOADER_TRAIN):
            input = batch[0]
            target = batch[1].long()
            targets.extend(target.cpu().data.numpy())
            input = input.to(device)
            target = target.to(device)
            # compute output
            if cnn_output == 'conv':
                output = cnn(input, conv_only=True).cpu().data.numpy()
                outputs.extend(output)
            elif cnn_output == 'fc':
                output = cnn(input, fc1_out=True).cpu().data.numpy()
                outputs.extend(output)
            elif cnn_output == 'both':
                output_conv = cnn(input, conv_only=True).cpu().data.numpy()
                outputs_conv.extend(output_conv)
                output_fc = cnn(input, fc1_out=True).cpu().data.numpy()
                outputs_fc.extend(output_fc)
            else:
                raise Exception('Unknown cnn output type! ({})'.format(cnn_output))
    if cnn_output == 'both':
        outputs_conv = np.array(outputs_conv)
        outputs_fc = np.array(outputs_fc)
        n_samples = len(outputs_conv)
        outputs_conv = np.reshape(outputs_conv, [n_samples, -1])
        outputs_fc = np.reshape(outputs_fc, [n_samples, -1])
        np.save('./result/noGAN/train_outputs_conv_{}'.format(CSV_SPECIF_TRAIN), outputs_conv)
        np.save('./result/noGAN/train_outputs_fc_{}'.format(CSV_SPECIF_TRAIN), outputs_fc)
        print('Train outputs shape conv: {}, fc1: {}'.format(outputs_conv.shape, outputs_fc.shape))
    else:
        outputs = np.array(outputs)
        n_samples = len(outputs)
        outputs = np.reshape(outputs, [n_samples, -1])
        np.save('./result/noGAN/train_outputs_{}_{}_only'.format(cnn_output, CSV_SPECIF_TRAIN), outputs)
        print('Train outputs shape: {}'.format(outputs.shape))
    np.save('./result/noGAN/train_targets_{}'.format(CSV_SPECIF_TRAIN), np.array(targets))
    return outputs


def get_val_outputs():
    outputs, targets = [], []
    outputs_conv, outputs_fc = [], []
    with torch.no_grad():
        for i, batch in enumerate(VAL_LOADER_TRAIN):
            input = batch[0]
            target = batch[1].long()
            targets.extend(target.cpu().data.numpy())
            input = input.to(device)
            target = target.to(device)
            # compute output
            if cnn_output == 'conv':
                output = cnn(input, conv_only=True).cpu().data.numpy()
                outputs.extend(output)
            elif cnn_output == 'fc':
                output = cnn(input, fc1_out=True).cpu().data.numpy()
                outputs.extend(output)
            elif cnn_output == 'both':
                output_conv = cnn(input, conv_only=True).cpu().data.numpy()
                outputs_conv.extend(output_conv)
                output_fc = cnn(input, fc1_out=True).cpu().data.numpy()
                outputs_fc.extend(output_fc)
            else:
                raise Exception('Unknown cnn output type! ({})'.format(cnn_output))
    if cnn_output == 'both':
        outputs_conv = np.array(outputs_conv)
        outputs_fc = np.array(outputs_fc)
        n_samples = len(outputs_conv)
        outputs_conv = np.reshape(outputs_conv, [n_samples, -1])
        outputs_fc = np.reshape(outputs_fc, [n_samples, -1])
        np.save('./result/noGAN/val_outputs_conv_{}'.format(CSV_SPECIF_TRAIN), outputs_conv)
        np.save('./result/noGAN/val_outputs_fc_{}'.format(CSV_SPECIF_TRAIN), outputs_fc)
        print('Val outputs shape conv: {}, fc1: {}'.format(outputs_conv.shape, outputs_fc.shape))
    else:
        outputs = np.array(outputs)
        n_samples = len(outputs)
        outputs = np.reshape(outputs, [n_samples, -1])
        np.save('./result/noGAN/val_outputs_{}_{}_only'.format(cnn_output, CSV_SPECIF_TRAIN), outputs)
        print('Val outputs shape: {}'.format(outputs.shape))
    np.save('./result/noGAN/val_targets_{}'.format(CSV_SPECIF_TRAIN), np.array(targets))
    return outputs


def get_test_outputs():
    outputs, targets = [], []
    outputs_conv, outputs_fc = [], []
    with torch.no_grad():
        for i, batch in enumerate(TRAIN_LOADER_TEST):
            input = batch[0]
            target = batch[1].long()
            targets.extend(target.cpu().data.numpy())
            input = input.to(device)
            # compute output
            if cnn_output == 'conv':
                output = cnn(input, conv_only=True).cpu().data.numpy()
                outputs.extend(output)
            elif cnn_output == 'fc1':
                output = cnn(input, fc1_out=True).cpu().data.numpy()
                outputs.extend(output)
            elif cnn_output == 'both':
                output_conv = cnn(input, conv_only=True).cpu().data.numpy()
                outputs_conv.extend(output_conv)
                output_fc = cnn(input, fc1_out=True).cpu().data.numpy()
                outputs_fc.extend(output_fc)
            else:
                raise Exception('Unknown cnn output type! ({})'.format(cnn_output))
    if cnn_output == 'both':
        outputs_conv = np.array(outputs_conv)
        outputs_fc = np.array(outputs_fc)
        n_samples = len(outputs_conv)
        outputs_conv = np.reshape(outputs_conv, [n_samples, -1])
        outputs_fc = np.reshape(outputs_fc, [n_samples, -1])
        np.save('./result/noGAN/test_outputs_conv_{}'.format(CSV_SPECIF_TEST), outputs_conv)
        np.save('./result/noGAN/test_outputs_fc_{}'.format(CSV_SPECIF_TEST), outputs_fc)
        print('Test outputs shape conv: {}, fc1: {}'.format(outputs_conv.shape, outputs_fc.shape))
    else:
        outputs = np.array(outputs)
        n_samples = len(outputs)
        outputs = np.reshape(outputs, [n_samples, -1])
        np.save('./result/noGAN/test_outputs_{}_{}_only'.format(cnn_output, CSV_SPECIF_TEST), outputs)
        print('Test outputs shape: {}'.format(outputs.shape))
    np.save('./result/noGAN/test_targets_{}'.format(CSV_SPECIF_TEST), np.array(targets))
    return outputs


def svd_reducer(data, n_components):
    reducer = TruncatedSVD(n_components=n_components)
    reducer.fit(data)
    print('n_components: {},\texplained variance: {:.4f}\texplained variance ratio: {:.4f}'
          .format(n_components, reducer.explained_variance_.sum(), reducer.explained_variance_ratio_.sum()))
    return reducer


def plot_output(train_samples, train_labels, test_samples, test_labels, centers, output_type, explained_var_ratio,
                train_only=False):
    plt.clf()
    plt.hold(True)
    colors = {0: 'blue', 1: 'red', 2: 'green', 3: 'black', 4: 'cyan', 5: 'magenta', 6: 'grey', 7: 'maroon'}
    dim = train_samples[0].shape[0]
    n_centers = centers.shape[0]
    if dim == 2:
        for sample, label in zip(train_samples, train_labels):
            plt.scatter(sample[0], sample[1], color=colors[label], s=1)
        if not train_only:
            for sample, label in zip(test_samples, test_labels):
                plt.scatter(sample[0], sample[1], color=colors[label + 4], s=1)
        for center in centers:
            plt.scatter(center[0], center[1], color='yellow')
        plt.title('output type: {}, explained variance ratio: {:.3f}, centers: {}'
                  .format(output_type, explained_var_ratio, n_centers))
    else:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.hold(True)
        for sample, label in zip(train_samples, train_labels):
            ax.scatter(sample[0], sample[1], sample[2], color=colors[label])
        if not train_only:
            for sample, label in zip(test_samples, test_labels):
                ax.scatter(sample[0], sample[1], sample[2], color=colors[label + 4])
        for center in centers:
            ax.scatter(center[0], center[1], center[2], color='yellow')
        plt.title('output type: {}, explained variance ratio: {:.3f}, centers: {}'
                  .format(output_type, explained_var_ratio, n_centers))
    plt.savefig('./result/noGAN/projections_{}_{}D_{}_{}.jpg'
                .format(output_type, dim, alg_type, 'train' if train_only else 'both'))
    pass


if __name__ == '__main__':

    train_cnn_only()
    torch.save(cnn, './result/noGAN/cnn_model.pt')
    train_loss, train_acc, test_loss, test_acc = validate(TRAIN_LOADER_TRAIN, TRAIN_LOADER_TEST)
    train_outputs = get_train_outputs()
    val_outputs = get_val_outputs()
    test_outputs = get_test_outputs()

    alg_type = 'HDBSCAN'
    train_outputs = np.load('./result/noGAN/train_outputs_{}_{}.npy'.format(output_type, CSV_SPECIF_TRAIN))
    train_labels = np.load('./result/noGAN/train_targets_{}.npy'.format(CSV_SPECIF_TRAIN))
    test_outputs = np.load('./result/noGAN/test_outputs_{}_{}.npy'.format(output_type, CSV_SPECIF_TEST))
    test_labels = np.load('./result/noGAN/test_targets_{}.npy'.format(CSV_SPECIF_TEST))
    val_outputs = np.load('./result/noGAN/val_outputs_{}_{}.npy'.format(output_type, CSV_SPECIF_TRAIN))
    explained = []
    for n_components in range(1, 31):
        print('\nn_components: {}'.format(n_components))
        reducer = svd_reducer(train_outputs, n_components=n_components)
        reduced_train = reducer.transform(train_outputs)
        reduced_val = reducer.transform(val_outputs)
        reduced_test = reducer.transform(test_outputs)
        centers = find_clusters(reduced_train, alg_type, 100)
        explained.append(reducer.explained_variance_ratio_.sum())
        if PLOT and (n_components == 2 or n_components == 3):
            np.save('./result/noGAN/projections_bb-lb_train_fc_{}D.npy'.format(n_components), reduced_train)
            np.save('./result/noGAN/projections_bb-lb_val_fc_{}D.npy'.format(n_components), reduced_val)
            np.save('./result/noGAN/projections_bb-rt_train_fc_{}D.npy'.format(n_components), reduced_test)
            plot_output(reduced_train, train_labels, reduced_test, test_labels, centers, output_type,
                        explained[-1], train_only=True)
            plot_output(reduced_train, train_labels, reduced_test, test_labels, centers, output_type,
                        explained[-1], train_only=False)
        train_distances, train_mean, train_var, train_min, train_max = dataset_distance(reduced_train, centers)
        print_dist_info('train', train_mean, train_var, train_min, train_max)
        test_distances, test_mean, test_var, test_min, test_max = dataset_distance(reduced_test, centers)
        print_dist_info('test', test_mean, test_var, test_min, test_max)
    plt.clf()
    plt.plot(np.arange(1, len(explained) + 1), explained)
    plt.title('Explained variance ratio, {}, output: {}, train: {}'.format(alg_type, output_type, CSV_SPECIF_TRAIN))
    plt.savefig('./result/noGAN/explained_{}_{}'.format(output_type, CSV_SPECIF_TRAIN))

    out_fname.close()
