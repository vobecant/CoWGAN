import os, sys

sys.path.append(os.getcwd())

import numpy as np
from matplotlib import pyplot as plt
from models.CNN import CNN
from models.wgan import GoodGeneratorConv, GoodDiscriminatorConv
import torch
import torchvision
from torch import nn
from torch import autograd
from torch import optim
from torchvision import transforms, datasets
from torch.autograd import grad
from timeit import default_timer as timer
from models.dataset import GeomDataset
import libs as lib
from libs.save import Saver
from libs.sampler import MyWeightedRandomSampler as MyWeightSampler
from libs.sampler import MyDataLoader
from libs.mean_counter import MeanVarCounter

# ========= DATA DIRECTORIES ============
DATA_SPECIF = 'splitted'
DATA_DIR = './datasets/' + DATA_SPECIF + '/train'
CSV_FILE_TRAIN_BOTTOM = DATA_DIR + '/geom_data_bottom.csv'
CSV_FILE_TRAIN = DATA_DIR + '/geom_data_all.csv'
VAL_DIR = './datasets/' + DATA_SPECIF + '/val'
CSV_FILE_VAL = VAL_DIR + '/geom_data_all.csv'
OUTPUT_PATH = './result/CNN_GAN/' + DATA_SPECIF + '/'
LOAD = True
LOAD_FILE_CNN = './result/CNN/' + DATA_SPECIF + '/cnn_best.pt'
LOAD_FILE_G = './result/WGAN/' + DATA_SPECIF + '/G_best.pt'
LOAD_FILE_D = './result/WGAN/' + DATA_SPECIF + '/D_best.pt'

# ========= CONSTANTS ============
N_TRAIN_SAMPLES = 10000
N_EPOCHS = 1000
LEARNING_RATE = 0.001
BATCH_SIZE = 64
CRITIC_ITERS = 5  # How many iterations to train the critic for
GENER_ITERS = 1
LAMBDA = 10  # Gradient penalty lambda hyperparameter
VAL_FREQ = 1
SAVE_FREQ = 1
DIM_CONV = 128
IM_SIZE_CONV = 8
# Device configuration
cuda_available = torch.cuda.is_available()
device = torch.device('cuda:0' if cuda_available else 'cpu')
NUM_TRAIN_SAMPLES = 10000
MULTIPLE_GPU = False


# ======== FUNCTION DEFINITIONS ==========

def load_my_data(path_to_folder, csvfile, batch_size=BATCH_SIZE, shuffle=True, drop_last=True):
    resize = transforms.Resize(64)
    crop = transforms.CenterCrop(64)
    to_tensor = transforms.ToTensor()
    dataset = GeomDataset(csvfile, path_to_folder, resize, crop, to_tensor)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=5,
                                                 drop_last=True, pin_memory=True)
    # print('loaded dataset_iter, number of samples: {}'.format(len(dataset_iter)))
    return dataset_loader


def load_my_data_weight_sampler(path_to_folder, csvfile, model, num_samples=None, batch_size=BATCH_SIZE):
    resize = transforms.Resize(64)
    crop = transforms.CenterCrop(64)
    to_tensor = transforms.ToTensor()
    dataset = GeomDataset(CSV_FILE_TRAIN, DATA_DIR, resize, crop, to_tensor)
    weights = list(np.ones(len(dataset)))
    print('dataset size: {}, WEIGHTS shape: {}, num_samples: {}, batch_size: {}'
          .format(len(dataset), len(weights), num_samples, batch_size))
    sampler = MyWeightSampler(weights, num_samples=num_samples, replacement=False)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, sampler=sampler, num_workers=0,
                                                 drop_last=True, pin_memory=True)
    for batch in dataset_loader:
        pass
    print('test ok')
    return dataset_loader


def load_data_last_iter(path_to_folder, csvfile, batch_size=BATCH_SIZE):
    resize = transforms.Resize(64)
    crop = transforms.CenterCrop(64)
    to_tensor = transforms.ToTensor()
    dataset = GeomDataset(csvfile, path_to_folder, resize, crop, to_tensor)
    dataset_loader = MyDataLoader(dataset, batch_size=batch_size, shuffle=True)
    # print('loaded dataset_iter, number of samples: {}'.format(len(dataset_iter)))
    return dataset_loader


def save_network(model, path, epoch):
    torch.save(model.state_dict(), path + '/model_{}.ckpt'.format(epoch))


def gen_rand_noise():
    noise = torch.randn(BATCH_SIZE, 128)
    noise = noise.to(device)
    return noise


def calc_gradient_penalty(netD, real_data, fake_data):
    alpha = torch.rand(BATCH_SIZE, 1)
    alpha = alpha.expand(BATCH_SIZE, int(real_data.nelement() / BATCH_SIZE)).contiguous()
    alpha = alpha.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
    alpha = alpha.to(device)

    fake_data = fake_data.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
    interpolates = alpha * real_data.detach() + ((1 - alpha) * fake_data.detach())

    interpolates = interpolates.to(device)
    interpolates.requires_grad_(True)

    disc_interpolates = netD(interpolates)

    gradients = autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                              grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]

    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * LAMBDA
    return gradient_penalty


def init_counter():
    loader = load_my_data(DATA_DIR, CSV_FILE_TRAIN_BOTTOM)
    res = []
    for batch in loader:
        data = batch[0].to(device)
        Dout = D(data).cpu().data.numpy()
        res.extend(Dout)
    mean = np.mean(res)
    var = np.var(res)
    n_samples = len(loader)
    counter = MeanVarCounter(mean_init=mean, var_init=var, samples_used=n_samples)
    return counter


# ======== MODELS ==========
cuda_available = torch.cuda.is_available()
device = torch.device("cuda" if cuda_available else "cpu")
fixed_noise = gen_rand_noise()

if LOAD:
    cnn = torch.load(LOAD_FILE_CNN)
    print('loaded CNN from {}'.format(LOAD_FILE_CNN))
    G = torch.load(LOAD_FILE_G)
    print('loaded generator from {}'.format(LOAD_FILE_G))
    D = torch.load(LOAD_FILE_D)
    print('loaded discriminator from {}'.format(LOAD_FILE_D))
else:
    cnn = CNN()
    G = GoodGeneratorConv()  # ConvInGenerator()
    D = GoodDiscriminatorConv()  # ConvInDiscriminator()
if torch.cuda.device_count() > 1 and MULTIPLE_GPU:
    print('Use {} GPUs'.format(torch.cuda.device_count()))
    cnn = nn.DataParallel(cnn)
    G = nn.DataParallel(G)
    D = nn.DataParallel(D)
G = G.to(device)
D = D.to(device)
cnn.to(device)

one = torch.FloatTensor([1])
mone = one * -1

optimizer_cnn = optim.Adam(cnn.parameters(), lr=LEARNING_RATE)
optimizer_D = optim.Adam(D.parameters(), lr=LEARNING_RATE, betas=(0, 0.9))
optimizer_G = optim.Adam(G.parameters(), lr=LEARNING_RATE, betas=(0, 0.9))

one = one.to(device)
mone = mone.to(device)
print('used device: {}'.format('gpu' if cuda_available else 'cpu'))
if cuda_available: cnn.to(device)
criterion = nn.CrossEntropyLoss()

VAL_LOADER_CNN = load_my_data(VAL_DIR, CSV_FILE_VAL)
TRAIN_LOADER_GAN = load_my_data(DATA_DIR, CSV_FILE_TRAIN)
train_loader_gan_noshuffle = load_my_data(DATA_DIR, CSV_FILE_TRAIN, shuffle=False, drop_last=False)

resize = transforms.Resize(64)
crop = transforms.CenterCrop(64)
to_tensor = transforms.ToTensor()
dataset = GeomDataset(CSV_FILE_TRAIN, DATA_DIR, resize, crop, to_tensor)
WEIGHTS = list(np.ones(len(dataset)))
print('dataset size: {}, WEIGHTS shape: {}, num_samples: {}, batch_size: {}'
      .format(len(dataset), len(WEIGHTS), NUM_TRAIN_SAMPLES, BATCH_SIZE))
SAMPLER = MyWeightSampler(WEIGHTS, num_samples=NUM_TRAIN_SAMPLES, replacement=False)
CNN_TRAIN_WEIGHT_LOADER = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, sampler=SAMPLER, num_workers=5,
                                                      drop_last=True, pin_memory=True)

out_folder = OUTPUT_PATH


# ======= TRAINING =========


def train(counter):
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver = Saver('val_acc', better_value_fnc, out_folder)
    mean_var_counter = counter
    print('start training')
    cnn_train_loader = load_my_data_weight_sampler(DATA_DIR, CSV_FILE_TRAIN, None, num_samples=10000, batch_size=64)
    gan_train_iter = iter(TRAIN_LOADER_GAN)
    for epoch in range(N_EPOCHS):
        start_time = timer()

        # ---------------------TRAIN G------------------------
        gen_cost = train_G()

        # ---------------------TRAIN D------------------------
        disc_cost, w_dist, weights = train_D(gan_train_iter)

        # --------------------TRAIN CNN-----------------------
        print(cnn_train_loader.sampler.weights[1:10])
        for batch in cnn_train_loader:
            pass
        print('before reweight OK')
        SAMPLER.reweight(weights)
        print(cnn_train_loader.sampler.weights[1:10])
        for batch in cnn_train_loader:
            pass
        print('after reweight OK')
        train_acc, train_loss_cnn = train_cnn(cnn_train_loader)

        # ------------------VALIDATE GAN----------------------
        cnn_val_iter = iter(VAL_LOADER_CNN)
        dev_disc_costs = []
        for _, images in enumerate(cnn_val_iter):
            imgs = torch.Tensor(images[0])
            imgs = imgs.to(device)
            with torch.no_grad():
                imgs_v = imgs
            D_res = D(imgs_v)
            _dev_disc_cost = -D_res.mean().cpu().data.numpy()
            dev_disc_costs.append(_dev_disc_cost)
        disc_cost_val = np.mean(dev_disc_costs)

        # ----------------- VALIDATE CNN ---------------------
        cnn_val_iter = iter(VAL_LOADER_CNN)
        val_loss, val_acc = validate(cnn_val_iter, cnn, criterion)

        # ------ create plots ------
        lib.plot.plot(OUTPUT_PATH + 'time', timer() - start_time)
        lib.plot.plot(OUTPUT_PATH + 'train_loss', train_loss_cnn)
        lib.plot.plot(OUTPUT_PATH + 'val_loss', val_loss)
        lib.plot.plot(OUTPUT_PATH + 'train_acc', train_acc)
        lib.plot.plot(OUTPUT_PATH + 'val_acc', val_acc)
        lib.plot.plot(OUTPUT_PATH + 'gen_cost', gen_cost)
        lib.plot.plot(OUTPUT_PATH + 'disc_cost', disc_cost)
        lib.plot.plot(OUTPUT_PATH + 'disc_cost_val', disc_cost_val)
        lib.plot.plot(OUTPUT_PATH + 'w_dist', w_dist)
        lib.plot.plot(OUTPUT_PATH + 'mean_D_real', mean_var_counter.get_mean())
        lib.plot.plot(OUTPUT_PATH + 'var_D_real', mean_var_counter.get_var())
        lib.plot.tick()

        # ------ save -------
        saver.update('cnn', cnn)
        saver.update('WEIGHTS', cnn.state_dict())
        saver.update('optimizer', optimizer_cnn.state_dict())
        saver.update('train_loss', train_loss_cnn)
        saver.update('val_loss', val_loss)
        saver.update('train_acc', train_acc)
        saver.update('val_acc', val_acc)
        saver.update('epoch', epoch)
        saver.update('D', D)
        saver.update('D_weights', D.state_dict())
        saver.update('G', G)
        saver.update('G_weights', G.state_dict())
        saver.update('gen_cost', gen_cost)
        saver.update('disc_cost', disc_cost)
        saver.update('disc_cost_val', disc_cost_val)
        saver.update('w_dist', w_dist)
        saver.update('mean_D_real', mean_var_counter.get_mean())
        saver.update('var_D_real', mean_var_counter.get_var())
        saver.update('counter_t', mean_var_counter.get_t())
        saver.tick()
        if epoch % SAVE_FREQ == (SAVE_FREQ - 1):
            lib.plot.flush(len(OUTPUT_PATH))
            saver.flush()

        # ----- print info ------
        print(
            'Epoch [{}/{}]\nCNN:\tTrain Loss: {:.5f}, Train Acc: {:.2f}, Val Loss: {:.5f}, Val Acc: {:.2f}\n'
            'GAN:\tG cost: {:.5f}, D cost train: {:.5f}, D cost val: {:.5f}, W dist: {:.5f}\n\t time: {:.3f}'.format(
                epoch + 1, N_EPOCHS, train_loss_cnn, train_acc, val_loss, val_acc,
                gen_cost, disc_cost, disc_cost_val, w_dist,
                timer() - start_time))


def train_cnn(train_loader):
    # train_iter = iter(train_loader)
    # UNfreeze CNN
    for p in cnn.parameters():
        p.requires_grad_(True)  # UNfreeze CNN
    train_losses_cnn = np.asarray([])
    total_train_cnn, correct_train_cnn = 0, 0
    optimizer_cnn.zero_grad()
    cnn.train()
    for batch in train_loader:
        # Forward pass
        images = batch[0]
        labels = batch[1]
        images = images.to(device)
        labels = labels.to(device)
        outputs = cnn(images)
        _, predicted = torch.max(outputs.data, 1)

        # forward pass
        loss = criterion(outputs, labels)
        train_losses_cnn = np.append(train_losses_cnn, loss.item())

        # Backward and optimize
        optimizer_cnn.zero_grad()
        loss.backward()
        optimizer_cnn.step()

        total_train_cnn += labels.size(0)
        correct_train_cnn += (predicted == labels).sum().item()
    train_acc = 100 * correct_train_cnn / total_train_cnn
    train_loss_cnn = np.mean(train_losses_cnn)
    return train_acc, train_loss_cnn


def train_D(loader):
    # freeze CNN
    for p in cnn.parameters():
        p.requires_grad_(False)  # freeze CNN
    for p in D.parameters():  # reset requires_grad
        p.requires_grad_(True)  # they are set to False below in training G
    for i in range(CRITIC_ITERS):
        # print("Critic iter: " + str(i))

        start = timer()
        D.zero_grad()
        optimizer_D.zero_grad()

        # gen fake dataset_iter and load real dataset_iter
        noise = gen_rand_noise()
        with torch.no_grad():
            noisev = noise  # totally freeze G, training D
        fake_data = G(noisev).detach()
        end = timer()
        # print(f'---gen G elapsed time: {end-start}')
        start = timer()
        batch = next(loader, None)
        if batch is None:
            loader = iter(loader)
            batch = loader.next()
        data = batch[0]  # batch[1] contains labels
        real_data = data.to(device)  # TODO: modify load_data for each loading
        real_data = cnn(real_data, conv_only=True)
        end = timer()
        # print(f'---load real imgs elapsed time: {end-start}')
        start = timer()

        # train with real dataset_iter
        disc_real = D(real_data)
        disc_real = disc_real.mean()
        # track mean response of discriminator on real data over the training
        mean_var_counter.add_samples(disc_real.cpu().data.numpy())

        # train with fake dataset_iter
        disc_fake = D(fake_data)
        disc_fake = disc_fake.mean()

        # showMemoryUsage(0)
        # train with interpolates dataset_iter
        gradient_penalty = calc_gradient_penalty(D, real_data, fake_data)
        # showMemoryUsage(0)

        # final disc cost
        disc_cost = disc_fake - disc_real + gradient_penalty
        disc_cost.backward()
        w_dist = disc_fake - disc_real
        optimizer_D.step()
    weights = compute_weights()
    return disc_cost.cpu().data.numpy(), w_dist.cpu().data.numpy(), weights


def compute_weights():
    '''
    Computes WEIGHTS for data sampling for CNN.
    Weights are set as an absolute distance from the mean discriminator response on real data.
    :return: list of WEIGHTS
    '''
    ts = timer()
    print('start computing WEIGHTS')
    weights = []
    with torch.no_grad():
        for batch in iter(train_loader_gan_noshuffle):
            data = batch[0]
            data = data.to(device)
            Dout = D(data).cpu().data.numpy()
            new_weights = np.subtract(Dout, mean_var_counter.get_mean())
            weights.extend(new_weights)
    print('WEIGHTS computed in {}'.format(timer() - ts))
    return np.abs(weights).tolist()


def train_G():
    optimizer_G.zero_grad()
    # freeze D
    for p in D.parameters():
        p.requires_grad_(False)  # freeze D
    gen_cost = None
    for i in range(GENER_ITERS):
        # print("Generator iters: " + str(i))
        G.zero_grad()
        noise = gen_rand_noise()
        noise.requires_grad_(True)
        fake_data = G(noise)
        gen_cost = D(fake_data)
        gen_cost = gen_cost.mean()
        gen_cost.backward(mone)
        gen_cost = -gen_cost
    optimizer_G.step()
    return gen_cost


def validate(val_loader, model, criterion, show=False):
    # switch to evaluate mode
    model.eval()

    losses = np.asarray([])
    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(val_loader):
            input = batch[0]
            target = batch[1]
            # compute output
            output = model(input)
            _, predicted = torch.max(output.data, 1)
            loss = criterion(output, target)
            losses = np.append(losses, loss.data.numpy())
            total += target.size(0)
            correct += (predicted == target).sum().item()
            if show:
                fig, ax = plt.subplots()
                for sample, pred, gt in zip(input, target, predicted):
                    show_sample(sample, pred, gt, ax)
    val_loss = np.mean(losses)
    val_acc = 100 * correct / total
    return val_loss, val_acc


def show_sample(sample, pred, gt, ax):
    sample = sample.data.numpy()
    pred = pred.data.numpy()
    gt = gt.data.numpy()
    im = np.rollaxis(sample, 0, 3)
    ax.cla()
    ax.imshow(im)
    ax.set_title('pred {}, gt {}'.format(pred, gt))
    plt.pause(0.5)


if __name__ == '__main__':
    mean_var_counter = init_counter()
    train(mean_var_counter)
