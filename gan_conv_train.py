import os, sys

sys.path.append(os.getcwd())

import time
import functools
import argparse

import numpy as np
# import sklearn.datasets

import libs as lib
import libs.plot
from tensorboardX import SummaryWriter

import pdb
# import gpustat

# import models.dcgan as dcgan
from models.wgan import *

import torch
import torchvision
from torch import nn
from torch import autograd
from torch import optim
from torchvision import transforms, datasets
from torch.autograd import grad
from timeit import default_timer as timer
from models.dataset import GeomDataset, ConvDataset
from create_dataset import NUM_CLASSES
from models.CNN import NN_inclass as NN
from libs.save import Saver

import torch.nn.init as init

DATA_SPECIF = 'splitted'
DATA_DIR = './datasets/' + DATA_SPECIF + '/train'  # + '/conv.npy'
LABELS_DIR = './datasets/' + DATA_SPECIF + '/labels.npy'
CSV_FILE_TRAIN = DATA_DIR + '/geom_data_bottom.csv'
VAL_DIR = './datasets/' + DATA_SPECIF + '/val'  # + '/conv_val.npy'
LABELS_VAL_DIR = './datasets/' + DATA_SPECIF + '/labels_val.npy'
CSV_FILE_VAL = VAL_DIR + '/geom_data_bottom.csv'
OUTPUT_PATH = './result/WGAN/splitted/'  # output path where result (.e.g drawing images, cost, chart) will be stored
CNN_FILE = './result/CNN/splitted/cnn_best.pt'

IMAGE_DATA_SET = 'geom'  # change this to something else, e.g. 'imagenets' or 'raw' if your dataset_iter is just a folder of raw images.
# If you use lmdb, you'll need to write the loader by yourself, see load_data
# TRAINING_CLASS = ['dining_room_train', 'bridge_train', 'restaurant_train', 'tower_train']
# VAL_CLASS = ['dining_room_val', 'bridge_val', 'restaurant_val', 'tower_val']

if len(DATA_DIR) == 0:
    raise Exception('Please specify path to dataset_iter directory in gan_64x64.py!')

RESTORE_MODE = False  # if True, it will load saved cnn from OUT_PATH and continue to train
START_ITER = 0  # starting iteration
MODE = 'wgan-gp-conv'  # dcgan, wgan
DIM = 64  # Model dimensionality
CRITIC_ITERS = 5  # How many iterations to train the critic for
GENER_ITERS = 1
N_GPUS = 1  # Number of GPUs
BATCH_SIZE = 64  # Batch size. Must be a multiple of N_GPUS
END_ITER = 100000  # How many iterations to train for
LAMBDA = 10  # Gradient penalty lambda hyperparameter
OUTPUT_DIM = 64 * 64 * 3  # Number of pixels in each image
VAL_FREQ = 100
DIM_CONV = 128
IM_SIZE_CONV = 8


def showMemoryUsage(device=1):
    gpu_stats = gpustat.GPUStatCollection.new_query()
    item = gpu_stats.jsonify()["gpus"][device]
    print('Used/total: ' + "{}/{}".format(item["memory.used"], item["memory.total"]))


def weights_init(m):
    if isinstance(m, MyConvo2d):
        if m.conv.weight is not None:
            if m.he_init:
                init.kaiming_uniform_(m.conv.weight)
            else:
                init.xavier_uniform_(m.conv.weight)
        if m.conv.bias is not None:
            init.constant_(m.conv.bias, 0.0)
    if isinstance(m, nn.Linear):
        if m.weight is not None:
            init.xavier_uniform_(m.weight)
        if m.bias is not None:
            init.constant_(m.bias, 0.0)


def load_data(path_to_folder, classes):
    data_transform = transforms.Compose([
        transforms.Scale(64),
        transforms.CenterCrop(64),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
    ])
    if IMAGE_DATA_SET == 'lsun':
        dataset = datasets.LSUN(path_to_folder, classes=classes, transform=data_transform)
    else:
        dataset = datasets.ImageFolder(root=path_to_folder, transform=data_transform)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True, num_workers=5,
                                                 drop_last=True, pin_memory=True)
    return dataset_loader


def load_my_data(path_to_folder, csvfile):
    resize = transforms.Resize(64)
    crop = transforms.CenterCrop(64)
    to_tensor = transforms.ToTensor()
    dataset = GeomDataset(csvfile, path_to_folder, resize, crop, to_tensor)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True, num_workers=5,
                                                 drop_last=True, pin_memory=True)
    #print('loaded dataset_iter, number of samples: {}'.format(len(dataset)))
    return dataset_loader


def load_conv_data(labels_file, data_file, shuffle=True):
    dataset = ConvDataset(labels_file, data_file)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=shuffle, num_workers=5,
                                                 drop_last=True, pin_memory=True)
    # print('loaded CONV dataset_iter, number of samples: {}'.format(len(dataset_iter)))
    return dataset_loader


def training_data_loader():
    return load_my_data(DATA_DIR, CSV_FILE_TRAIN)
    # return load_conv_data(LABELS_DIR, TRAIN_DIR_TRAINSET)


def val_data_loader():
    return load_my_data(VAL_DIR, CSV_FILE_VAL)
    # return load_conv_data(LABELS_VAL_DIR, VAL_DIR_TRAIN)


def calc_gradient_penalty(netD, real_data, fake_data):
    alpha = torch.rand(BATCH_SIZE, 1)
    alpha = alpha.expand(BATCH_SIZE, int(real_data.nelement() / BATCH_SIZE)).contiguous()
    alpha = alpha.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
    alpha = alpha.to(device)

    fake_data = fake_data.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
    interpolates = alpha * real_data.detach() + ((1 - alpha) * fake_data.detach())

    interpolates = interpolates.to(device)
    interpolates.requires_grad_(True)

    disc_interpolates = netD(interpolates)

    gradients = autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                              grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]

    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * LAMBDA
    return gradient_penalty


def generate_image(netG, noise=None):
    if noise is None:
        noise = gen_rand_noise()

    with torch.no_grad():
        noisev = noise
    samples = netG(noisev)
    samples = samples.view(BATCH_SIZE, 3, 64, 64)
    samples = samples * 0.5 + 0.5
    return samples


OLDGAN = False


def gen_rand_noise():
    if OLDGAN:
        noise = torch.FloatTensor(BATCH_SIZE, 128, 1, 1)
        noise.resize_(BATCH_SIZE, 128, 1, 1).normal_(0, 1)
    else:
        noise = torch.randn(BATCH_SIZE, 128)
    noise = noise.to(device)

    return noise


cuda_available = torch.cuda.is_available()
device = torch.device("cuda" if cuda_available else "cpu")
fixed_noise = gen_rand_noise()

if RESTORE_MODE:
    aG = torch.load(OUTPUT_PATH + "generator.pt")
    aD = torch.load(OUTPUT_PATH + "discriminator.pt")
    print('restored learned WEIGHTS, START_ITER={}'.format(START_ITER))
else:
    if MODE == 'wgan-gp':
        aG = GoodGenerator(64, 64 * 64 * 3)
        aD = GoodDiscriminator(64)
        OLDGAN = False
    elif MODE == 'dcgan':
        aG = FCGenerator()
        aD = DCGANDiscriminator()
        OLDGAN = False
    elif MODE == 'wgan-gp-conv':
        aG = GoodGeneratorConv()  # ConvInGenerator()
        aD = GoodDiscriminatorConv()  # ConvInDiscriminator()
        OLDGAN = False
    else:
        aG = dcgan.DCGAN_G(DIM, 128, 3, 64, 1, 0)
        aD = dcgan.DCGAN_D(DIM, 128, 3, 64, 1, 0)
        OLDGAN = True
    aG.apply(weights_init)
    aD.apply(weights_init)

if not cuda_available:
    cnn = torch.load(CNN_FILE, map_location='cpu')
else:
    cnn = torch.load(CNN_FILE)
LR = 1e-4
optimizer_g = torch.optim.Adam(aG.parameters(), lr=LR, betas=(0, 0.9))
optimizer_d = torch.optim.Adam(aD.parameters(), lr=LR, betas=(0, 0.9))
one = torch.FloatTensor([1])
mone = one * -1

aG = aG.to(device)
aD = aD.to(device)
# nn_regressor = nn_regressor.to(device)
one = one.to(device)
mone = mone.to(device)



# Reference: https://github.com/caogang/wgan-gp/blob/master/gan_cifar10.py
def train():
    best_wdist = np.inf
    better_value_fnc = lambda best_wd, new_wd: new_wd < best_wd
    saver = Saver('wdist', better_value_fnc, OUTPUT_PATH)
    dataloader = training_data_loader()
    dataiter = iter(dataloader)
    # freeze CNN
    for p in cnn.parameters():
        p.requires_grad_(False)  # freeze CNN
    for iteration in range(START_ITER, END_ITER):
        start_time = time.time()
        # print("Iter: " + str(iteration))
        start = timer()
        # ---------------------TRAIN G------------------------
        for p in aD.parameters():
            p.requires_grad_(False)  # freeze D

        gen_cost = None
        for i in range(GENER_ITERS):
            # print("Generator iters: " + str(i))
            aG.zero_grad()
            noise = gen_rand_noise()
            noise.requires_grad_(True)
            fake_data = aG(noise)
            gen_cost = aD(fake_data)
            gen_cost = gen_cost.mean()
            gen_cost.backward(mone)
            gen_cost = -gen_cost

        optimizer_g.step()
        end = timer()
        # print(out_fname'---train G elapsed time: {end - start}')
        # ---------------------TRAIN D------------------------
        for p in aD.parameters():  # reset requires_grad
            p.requires_grad_(True)  # they are set to False below in training G
        for i in range(CRITIC_ITERS):
            # print("Critic iter: " + str(i))

            start = timer()
            aD.zero_grad()

            # gen fake dataset_iter and load real dataset_iter
            noise = gen_rand_noise()
            with torch.no_grad():
                noisev = noise  # totally freeze G, training D
            fake_data = aG(noisev).detach()
            end = timer();
            # print(out_fname'---gen G elapsed time: {end-start}')
            start = timer()
            batch = next(dataiter, None)
            if batch is None:
                dataiter = iter(dataloader)
                batch = dataiter.next()
            batch = batch[0]  # batch[1] contains labels
            real_data = batch.to(device)  # TODO: modify load_data for each loading
            real_data = cnn(real_data, conv_only=True)
            end = timer();
            # print(out_fname'---load real imgs elapsed time: {end-start}')
            start = timer()

            # train with real dataset_iter
            disc_real = aD(real_data)
            disc_real = disc_real.mean()

            # train with fake dataset_iter
            disc_fake = aD(fake_data)
            disc_fake = disc_fake.mean()

            # showMemoryUsage(0)
            # train with interpolates dataset_iter
            gradient_penalty = calc_gradient_penalty(aD, real_data, fake_data)
            # showMemoryUsage(0)

            # final disc cost
            disc_cost = disc_fake - disc_real + gradient_penalty
            disc_cost.backward()
            w_dist = disc_fake - disc_real
            optimizer_d.step()
            # ------------------VISUALIZATION----------
            if i == CRITIC_ITERS - 1 and not OLDGAN:
                writer.add_scalar('dataset_iter/disc_cost', disc_cost, iteration)
                writer.add_scalar('dataset_iter/gradient_pen', gradient_penalty, iteration)
                if iteration % VAL_FREQ == (VAL_FREQ - 1):
                    continue
                    body_model = [i for i in aD.children()][0]
                    layer1 = body_model.conv
                    xyz = layer1.weight.data.clone()
                    tensor = xyz.cpu()
                    tensors = torchvision.utils.make_grid(tensor, nrow=8, padding=1)
                    writer.add_image('D/conv1', tensors, iteration)

            end = timer();
            # print(out_fname'---train D elapsed time: {end-start}')

        # ---------------TRAIN NN regressor----------------
        # TODO:
        '''
        Jak získat vrozky na kterých naučit regresor?
        Vstupem by měl být výstup criticu/discriminatoru a výstup zda vzorek pochází z distribuce či nikoliv.
        Mohl bych tedy vždy například trénovat pouze na 5 náhodných batch opravdových dat a 5 náhodných batch 
        generovaných dat.
        Výstupy pro reálná a generovaná data se k sobě ale budou přibližovat.
        To by tedy mohlo vyústit v problém, kdy by při dobrém generátoru byl výstup D pro generovaná data velice
        blízko výstupu reálných dat.
        Mohli bychom tedy učit mapování z výstupu diskriminátoru/kritiku na Wasserstein distance.
        '''

        # ---------------VISUALIZATION---------------------
        writer.add_scalar('dataset_iter/gen_cost', gen_cost, iteration)

        lib.plot.plot(OUTPUT_PATH + 'time', time.time() - start_time)
        lib.plot.plot(OUTPUT_PATH + 'train_disc_cost', disc_cost.cpu().data.numpy())
        lib.plot.plot(OUTPUT_PATH + 'train_gen_cost', gen_cost.cpu().data.numpy())
        lib.plot.plot(OUTPUT_PATH + 'wasserstein_distance', w_dist.cpu().data.numpy())
        w_dist = np.abs(w_dist.item())

        # ---------------SAVING--------------------
        saver.update('D', aD)
        saver.update('D_w', aD.state_dict())
        saver.update('G', aG)
        saver.update('G_w', aG.state_dict())
        saver.update('wdist', w_dist)
        saver.update('D_cost', disc_cost.item())
        saver.update('G_cost', disc_cost.item())
        saver.update('D_opt',optimizer_d.state_dict())
        saver.update('G_opt',optimizer_g.state_dict())

        if w_dist < best_wdist:
            best_wdist = w_dist
            torch.save(aG, OUTPUT_PATH + "generator_best_abs.pt")
            torch.save(aD, OUTPUT_PATH + "discriminator_best_abs.pt")

        if iteration % VAL_FREQ == (VAL_FREQ - 1):
            val_loader = val_data_loader()
            dev_disc_costs = []
            for _, images in enumerate(val_loader):
                imgs = torch.Tensor(images[0])
                imgs = imgs.to(device)
                with torch.no_grad():
                    imgs_v = imgs
                D = aD(imgs_v)
                _dev_disc_cost = -D.mean().cpu().data.numpy()
                dev_disc_costs.append(_dev_disc_cost)
            lib.plot.plot(OUTPUT_PATH + 'dev_disc_cost.png', np.mean(dev_disc_costs))
            saver.update('D_cost_val', np.mean(dev_disc_costs))

            # ----------------------flush and save----------------------
            lib.plot.flush(len(OUTPUT_PATH),print_info=True)
            saver.flush()
        lib.plot.tick()
        saver.tick()


if __name__ == '__main__':
    print('cuda available: {}'.format(cuda_available))
    train()
