import os, sys

sys.path.append(os.getcwd())
f = open('log-data_eval.txt', 'w')

import numpy as np
import matplotlib

matplotlib.use('agg')
from matplotlib import pyplot as plt
from models.CNN import CNN
from models.wgan import GoodGeneratorConv, GoodDiscriminatorConv
import torch
import torchvision
from torch import nn
from torch import autograd
from torch import optim
from torchvision import transforms, datasets
from torch.autograd import grad
from timeit import default_timer as timer
from models.dataset import GeomDataset, GeomDataset3d
import libs as lib
from libs.save import Saver
from libs.sampler import MyWeightedRandomSampler as MyWeightSampler
from libs.sampler import MyDataLoader
from libs.mean_counter import MeanVarCounter
from models.wgan import *
import torch.nn.init as init
from libs.plot import Plotter
import matplotlib.mlab as mlab
from models.types import *
from scipy.stats import norm

# ========= DATA DIRECTORIES ============
DATA_SPECIF = 'geom3D'

TRAIN_DIR_TRAINSET = '/home/skovirad/dataset3d/3d_bottom/train'  # './datasets/' + DATA_SPECIF + '/train'
CSV_FILE_TRAIN_TRAINSET = TRAIN_DIR_TRAINSET + '/geom_data.csv'
TRAIN_DIR_TOP_TESTSET = '/home/skovirad/dataset3d/3d_top/train'
CSV_FILE_TRAIN_TOP_TESTSET = TRAIN_DIR_TOP_TESTSET + '/geom_data.csv'
TRAIN_DIR_ALL = '/home/skovirad/dataset3d/3d_unlimited/train'
CSV_FILE_TRAIN_ALL = TRAIN_DIR_ALL + '/geom_data.csv'

VAL_DIR_BOTTOM = '/home/skovirad/dataset3d/3d_bottom/valid'
CSV_FILE_VAL_BOTTOM = VAL_DIR_BOTTOM + '/geom_data.csv'
VAL_DIR_ALL = '/home/skovirad/dataset3d/3d_unlimited/valid'
CSV_FILE_VAL_ALL = VAL_DIR_ALL + '/geom_data.csv'

OUTPUT_PATH_CNN = './result/' + DATA_SPECIF + '/CNN/'
OUTPUT_PATH_GAN = './result/' + DATA_SPECIF + '/GAN/'
OUTPUT_PATH_BOTH = './result/' + DATA_SPECIF + '/BOTH/'
OUTPUT_PATH_CNN_FURTHER = './result/' + DATA_SPECIF + '/CNN/'

LOAD_FILE_CNN = './result/geom3D/CNN/cnn_best.pt'
LOAD_FILE_CNN_OPT = ''
LOAD_FILE_G = ''
LOAD_FILE_G_OPT = ''
LOAD_FILE_D = './result/geom3D/conGAN-3D_img/D_best.pt'
LOAD_FILE_D_OPT = ''
CNN_PARAMS_FILE = ''
GAN_PARAMS_FILE = ''
USE3D = True

# ========= CONSTANTS ===========
LOAD = False
USE_CNN, USE_G, USE_D = False, False, True
MULTIPLE_GPU = False
TRAIN_CNN, TRAIN_GAN = True, True

# CNN parameters
NUM_CLASSES = 4
N_EPOCHS_CNN = 5
LEARNING_RATE = 0.0001
LR_CNN = 1e-4
BATCH_SIZE = 64
PLOT_FREQ_CNN = 1
SAVE_FREQ_CNN = 1

# Device configuration
cuda_available = torch.cuda.is_available()
device = torch.device('cuda' if cuda_available else 'cpu')

# GAN parameters
RESTORE_MODE = False  # if True, it will load saved cnn from OUT_PATH and continue to train
LR_G, LR_D = 1e-4, 1e-4
START_ITER = 0  # starting iteration
MODE = 'wgan-gp-conv'  # 'wgan-gp-conv'
MODE = GAN_types.WGAN_GP_conv
ISCONV = (MODE == GAN_types.WGAN_GP_conv or MODE == GAN_types.ConWGAN_GP_conv or MODE == GAN_types.WGAN_GP_conv_FC)
DIM = 64  # Model dimensionality
CRITIC_ITERS = 5  # How many iterations to train the critic for
GENER_ITERS = 1
N_GPUS = 1  # Number of GPUs
END_ITER_GAN = 1000  # How many iterations to train for
LAMBDA = 10  # Gradient penalty lambda hyperparameter
OUTPUT_DIM = 64 * 64 * 3  # Number of pixels in each image
VAL_FREQ_GAN = 100
DIM_CONV = 128
IM_SIZE_CONV = 8


# ========= HELPER FUNCTIONS ===========
def print(text):
    sys.stdout.write('{}\n'.format(text))
    f.write('{}\n'.format(text))
    f.flush()


def find_itersect_points(m1, m2, std1, std2):
    a = 1 / (2 * std1 ** 2) - 1 / (2 * std2 ** 2)
    b = m2 / (std2 ** 2) - m1 / (std1 ** 2)
    c = m1 ** 2 / (2 * std1 ** 2) - m2 ** 2 / (2 * std2 ** 2) - np.log(std2 / std1)
    return np.roots([a, b, c])


def intersect_area(intersections, mean_train, mean_test, std_train):
    intersections -= mean_train
    intersections /= std_train
    intersections = np.sort(intersections)
    if len(intersections) == 2:
        res = norm.cdf(intersections[1]) - norm.cdf(intersections[0])
    else:
        if mean_train > mean_test:
            res = norm.cdf(intersections[0])
        else:
            res = 1 - norm.cdf(intersections[0])
    return res * 100


def load_my_data(path_to_folder, csvfile, batch_size=BATCH_SIZE, shuffle=True, drop_last=True, is3d=False):
    data_transform = transforms.Compose([
        transforms.Resize(64),
        transforms.CenterCrop(64),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
    ])
    if is3d:
        dataset = GeomDataset3d(csvfile, path_to_folder, data_transform)
    else:
        dataset = GeomDataset(csvfile, path_to_folder, data_transform)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=shuffle, num_workers=5,
                                                 drop_last=drop_last, pin_memory=True)
    # print('created dataset loader from {}, number of samples: {}, shuffle: {}'
    #      .format(path_to_folder, len(dataset), shuffle))
    return dataset_loader


def load_my_data_weight_sampler(path_to_folder, csvfile, model, num_samples=None, batch_size=BATCH_SIZE, is3d=False):
    data_transform = transforms.Compose([
        transforms.Resize(64),
        transforms.CenterCrop(64),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
    ])
    if is3d:
        dataset = GeomDataset3d(csvfile, path_to_folder, data_transform)
    else:
        dataset = GeomDataset(csvfile, path_to_folder, data_transform)
    weights = list(np.ones(len(dataset)))
    # print('dataset size: {}, WEIGHTS shape: {}, num_samples: {}, batch_size: {}'
    #     .format(len(dataset), len(weights), num_samples, batch_size))
    sampler = MyWeightSampler(weights, num_samples=num_samples, replacement=False)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, sampler=sampler, num_workers=0,
                                                 drop_last=True, pin_memory=True)
    return dataset_loader, sampler


def training_data_loader(csv, is3d=USE3D):
    return load_my_data(TRAIN_DIR_TRAINSET, csv, is3d=is3d)


def val_data_loader(csv, is3d=USE3D):
    return load_my_data(VAL_DIR_BOTTOM, csv, is3d=is3d)


def gen_rand_noise():
    noise = torch.randn(BATCH_SIZE, 128)
    noise = noise.to(device)
    return noise


def calc_gradient_penalty(netD, real_data, fake_data):
    alpha = torch.rand(BATCH_SIZE, 1)
    alpha = alpha.expand(BATCH_SIZE, int(real_data.nelement() / BATCH_SIZE)).contiguous()
    alpha = alpha.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
    alpha = alpha.to(device)

    fake_data = fake_data.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
    interpolates = alpha * real_data.detach() + ((1 - alpha) * fake_data.detach())

    interpolates = interpolates.to(device)
    interpolates.requires_grad_(True)

    disc_interpolates = netD(interpolates)

    gradients = autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                              grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]

    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * LAMBDA
    return gradient_penalty


def init_counter(D, num_classes=1):
    # print('Initializing counter.')
    st = timer()
    loader = load_my_data(TRAIN_DIR_TRAINSET, CSV_FILE_TRAIN_TRAINSET, is3d=USE3D)
    results, labels = [], []
    with torch.no_grad():
        for batch in loader:
            data = batch[0].to(device)
            tmp_labels = batch[1].cpu().data.numpy()
            if ISCONV: data = cnn(data, conv_only=True)
            Dout = D(data).cpu().data.numpy()
            results.extend(Dout)
            labels.extend(tmp_labels)
    counter = MeanVarCounter(num_classes=num_classes, samples=results, labels=labels)
    # print('Counter initialized in {}.'.format(timer() - st))
    return counter


def weights_init(m):
    if isinstance(m, MyConvo2d):
        if m.conv.weight is not None:
            if m.he_init:
                init.kaiming_uniform_(m.conv.weight)
            else:
                init.xavier_uniform_(m.conv.weight)
        if m.conv.bias is not None:
            init.constant_(m.conv.bias, 0.0)
    if isinstance(m, nn.Linear):
        if m.weight is not None:
            init.xavier_uniform_(m.weight)
        if m.bias is not None:
            init.constant_(m.bias, 0.0)


# ======= SET LOADERS, LOAD NETS,... =======
cuda_available = torch.cuda.is_available()
fixed_noise = gen_rand_noise()
VAL_LOADER_CNN_BOTTOM = load_my_data(VAL_DIR_BOTTOM, CSV_FILE_VAL_BOTTOM, is3d=USE3D)
VAL_LOADER_CNN_ALL = load_my_data(VAL_DIR_ALL, CSV_FILE_VAL_ALL, is3d=USE3D)
LOADER_TRAIN = load_my_data(TRAIN_DIR_TRAINSET, CSV_FILE_TRAIN_TRAINSET, is3d=USE3D)
LOADER_TEST = load_my_data(TRAIN_DIR_TOP_TESTSET, CSV_FILE_TRAIN_TOP_TESTSET, is3d=USE3D)
TRAIN_LOADER_ALL = load_my_data(TRAIN_DIR_ALL, CSV_FILE_TRAIN_ALL, is3d=USE3D)
train_loader_gan_noshuffle = load_my_data(TRAIN_DIR_ALL, CSV_FILE_TRAIN_ALL, shuffle=False, drop_last=False)
criterion_cnn = nn.CrossEntropyLoss()

if LOAD:
    if LOAD_FILE_CNN != '':
        cnn = torch.load(LOAD_FILE_CNN)
        print('loaded CNN from {}'.format(LOAD_FILE_CNN))
        optimizer_cnn = optim.Adam(cnn.parameters(), lr=LR_CNN)
        if LOAD_FILE_CNN_OPT != '': optimizer_cnn.load_state_dict(torch.load(LOAD_FILE_CNN_OPT))
    if LOAD_FILE_G != '':
        G = torch.load(LOAD_FILE_G)
        print('loaded generator from {}'.format(LOAD_FILE_G))
        optimizer_g = torch.optim.Adam(G.parameters(), lr=LR_G, betas=(0, 0.9))
        if LOAD_FILE_G_OPT != '': optimizer_g.load_state_dict(torch.load(LOAD_FILE_G_OPT))
    if LOAD_FILE_D != '':
        D = torch.load(LOAD_FILE_D)
        print('loaded discriminator from {}'.format(LOAD_FILE_D))
        optimizer_d = torch.optim.Adam(D.parameters(), lr=LR_D, betas=(0, 0.9))
        if LOAD_FILE_D_OPT != '': optimizer_d.load_state_dict(torch.load(LOAD_FILE_D_OPT))
    if CNN_PARAMS_FILE != '': cnn_params = np.load(CNN_PARAMS_FILE).item()
    if GAN_PARAMS_FILE != '': gan_params = np.load(GAN_PARAMS_FILE).item()
    # START_ITER = len(gan_params['wdist']) + 1
    print('loaded nets')
else:
    cnn = CNN(num_classes=NUM_CLASSES)
    if MODE == GAN_types.WGAN_GP_conv:
        G = GoodGeneratorConv()
        D = GoodDiscriminatorConv()
    elif MODE == GAN_types.WGAN_GP:
        G = GoodGenerator()
        D = GoodDiscriminator()
    elif MODE == GAN_types.ConWGAN_GP:
        G = GoodGeneratorCon()
        D = GoodDiscriminatorCon(num_class=NUM_CLASSES)
    optimizer_cnn = optim.Adam(cnn.parameters(), lr=LR_CNN)
    optimizer_g = torch.optim.Adam(G.parameters(), lr=LR_G, betas=(0, 0.9))
    optimizer_d = torch.optim.Adam(D.parameters(), lr=LR_D, betas=(0, 0.9))
    G.apply(weights_init)
    D.apply(weights_init)
if torch.cuda.device_count() > 1 and MULTIPLE_GPU:
    print('Use {} GPUs'.format(torch.cuda.device_count()))
    if USE_CNN: cnn = nn.DataParallel(cnn)
    if USE_G: G = nn.DataParallel(G)
    if USE_D: D = nn.DataParallel(D)
if USE_G: G = G.to(device)
if USE_D: D = D.to(device)
if USE_CNN: cnn.to(device)

one = torch.FloatTensor([1])
mone = one * -1

one = one.to(device)
mone = mone.to(device)

counter = None


# ======= TRAINING FUNCTIONS ========

def train_cnn_only():
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver = Saver('val_loss', better_value_fnc, OUTPUT_PATH_CNN)
    plotter_cnn = Plotter()
    print('start training CNN only')
    optimizer = optimizer_cnn  # optim.Adam(cnn.parameters(), lr=LEARNING_RATE)
    for epoch in range(N_EPOCHS_CNN):
        start_time = timer()
        # print('epoch {}\ttrain start'.format(epoch))
        ep_start = timer()
        # ------ training -------
        train_losses = np.asarray([])
        total_train, correct_train = 0, 0
        cnn.train()
        for i, batch in enumerate(LOADER_TRAIN):
            # if i == 5: break
            # Forward pass
            images = batch[0]
            labels = batch[1].long()
            # print(labels)
            images = images.to(device)
            labels = labels.to(device)
            # print(labels)
            outputs = cnn(images)
            _, predicted = torch.max(outputs.data, 1)

            # forward pass
            loss = criterion_cnn(outputs, labels)
            train_losses = np.append(train_losses, loss.item())

            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            total_train += labels.size(0)
            correct_train += (predicted == labels).sum().item()

        train_acc = 100 * correct_train / total_train
        train_loss = np.mean(train_losses)

        # ------ validation ------
        # print('epoch {}\tvalidation start'.format(epoch))
        val_loss, val_acc = validate_cnn_only(LOADER_TRAIN, cnn, criterion_cnn)

        # ------ create plots ------
        plotter_cnn.plot(OUTPUT_PATH_CNN + 'time', timer() - start_time)
        plotter_cnn.plot(OUTPUT_PATH_CNN + 'train_loss', train_loss)
        plotter_cnn.plot(OUTPUT_PATH_CNN + 'val_loss', val_loss)
        plotter_cnn.plot(OUTPUT_PATH_CNN + 'train_acc', train_acc)
        plotter_cnn.plot(OUTPUT_PATH_CNN + 'val_acc', val_acc)
        if epoch % PLOT_FREQ_CNN == (PLOT_FREQ_CNN - 1):
            plotter_cnn.flush(len(OUTPUT_PATH_CNN))
        plotter_cnn.tick()

        # ------ save -------
        saver.update('cnn', cnn)
        saver.update('cnn_w', cnn.state_dict())
        saver.update('cnn_opt', optimizer.state_dict())
        saver.update('train_loss', train_loss)
        saver.update('val_loss', val_loss)
        saver.update('train_acc', train_acc)
        saver.update('val_acc', val_acc)
        saver.update('epoch', epoch)
        if epoch % SAVE_FREQ_CNN == (SAVE_FREQ_CNN - 1):
            saver.flush()
        saver.tick()

        # ----- print info ------
        print('Epoch [{}/{}], Train Loss: {:.6f}, Train Acc: {:.2f}, Val Loss: {:.6f}, Val Acc: {:.2f}, time: {:.3f}'
              .format(epoch + 1, N_EPOCHS_CNN, train_loss, train_acc, val_loss, val_acc, timer() - ep_start))


def validate_cnn_only(val_loader, model, criterion, show=False):
    # switch to evaluate mode
    model.eval()
    losses = np.asarray([])
    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(val_loader):
            input = batch[0]
            target = batch[1].long()
            input = input.to(device)
            target = target.to(device)
            # compute output
            output = model(input)
            _, predicted = torch.max(output.data, 1)
            loss = criterion(output, target)
            losses = np.append(losses, loss.cpu().data.numpy())
            total += target.size(0)
            correct += (predicted == target).sum().item()
    val_loss = np.mean(losses)
    val_acc = 100 * correct / total
    return val_loss, val_acc


# Reference: https://github.com/caogang/wgan-gp/blob/master/gan_cifar10.py
def train_gan(D, G):
    best_wdist = np.inf
    better_value_fnc = lambda best_wd, new_wd: new_wd < best_wd
    saver = Saver('wdist', better_value_fnc, OUTPUT_PATH_GAN)
    plotter = Plotter()
    dataloader = training_data_loader(CSV_FILE_TRAIN_TRAINSET)
    val_loader = val_data_loader(CSV_FILE_VAL_BOTTOM)
    dataiter = iter(dataloader)
    print('start training GAN only  ')
    # freeze CNN
    for p in cnn.parameters():
        p.requires_grad_(False)  # freeze CNN
    for iteration in range(START_ITER, END_ITER_GAN):
        start_time = timer()
        # print("Iter: " + str(iteration))
        start = timer()
        # ---------------------TRAIN G------------------------
        for p in D.parameters():
            p.requires_grad_(False)  # freeze D

        gen_cost = None
        for i in range(GENER_ITERS):
            # print("Generator iters: " + str(i))
            G.zero_grad()
            noise = gen_rand_noise()
            noise.requires_grad_(True)
            fake_data = G(noise)
            gen_cost = D(fake_data)
            gen_cost = gen_cost.mean()
            gen_cost.backward(mone)
            gen_cost = -gen_cost

        optimizer_g.step()
        end = timer()
        # print(out_fname'---train G elapsed time: {end - start}')
        # ---------------------TRAIN D------------------------
        for p in D.parameters():  # reset requires_grad
            p.requires_grad_(True)  # they are set to False below in training G
        for i in range(CRITIC_ITERS):
            # print("Critic iter: " + str(i))

            start = timer()
            D.zero_grad()

            # gen fake dataset_iter and load real dataset_iter
            noise = gen_rand_noise()
            with torch.no_grad():
                noisev = noise  # totally freeze G, training D
            fake_data = G(noisev).detach()
            end = timer();
            # print(out_fname'---gen G elapsed time: {end-start}')
            start = timer()
            batch = next(dataiter, None)
            if batch is None:
                dataiter = iter(dataloader)
                batch = dataiter.next()
            batch = batch[0]  # batch[1] contains labels
            real_data = batch.to(device)  # TODO: modify load_data for each loading
            real_data = cnn(real_data, conv_only=True)
            end = timer();
            # print(out_fname'---load real imgs elapsed time: {end-start}')
            start = timer()

            # train with real dataset_iter
            disc_real = D(real_data)
            disc_real = disc_real.mean()

            # train with fake dataset_iter
            disc_fake = D(fake_data)
            disc_fake = disc_fake.mean()

            # showMemoryUsage(0)
            # train with interpolates dataset_iter
            gradient_penalty = calc_gradient_penalty(D, real_data, fake_data)
            # showMemoryUsage(0)

            # final disc cost
            disc_cost = disc_fake - disc_real + gradient_penalty
            disc_cost.backward()
            w_dist = disc_fake - disc_real
            optimizer_d.step()
            # ------------------VISUALIZATION----------
            end = timer();
            # print(out_fname'---train D elapsed time: {end-start}')

        # ---------------VISUALIZATION---------------------

        plotter.plot(OUTPUT_PATH_GAN + 'time', timer() - start_time)
        plotter.plot(OUTPUT_PATH_GAN + 'train_disc_cost', disc_cost.cpu().data.numpy())
        plotter.plot(OUTPUT_PATH_GAN + 'train_gen_cost', gen_cost.cpu().data.numpy())
        plotter.plot(OUTPUT_PATH_GAN + 'wasserstein_distance', w_dist.cpu().data.numpy())
        w_dist = np.abs(w_dist.item())

        # ---------------SAVING--------------------
        saver.update('D', D)
        saver.update('D_w', D.state_dict())
        saver.update('G', G)
        saver.update('G_w', G.state_dict())
        saver.update('wdist', w_dist)
        saver.update('D_cost', disc_cost.item())
        saver.update('G_cost', disc_cost.item())
        saver.update('D_opt', optimizer_d.state_dict())
        saver.update('G_opt', optimizer_g.state_dict())

        if w_dist < best_wdist:
            best_wdist = w_dist
            torch.save(G, OUTPUT_PATH_GAN + "generator_best_abs.pt")
            torch.save(D, OUTPUT_PATH_GAN + "discriminator_best_abs.pt")

        if iteration % VAL_FREQ_GAN == (VAL_FREQ_GAN - 1):
            dev_disc_costs = []
            for _, images in enumerate(val_loader):
                imgs = torch.Tensor(images[0])
                imgs = imgs.to(device)
                with torch.no_grad():
                    imgs_v = imgs
                D_res = D(imgs_v)
                _dev_disc_cost = -D_res.mean().cpu().data.numpy()
                dev_disc_costs.append(_dev_disc_cost)
            plotter.plot(OUTPUT_PATH_GAN + 'dev_disc_cost', np.mean(dev_disc_costs))
            saver.update('D_cost_val', np.mean(dev_disc_costs))

            # ----------------------flush and save----------------------
            plotter.flush(len(OUTPUT_PATH_GAN), print_info=True)
            saver.flush()
        plotter.tick()
        saver.tick()


def train_cnn(train_loader):
    # train_iter = iter(train_loader)
    # UNfreeze CNN
    for p in cnn.parameters():
        p.requires_grad_(True)  # UNfreeze CNN
    train_losses_cnn = np.asarray([])
    total_train_cnn, correct_train_cnn = 0, 0
    optimizer_cnn.zero_grad()
    cnn.train()
    for batch in train_loader:
        # Forward pass
        images = batch[0]
        labels = batch[1].long()
        images = images.to(device)
        labels = labels.to(device)
        outputs = cnn(images)
        _, predicted = torch.max(outputs.data, 1)

        # forward pass
        loss = criterion_cnn(outputs, labels)
        train_losses_cnn = np.append(train_losses_cnn, loss.item())

        # Backward and optimize
        optimizer_cnn.zero_grad()
        loss.backward()
        optimizer_cnn.step()

        total_train_cnn += labels.size(0)
        correct_train_cnn += (predicted == labels).sum().item()
    train_acc = 100 * correct_train_cnn / total_train_cnn
    train_loss_cnn = np.mean(train_losses_cnn)
    return train_acc, train_loss_cnn


def train_D(loader, counter):
    # freeze CNN
    for p in cnn.parameters():
        p.requires_grad_(False)  # freeze CNN
    for p in D.parameters():  # reset requires_grad
        p.requires_grad_(True)  # they are set to False below in training G
    for i in range(CRITIC_ITERS):
        # print("Critic iter: " + str(i))

        start = timer()
        D.zero_grad()
        optimizer_d.zero_grad()

        # gen fake dataset_iter and load real dataset_iter
        noise = gen_rand_noise()
        with torch.no_grad():
            noisev = noise  # totally freeze G, training D
        fake_data = G(noisev).detach()
        end = timer()
        # print(out_fname'---gen G elapsed time: {end-start}')
        start = timer()
        batch = next(loader, None)
        if batch is None:
            loader = iter(loader)
            batch = loader.next()
        data = batch[0]  # batch[1] contains labels
        real_data = data.to(device)  # TODO: modify load_data for each loading
        real_data = cnn(real_data, conv_only=True)
        end = timer()
        # print(out_fname'---load real imgs elapsed time: {end-start}')
        start = timer()

        # train with real dataset_iter
        disc_real = D(real_data)
        # track mean response of discriminator on real data over the training
        counter.add_samples(disc_real.cpu().data.numpy())
        disc_real = disc_real.mean()

        # train with fake dataset_iter
        disc_fake = D(fake_data)
        disc_fake = disc_fake.mean()

        # showMemoryUsage(0)
        # train with interpolates dataset_iter
        gradient_penalty = calc_gradient_penalty(D, real_data, fake_data)
        # showMemoryUsage(0)

        # final disc cost
        disc_cost = disc_fake - disc_real + gradient_penalty
        disc_cost.backward()
        w_dist = disc_fake - disc_real
        optimizer_d.step()
    weights = compute_weights(counter)
    return disc_cost.cpu().data.numpy(), w_dist.cpu().data.numpy(), weights


def compute_weights(counter):
    '''
    Computes WEIGHTS for data sampling for CNN.
    Weights are set as an absolute distance from the mean discriminator response on real data.
    :return: list of WEIGHTS
    '''
    ts = timer()
    weights = []
    with torch.no_grad():
        for batch in iter(train_loader_gan_noshuffle):
            data = batch[0]
            data = data.to(device)
            data = cnn(data, conv_only=True)
            Dout = D(data).cpu().data.numpy()
            new_weights = np.subtract(Dout, counter.get_mean())
            weights.extend(new_weights)
    # print('WEIGHTS computed in {}, '.format(timer() - ts), end='')
    return np.abs(weights).tolist()


def train_G():
    optimizer_g.zero_grad()
    # freeze D
    for p in D.parameters():
        p.requires_grad_(False)  # freeze D
    gen_cost = None
    for i in range(GENER_ITERS):
        # print("Generator iters: " + str(i))
        G.zero_grad()
        noise = gen_rand_noise()
        noise.requires_grad_(True)
        fake_data = G(noise)
        gen_cost = D(fake_data)
        gen_cost = gen_cost.mean()
        gen_cost.backward(mone)
        gen_cost = -gen_cost
    optimizer_g.step()
    return gen_cost.cpu().data.numpy()


def validate_cnn(val_loader, model, criterion, show=False):
    # switch to evaluate mode
    model.eval()
    losses = np.asarray([])
    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(val_loader):
            data = batch[0]
            data = data.to(device)
            target = batch[1].long()
            target = target.to(device)
            # compute output
            output = model(data)
            _, predicted = torch.max(output.data, 1)
            loss = criterion(output, target)
            losses = np.append(losses, loss.cpu().data.numpy())
            total += target.size(0)
            correct += (predicted == target).sum().item()
    val_loss = np.mean(losses)
    val_acc = 100 * correct / total
    return val_loss, val_acc


def wdist(samples, D, disc_train, label=None, cnn=None):
    if cnn is not None:
        samples = cnn(samples, conv_only=True)
    disc_sample = D(samples).data.cpu().numpy()
    # print(disc_train)
    wd = disc_sample
    # wd = np.abs(disc_sample - disc_train)
    # print('wd={}'.format(wd))
    return wd


def wdist_test(test_iter, D=None, cnn=None, n_test_iters=None, train_iter=None, n_train_iters=1, show=False,
               disc_train=None, num_classes=NUM_CLASSES):
    wds_train = {key: [] for key in range(num_classes)}
    # print('disc_train={}'.format(disc_train))
    if train_iter is not None:
        print('train iter # of batches: {}, running for {} iterations'.format(len(train_iter), n_train_iters))
        start_time = timer()
        print('start timer')
        for i in range(n_train_iters):
            batch = next(train_iter)
            data = batch[0]
            data = data.to(device)
            if ISCONV: data = cnn(data, conv_only=True)
            label = batch[1].cpu().data.numpy()
            if num_classes == 1: label = np.zeros(label.shape[0])
            wd = wdist(data, D, disc_train)
            for w, l in zip(wd, label):
                wds_train[l].append(w)
            if i < 5: print('mean disc of train dataset_iter batch: {}'.format(np.mean(wd)))
    means_train = {}
    for label in wds_train.keys():
        means_train[label] = np.mean(wds_train[label])
        print('class {}:\n\tmean test disc: {}, var: {}, min: {}, max: {}, time: {}'
              .format(label, means_train[label], np.var(wds_train[label]), np.min(wds_train[label]),
                      np.max(wds_train[label]), timer() - start_time))
    wds_test = {key: [] for key in range(num_classes)}  # []
    batch_n = 0
    print('test iter # of batches: {}, running for {} iterations'.format(len(test_iter), n_test_iters))
    while True:
        batch_n += 1
        batch = next(test_iter, None)
        if batch is None or n_test_iters is not None and n_test_iters < batch_n: break
        data = batch[0]
        data = data.to(device)
        if ISCONV: data = cnn(data, conv_only=True)
        label = batch[1].cpu().data.numpy()
        if num_classes == 1: label = np.zeros(label.shape[0])
        wd = wdist(data, D, disc_train)
        for w, l in zip(wd, label):
            wds_test[l].append(w)
        # wds_top.extend(wd)
        if batch_n < 6: print('mean disc of test dataset_iter batch: {}'.format(np.mean(wd)))
    means_test = {}
    for label in wds_test.keys():
        means_test[label] = np.mean(wds_test[label])
        print('class {}:\n\tmean test disc: {}, var: {}, min: {}, max: {}'
              .format(label, means_test[label], np.var(wds_test[label]), np.min(wds_test[label]),
                      np.max(wds_test[label])))
        wds_top_2trainmean = np.abs(np.subtract(wds_test[label], np.mean(wds_train[label])))
        print('\tmean test disc 2 train mean: {}, var: {}, min: {}, max: {}'
              .format(np.mean(wds_top_2trainmean), np.var(wds_top_2trainmean), np.min(wds_top_2trainmean),
                      np.max(wds_top_2trainmean)))
    return wds_test, means_test, wds_train, means_train


def gan_test():
    num_classes = 1
    counter = init_counter(D, num_classes=num_classes)
    print('counter:\tmeans: {}, vars: {}, samples: {}'.format(counter.get_mean(), counter.get_var(), counter.get_t()))
    wds_test, means_test, wds_train, means_train \
        = wdist_test(iter(LOADER_TEST), D=D, cnn=cnn, n_test_iters=len(LOADER_TEST), train_iter=iter(LOADER_TRAIN),
                     n_train_iters=len(LOADER_TRAIN), disc_train=None, show=False, num_classes=num_classes)
    plot_statistics(wds_train, means_train, wds_test, means_test)


def congan_test():
    num_classes = 1
    counter = init_counter(D, num_classes=num_classes)
    print('counter:\tmeans: {}, vars: {}, samples: {}'.format(counter.get_mean(), counter.get_var(), counter.get_t()))
    wds_test, means_test, wds_train, means_train \
        = wdist_test(iter(LOADER_TEST), D=D, cnn=cnn, n_test_iters=len(LOADER_TEST), train_iter=iter(LOADER_TRAIN),
                     n_train_iters=len(LOADER_TRAIN), disc_train=None, show=False, num_classes=num_classes)
    plot_statistics(wds_train, means_train, wds_test, means_test)


def run():
    # CNN only
    if TRAIN_CNN:
        train_cnn_only()
        torch.save(cnn, './cnn_basic.pt')
        torch.save(optimizer_cnn.state_dict(), './cnn_opt.pt')

    # GAN only
    if TRAIN_GAN:
        # pass
        train_gan(D, G)
    else:
        pass
        # D = torch.load(LOAD_FILE_D)

    # initialize counter
    counter = init_counter(D)

    # check CNN on the UPPER dataset
    if TRAIN_CNN:
        total, correct = 0, 0
        with torch.no_grad():
            cnn.eval()
            for batch in TRAIN_LOADER_ALL:
                data = batch[0].to(device)
                labels = batch[1].long().to(device)
                output = cnn(data)
                _, predicted = torch.max(output.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()

        acc = 100 * correct / total
        print('CNN accuracy on UPPER dataset: {:.2f}'.format(acc))

    wds_test, wds_test_mean, wds_train, wds_train_mean \
        = wdist_test(iter(LOADER_TEST), D=D, cnn=None, n_test_iters=len(LOADER_TEST),
                     train_iter=iter(LOADER_TRAIN), n_train_iters=len(LOADER_TRAIN),
                     disc_train=counter.get_mean(), show=False)

    plot_statistics(wds_train, wds_train_mean, wds_test, wds_test_mean)


def plot_statistics(wds_train, wds_train_mean, wds_test, wds_test_mean):
    # statistics of Wassertein distance on the BOTTOM set
    for label in wds_train.keys():
        wds_train_var = np.var(wds_train[label])
        wds_train_median = np.median(wds_train[label])
        wds_train_max, wds_train_min = np.max(wds_train[label]), np.min(wds_train[label])
        gauss_train_x = np.linspace(wds_train_mean[label] - 5 * wds_train_var,
                                    wds_train_mean[label] + 5 * wds_train_var, 100)
        gauss_train_y = mlab.normpdf(gauss_train_x, wds_train_mean[label], wds_train_var)
        dist_sigma_train = np.subtract(wds_train[label], wds_train_mean[label])
        dist_sigma_train = np.divide(dist_sigma_train, wds_train_var)
        dist_sigma_train_mu, dist_sigma_train_sigma = np.mean(dist_sigma_train), np.var(dist_sigma_train)
        gauss_train_x_dist = np.linspace(dist_sigma_train_mu - 5 * dist_sigma_train_sigma,
                                         dist_sigma_train_mu + 5 * dist_sigma_train_sigma, 100)
        gauss_train_y_dist = mlab.normpdf(gauss_train_x_dist, dist_sigma_train_mu, dist_sigma_train_sigma)

        # evaluate Wasserstein distance of the UPPER set
        wds_test_var = np.var(wds_test[label])
        wds_test_median = np.median(wds_test[label])
        wds_test_max, wds_test_min = np.max(wds_test[label]), np.min(wds_test[label])
        gauss_test_x = np.linspace(wds_test_mean[label] - 5 * wds_test_var, wds_test_mean[label] + 5 * wds_test_var,
                                   100)
        gauss_test_y = mlab.normpdf(gauss_test_x, wds_test_mean[label], wds_test_var)
        dist_sigma_test = np.subtract(wds_test[label], wds_train_mean[label])
        dist_sigma_test = np.divide(dist_sigma_test, wds_train_var)
        dist_sigma_test_mu, dist_sigma_test_sigma = np.mean(dist_sigma_test), np.var(dist_sigma_test)
        gauss_test_x_dist = np.linspace(dist_sigma_test_mu - 5 * dist_sigma_test_sigma,
                                        dist_sigma_test_mu + 5 * dist_sigma_test_sigma, 100)
        gauss_test_y_dist = mlab.normpdf(gauss_test_x_dist, dist_sigma_test_mu, dist_sigma_test_sigma)

        std_train, std_test = np.sqrt(wds_train_var), np.sqrt(wds_test_var)
        print(
            'test: u={:.5f},sigma={:.5f}, median={:.5f}, min={:.5f}, max={:.5f}\n'
            'train: u={:.5f},sigma={:.5f}, median={:.5f}, min={:.5f}, max={:.5f}\n overlap of train in {}'.format(
                wds_test_mean[label], wds_test_var, wds_test_median, wds_test_min, wds_test_max,
                wds_train_mean[label], wds_train_var, wds_train_median, wds_train_min, wds_train_max,
                intersect_area(find_itersect_points(m1=wds_train_mean, m2=wds_test_mean,
                                                    std1=std_train, std2=std_test),
                               mean_train=wds_train_mean, mean_test=wds_test_mean, std_train=std_train)))

        plt.clf()
        plt.plot(dist_sigma_train, np.zeros(len(dist_sigma_train)), 'rx', label='train distances')
        plt.plot(gauss_train_x_dist, gauss_train_y_dist, 'r', label='train gauss')
        plt.plot(dist_sigma_test, np.zeros(len(dist_sigma_test)), 'bx', label='test distances')
        plt.plot(gauss_test_x_dist, gauss_test_y_dist, 'b', label='test gauss')
        plt.legend(loc='best')
        plt.title('distance to the mean value of train distribution measured in variances\nlabel: {}'.format(label))
        if len(wds_train.keys()) == 1:
            fname = './experiment_gauss_variances_{}.jpg'.format(MODE)
            plt.savefig(fname)
            print('saved fig to {}'.format(fname))
        else:
            fname = './experiment_gauss_variances_{}_label_{}.jpg'.format(MODE, label)
            plt.savefig(fname)
            print('saved fig to {}'.format(fname))

        plt.clf()
        plt.plot(wds_train[label], np.zeros(len(wds_train[label])), 'rx', label='train discriminator')
        plt.plot(gauss_train_x, gauss_train_y, 'r', label='train gauss')
        plt.plot(wds_test[label], np.zeros(len(wds_test[label])), 'bx', label='test discriminator')
        plt.plot(gauss_test_x, gauss_test_y, 'b', label='test gauss')
        plt.legend(loc='best')
        plt.title('D output, label: {}\ntrain: mu={:.3f}, sigma={:.3f}; test: mu={:.3f}, sigma={:.3f}'
                  .format(label, wds_train_mean[label], wds_train_var, wds_test_mean[label], wds_test_var))
        if len(wds_train.keys()) == 1:
            fname = './experiment_gauss_discriminator-output_{}.jpg'.format(MODE)
            plt.savefig(fname)
            print('saved fig to {}'.format(fname))
        else:
            fname = './experiment_gauss_discriminator-output_{}_label_{}.jpg'.format(MODE, label)
            plt.savefig(fname)
            print('saved fig to {}'.format(fname))


if __name__ == '__main__':
    # run()
    gan_test()
    f.close()
    sys.stdout.close()
