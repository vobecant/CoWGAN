import os, sys

sys.path.append(os.getcwd())
out_fname = open('./result/GEOM_3D_LARGE/log-pipeline-GAN_fc_FCG.txt', 'w')

import numpy as np
import matplotlib

matplotlib.use('agg')
from matplotlib import pyplot as plt
from models.CNN import CNN
import torch
import torchvision
from torch import nn
from torch import autograd
from torch import optim
from torchvision import transforms, datasets
from torch.autograd import grad
from timeit import default_timer as timer
from models.dataset import GeomDataset, GeomDataset_new, GeomDataset3d
import libs as lib
from libs.save import Saver
from libs.sampler import MyWeightedRandomSampler as MyWeightSampler
from libs.sampler import MyDataLoader
from libs.mean_counter import MeanVarCounter
from models.wgan import *
from models.conwgan import GoodDiscriminator as GoodDiscriminatorCon
from models.conwgan import GoodGenerator as GoodGeneratorCon
import torch.nn.init as init
from libs.plot import Plotter
from tensorboardX import SummaryWriter
from models.types import *

# ========= DATA DIRECTORIES ============
DATA_SPECIF = DATASET_types.GEOM_2D_CIRCLES
DATASET_TYPE = DATASET_types.GEOM_2D_CIRCLES
CSV_SPECIF_TRAIN = 'bb-lb'
CSV_SPECIF_TEST = 'bb-rt'
# training files
TRAIN_DIR_TRAIN = DATA_SPECIF.value + '/train'  # DATASET_types.GEOM_3D_BOTTOM.value + '/train'  # './datasets/' + DATA_SPECIF + '/train'
CSV_FILE_TRAIN_TRAIN = TRAIN_DIR_TRAIN + '/geom_data.csv'  # './datasets/{}_train.csv'.format(CSV_SPECIF_TRAIN)
VAL_DIR_TRAIN = DATA_SPECIF.value + '/val'
CSV_FILE_VAL_TRAIN = VAL_DIR_TRAIN + '/geom_data.csv'  # './datasets/{}_val.csv'.format(CSV_SPECIF_TRAIN)
# test files
TRAIN_DIR_TEST = TRAIN_DIR_TRAIN
CSV_FILE_TRAIN_TEST = './datasets/{}_train.csv'.format(CSV_SPECIF_TEST)
# rest
TRAIN_DIR_ALL = DATASET_types.GEOM_3D_UNLIMITED.value + '/train'
CSV_FILE_TRAIN_ALL = TRAIN_DIR_ALL + '/{}_train.csv'.format(CSV_SPECIF_TRAIN)
VAL_DIR_ALL = '/home/skovirad/dataset3d/3d_unlimited/valid'
CSV_FILE_VAL_ALL = VAL_DIR_ALL + '/geom_data.csv'
# output paths
OUTPUT_PATH_CNN = './result/' + DATA_SPECIF.name + '/CNN-3D_fc_FCG/'
OUTPUT_PATH_GAN = './result/' + DATA_SPECIF.name + '/GAN-2D/'
OUTPUT_PATH_CONGAN = './result/' + DATA_SPECIF.name + '/CONGAN-3D_img/'
OUTPUT_PATH_BOTH = './result/' + DATA_SPECIF.name + '/BOTH/'
OUTPUT_PATH_CNN_FURTHER = './result/' + DATA_SPECIF.name + '/CNN-only-further/'
# paths to LOAD files
LOAD_FILE_CNN = './result/splitted-img/CNN/cnn_last.pt'
LOAD_FILE_G = './result/geom3D/CONGAN-3D_img/G_last.pt'
LOAD_FILE_D = './result/geom3D/CONGAN-3D_img/D_last.pt'
LOAD_FILE_CNN_OPT = './result/splitted-img/CNN/cnn_opt_last.pt'
LOAD_FILE_G_OPT = './result/geom3D/CONGAN-3D_img/G_opt_last.pt'
LOAD_FILE_D_OPT = './result/geom3D/CONGAN-3D_img/D_opt_last.pt'
CNN_PARAMS_FILE = './result/splitted-img/CNN/save_last.npy'
GAN_PARAMS_FILE = './result/geom3D/CONGAN-3D_img/save_last.npy'
USE3D = True

# ========= CONSTANTS ===========
LOAD = False
MULTIPLE_GPU = True

# CNN parameters
NUM_CLASSES = 4
N_EPOCHS_CNN = 10
LR_CNN = 1e-4
BATCH_SIZE = 64
PLOT_FREQ_CNN = 1
SAVE_FREQ_CNN = 1
MAKE_SAVES_CNN = True

# Device configuration
cuda_available = torch.cuda.is_available()
device = torch.device('cuda' if cuda_available else 'cpu')

# GAN parameters
RESTORE_MODE = False  # if True, it will load saved cnn from OUT_PATH and continue to train
USE_VAR_PENALTY = False
WEIGHT_CRITERION = WEIGHT_CRITERION_types.WGAN
LR_G, LR_D = 1e-4, 1e-4
START_ITER = 0  # starting iteration
MODE = GAN_types.WGAN_GP_conv  # GAN_types.WGAN_GP_conv
ISCONV = MODE in [GAN_types.WGAN_GP_conv, GAN_types.ConWGAN_GP_conv, GAN_types.WGAN_GP_conv_FCG]
DIM = 64  # Model dimensionality
CRITIC_ITERS = 5  # How many iterations to train the critic for
GENER_ITERS = 1
N_GPUS = 1  # Number of GPUs
NUM_ITER_GAN = 50000  # How many iterations to train for
END_ITER_GAN = START_ITER + NUM_ITER_GAN
LAMBDA = 10  # Gradient penalty lambda hyperparameter
OUTPUT_DIM = 64 * 64 * 3  # Number of pixels in each image
VAL_FREQ_GAN = 50
PLOT_DISTR_FREQ = 1000
DIM_CONV = 128
FC_DIM = 128
IM_SIZE_CONV = 8
N_ROWS = 8
FIX_NOISE_SIZE = N_ROWS ** 2

# CONGAN parameters only
ACGAN_SCALE = 1.  # How to scale the critic's ACGAN loss relative to WGAN loss
ACGAN_SCALE_G = 1.  # How to scale generator's ACGAN loss relative to WGAN loss

# training BOTH together
N_EPOCHS_BOTH = 10
SAVE_FREQ_BOTH = 1
NUM_SAMPLES_WEIGHT = 20000


# ========= HELPER FUNCTIONS ===========
def print(text):
    sys.stdout.write('{}\n'.format(text))
    out_fname.write('{}\n'.format(text))
    out_fname.flush()


def load_my_data(path_to_folder, csvfile, batch_size=BATCH_SIZE, shuffle=True, drop_last=True, is3d=False):
    data_transform = transforms.Compose([
        transforms.Resize(64),
        transforms.CenterCrop(64),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
    ])
    if is3d:
        dataset = GeomDataset3d(csvfile, path_to_folder, data_transform)
    else:
        dataset = GeomDataset_new(csvfile, path_to_folder, data_transform)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=shuffle, num_workers=5,
                                                 drop_last=drop_last, pin_memory=True)
    print('created dataset loader from {}, number of samples: {}, shuffle: {}'
          .format(path_to_folder, len(dataset), shuffle))
    return dataset_loader


def load_my_data_weight_sampler(path_to_folder, csvfile, model, num_samples=None, batch_size=BATCH_SIZE, is3d=False):
    data_transform = transforms.Compose([
        transforms.Resize(64),
        transforms.CenterCrop(64),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
    ])
    if is3d:
        dataset = GeomDataset3d(csvfile, path_to_folder, data_transform)
    else:
        dataset = GeomDataset(csvfile, path_to_folder, data_transform)
    weights = list(np.ones(len(dataset)))
    # print('dataset size: {}, WEIGHTS shape: {}, num_samples: {}, batch_size: {}'
    #     .format(len(dataset), len(weights), num_samples, batch_size))
    sampler = MyWeightSampler(weights, num_samples=num_samples, replacement=False)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, sampler=sampler, num_workers=0,
                                                 drop_last=True, pin_memory=True)
    return dataset_loader, sampler


def training_data_loader(csv, is3d=USE3D):
    return load_my_data(TRAIN_DIR_TRAIN, csv, is3d=is3d)


def val_data_loader(csv, is3d=USE3D):
    return load_my_data(VAL_DIR_TRAIN, csv, is3d=is3d)


def gen_rand_noise():
    noise = torch.randn(BATCH_SIZE, 128)
    noise = noise.to(device)
    return noise


def gen_rand_noise_with_label(label=None, size=BATCH_SIZE):
    if label is None:
        label = np.random.randint(0, NUM_CLASSES, size)
    # attach label into noise
    noise = np.random.normal(0, 1, (size, 128))
    prefix = np.zeros((size, NUM_CLASSES))
    prefix[np.arange(size), label] = 1
    noise[np.arange(size), :NUM_CLASSES] = prefix[np.arange(size)]

    noise = torch.from_numpy(noise).float()
    noise = noise.to(device)

    return noise


def calc_gradient_penalty(netD, real_data, fake_data):
    alpha = torch.rand(BATCH_SIZE, 1)
    alpha = alpha.expand(BATCH_SIZE, int(real_data.nelement() / BATCH_SIZE)).contiguous()
    if MODE in [GAN_types.WGAN_GP_conv, GAN_types.ConWGAN_GP_conv, GAN_types.WGAN_GP_conv_FCG]:
        alpha = alpha.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
        fake_data = fake_data.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
    elif MODE == GAN_types.WGAN_GP_fc_FCG:
        alpha = alpha.view(BATCH_SIZE, FC_DIM)
        fake_data = fake_data.view(BATCH_SIZE, FC_DIM)
    elif MODE == GAN_types.WGAN_GP:
        alpha = alpha.view(BATCH_SIZE, 3, DIM, DIM)
        fake_data = fake_data.view(BATCH_SIZE, 3, DIM, DIM)
    else:
        raise Exception('Invalid MODE!')
    alpha = alpha.to(device)

    interpolates = alpha * real_data.detach() + ((1 - alpha) * fake_data.detach())

    interpolates = interpolates.to(device)
    interpolates.requires_grad_(True)

    disc_interpolates = netD(interpolates)

    gradients = autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                              grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]

    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * LAMBDA
    return gradient_penalty


def calc_gradient_penalty_conwgan(netD, real_data, fake_data):
    alpha = torch.rand(BATCH_SIZE, 1)
    alpha = alpha.expand(BATCH_SIZE, int(real_data.nelement() / BATCH_SIZE)).contiguous()
    if MODE == GAN_types.ConWGAN_GP_conv:
        alpha = alpha.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
        fake_data = fake_data.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
    elif MODE == GAN_types.ConWGAN_GP:
        alpha = alpha.view(BATCH_SIZE, 3, DIM, DIM)
        fake_data = fake_data.view(BATCH_SIZE, 3, DIM, DIM)
    else:
        raise Exception('Invalid MODE!')
    alpha = alpha.to(device)

    fake_data = fake_data.view(BATCH_SIZE, 3, DIM, DIM)
    interpolates = alpha * real_data.detach() + ((1 - alpha) * fake_data.detach())

    interpolates = interpolates.to(device)
    interpolates.requires_grad_(True)

    disc_interpolates, _ = netD(interpolates)

    gradients = autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                              grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]

    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * LAMBDA
    return gradient_penalty


def init_counter():
    # print('Initializing counter.')
    st = timer()
    loader = load_my_data(TRAIN_DIR_TRAIN, CSV_FILE_TRAIN_TRAIN, is3d=USE3D)
    results, labels = [], []
    with torch.no_grad():
        for batch in loader:
            data = batch[0].to(device)
            labels.extend(batch[1].cpu().data.numpy())
            if MODE in [GAN_types.WGAN_GP_conv, GAN_types.WGAN_GP_conv_FCG]: data = cnn(data, conv_only=True)
            if MODE == GAN_types.WGAN_GP_fc_FCG: data = cnn(data, fc1_out=True)
            if MODE == GAN_types.ConWGAN_GP:
                Dout, aux_out = D(data).cpu().data.numpy()
                if WEIGHT_CRITERION == WEIGHT_CRITERION_types.WGAN:
                    results.extend(Dout)
                elif WEIGHT_CRITERION == WEIGHT_CRITERION_types.ConWGAN:
                    results.extend(aux_out)
                else:
                    raise Exception('Unsupported WEIGHT_CRITERION for conwgan-gp!')
            else:
                Dout = D(data).cpu().data.numpy()
                if WEIGHT_CRITERION == WEIGHT_CRITERION_types.ConWGAN:
                    results.extend(Dout)
                else:
                    raise Exception('Unsupported WEIGHT_CRITERION for {}!'.format(MODE))
    counter = MeanVarCounter(num_classes=NUM_CLASSES, samples=results, labels=labels)
    print('Counter initialized in {}.'.format(timer() - st))
    return counter


def weights_init(m):
    if isinstance(m, MyConvo2d):
        if m.conv.weight is not None:
            if m.he_init:
                init.kaiming_uniform_(m.conv.weight)
            else:
                init.xavier_uniform_(m.conv.weight)
        if m.conv.bias is not None:
            init.constant_(m.conv.bias, 0.0)
    if isinstance(m, nn.Linear):
        if m.weight is not None:
            init.xavier_uniform_(m.weight)
        if m.bias is not None:
            init.constant_(m.bias, 0.0)


def generate_image(netG, noise=None, conv=False):
    if noise is None:
        noise = gen_rand_noise()

    with torch.no_grad():
        noisev = noise
    samples = netG(noisev)
    if MODE == GAN_types.WGAN_GP_fc_FCG:
        samples = samples.view(BATCH_SIZE, FC_DIM)
    elif not conv:
        samples = samples.view(BATCH_SIZE, 3, 64, 64)
    else:
        samples = samples.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
        # print('samples: {}, {}'.format(type(samples),samples.size()))
        samples = samples[:, 0, :, :].view(BATCH_SIZE, 1, IM_SIZE_CONV, IM_SIZE_CONV)
        # print('samples: {}, {}'.format(type(samples), samples.size()))
    samples = samples * 0.5 + 0.5
    return samples


def generate_image_congan(netG, noise=None):
    if noise is None:
        rand_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
        noise = gen_rand_noise_with_label(rand_label)
    with torch.no_grad():
        noisev = noise
    samples = netG(noisev)
    samples = samples.view(BATCH_SIZE, 3, DIM, DIM)

    samples = samples * 0.5 + 0.5

    return samples


# ======= SET LOADERS, LOAD NETS,... =======
cuda_available = torch.cuda.is_available()
fixed_noise = gen_rand_noise()
fixed_label = []
for c in range(FIX_NOISE_SIZE):
    fixed_label.append(c % NUM_CLASSES)
fixed_noise_congan = gen_rand_noise_with_label(fixed_label, size=FIX_NOISE_SIZE)
VAL_LOADER_CNN_TRAIN = load_my_data(VAL_DIR_TRAIN, CSV_FILE_VAL_TRAIN, is3d=USE3D)
# VAL_LOADER_CNN_ALL = load_my_data(VAL_DIR_ALL, CSV_FILE_VAL_ALL, is3d=USE3D)
TRAIN_LOADER_TRAIN = load_my_data(TRAIN_DIR_TRAIN, CSV_FILE_TRAIN_TRAIN, is3d=USE3D)
TRAIN_LOADER_TEST = load_my_data(TRAIN_DIR_TEST, CSV_FILE_TRAIN_TEST, is3d=USE3D)
# TRAIN_LOADER_ALL = load_my_data(TRAIN_DIR_ALL, CSV_FILE_TRAIN_ALL, is3d=USE3D)
# train_loader_gan_noshuffle = load_my_data(TRAIN_DIR_ALL, CSV_FILE_TRAIN_ALL, shuffle=False, drop_last=False)
criterion_cnn = nn.CrossEntropyLoss()

if LOAD:
    cnn = torch.load(LOAD_FILE_CNN)
    print('loaded CNN from {}'.format(LOAD_FILE_CNN))
    G = torch.load(LOAD_FILE_G)
    print('loaded generator from {}'.format(LOAD_FILE_G))
    D = torch.load(LOAD_FILE_D)
    print('loaded discriminator from {}'.format(LOAD_FILE_D))
    cnn_params = np.load(CNN_PARAMS_FILE).item()
    gan_params = np.load(GAN_PARAMS_FILE).item()
    optimizer_cnn = optim.Adam(cnn.parameters(), lr=LR_CNN)
    optimizer_cnn.load_state_dict(torch.load(LOAD_FILE_CNN_OPT))
    optimizer_g = torch.optim.Adam(G.parameters(), lr=LR_G, betas=(0, 0.9))
    optimizer_g.load_state_dict(torch.load(LOAD_FILE_G_OPT))
    optimizer_d = torch.optim.Adam(D.parameters(), lr=LR_D, betas=(0, 0.9))
    optimizer_d.load_state_dict(torch.load(LOAD_FILE_D_OPT))
    START_ITER = len(gan_params['wdist']) + 1
    START_ITER = 4649
    END_ITER_GAN = START_ITER + NUM_ITER_GAN
    print('loaded nets, GAN START_ITER: {}, END_ITER_GAN: {}'.format(START_ITER, END_ITER_GAN))
else:
    cnn = CNN(num_classes=NUM_CLASSES)
    if MODE == GAN_types.WGAN_GP_conv:
        G = GoodGeneratorConv(conv_dim=512)
        D = GoodDiscriminatorConv(conv_dim=512)
    elif MODE == GAN_types.WGAN_GP:
        G = GoodGenerator()
        D = GoodDiscriminator()
    elif MODE == GAN_types.WGAN_GP_conv_FCG:
        G = FCGenerator(output_dim=DIM_CONV * IM_SIZE_CONV * IM_SIZE_CONV)
        D = GoodDiscriminatorConv()
    elif MODE == GAN_types.WGAN_GP_fc_FCG:
        G = FCGenerator(output_dim=FC_DIM)
        D = GoodDiscriminatorConv()
    elif MODE == GAN_types.ConWGAN_GP:
        G = GoodGeneratorCon()
        D = GoodDiscriminatorCon(num_class=NUM_CLASSES)
    optimizer_cnn = optim.Adam(cnn.parameters(), lr=LR_CNN)
    optimizer_g = torch.optim.Adam(G.parameters(), lr=LR_G, betas=(0, 0.9))
    optimizer_d = torch.optim.Adam(D.parameters(), lr=LR_D, betas=(0, 0.9))
    G.apply(weights_init)
    D.apply(weights_init)
if torch.cuda.device_count() > 1 and MULTIPLE_GPU:
    print('Use {} GPUs'.format(torch.cuda.device_count()))
    cnn = nn.DataParallel(cnn)
    G = nn.DataParallel(G)
    D = nn.DataParallel(D)
G = G.to(device)
D = D.to(device)
cnn.to(device)

aux_criterion = nn.CrossEntropyLoss()  # nn.NLLLoss()

one = torch.FloatTensor(1)
mone = one * -1

one = one.to(device)
mone = mone.to(device)

counter = None
writer = SummaryWriter()


# ======= TRAINING FUNCTIONS ========

def train_cnn_only():
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver = Saver('val_loss', better_value_fnc, OUTPUT_PATH_CNN)
    plotter_cnn = Plotter()
    print('start training CNN only')
    optimizer = optimizer_cnn  # optim.Adam(cnn.parameters(), lr=LEARNING_RATE)
    for epoch in range(N_EPOCHS_CNN):
        start_time = timer()
        # print('epoch {}\ttrain start'.format(epoch))
        ep_start = timer()
        # ------ training -------
        train_losses = np.asarray([])
        total_train, correct_train = 0, 0
        cnn.train()
        for i, batch in enumerate(TRAIN_LOADER_TRAIN):
            # if i == 5: break
            # Forward pass
            images = batch[0]
            labels = batch[1].long()
            # print(labels)
            images = images.to(device)
            labels = labels.to(device)
            # print(labels)
            outputs = cnn(images)
            _, predicted = torch.max(outputs.data, 1)

            # forward pass
            loss = criterion_cnn(outputs, labels)
            train_losses = np.append(train_losses, loss.item())

            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            total_train += labels.size(0)
            correct_train += (predicted == labels).sum().item()

        train_acc = 100 * correct_train / total_train
        train_loss = np.mean(train_losses)

        # ------ validation ------
        # print('epoch {}\tvalidation start'.format(epoch))
        val_loss, val_acc = validate_cnn_only(TRAIN_LOADER_TRAIN, cnn, criterion_cnn)

        # ------ create plots ------
        plotter_cnn.plot(OUTPUT_PATH_CNN + 'time', timer() - start_time)
        plotter_cnn.plot(OUTPUT_PATH_CNN + 'train_loss', train_loss)
        plotter_cnn.plot(OUTPUT_PATH_CNN + 'val_loss', val_loss)
        plotter_cnn.plot(OUTPUT_PATH_CNN + 'train_acc', train_acc)
        plotter_cnn.plot(OUTPUT_PATH_CNN + 'val_acc', val_acc)
        if epoch % PLOT_FREQ_CNN == (PLOT_FREQ_CNN - 1):
            plotter_cnn.flush(len(OUTPUT_PATH_CNN))
        plotter_cnn.tick()

        # ------ save -------
        if MAKE_SAVES_CNN:
            saver.update('cnn', cnn)
            saver.update('cnn_w', cnn.state_dict())
            saver.update('cnn_opt', optimizer.state_dict())
            saver.update('train_loss', train_loss)
            saver.update('val_loss', val_loss)
            saver.update('train_acc', train_acc)
            saver.update('val_acc', val_acc)
            saver.update('epoch', epoch)
            if epoch % SAVE_FREQ_CNN == (SAVE_FREQ_CNN - 1):
                saver.flush()
            saver.tick()

        # ----- print info ------
        print('Epoch [{}/{}], Train Loss: {:.6f}, Train Acc: {:.2f}, Val Loss: {:.6f}, Val Acc: {:.2f}, time: {:.3f}'
              .format(epoch + 1, N_EPOCHS_CNN, train_loss, train_acc, val_loss, val_acc, timer() - ep_start))


def train_cnn_only_further():
    cnn = torch.load('./cnn_basic.pt')
    optimizer = optim.Adam(cnn.parameters())
    optimizer.load_state_dict(torch.load('./cnn_opt.pt'))
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver = Saver('val_loss', better_value_fnc, OUTPUT_PATH_CNN_FURTHER)
    plotter = Plotter()
    print('start training CNN only')
    for epoch in range(N_EPOCHS_BOTH):
        start_time = timer()
        # print('epoch {}\ttrain start'.format(epoch))
        ep_start = timer()
        # ------ training -------
        train_losses = np.asarray([])
        total_train, correct_train = 0, 0
        cnn.train()
        n_samples = 0
        for i, batch in enumerate(TRAIN_LOADER_ALL):
            # if i == 5: break
            # Forward pass
            images = batch[0]
            labels = batch[1].long()
            # print(labels)
            images = images.to(device)
            labels = labels.to(device)
            # print(labels)
            outputs = cnn(images)
            _, predicted = torch.max(outputs.data, 1)

            # forward pass
            loss = criterion_cnn(outputs, labels)
            train_losses = np.append(train_losses, loss.item())

            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            total_train += labels.size(0)
            correct_train += (predicted == labels).sum().item()
            n_samples += len(labels)
            if n_samples > NUM_SAMPLES_WEIGHT: break

        train_acc = 100 * correct_train / total_train
        train_loss = np.mean(train_losses)

        # ------ validation ------
        # print('epoch {}\tvalidation start'.format(epoch))
        val_loss, val_acc = validate_cnn_only(TRAIN_LOADER_ALL, cnn, criterion_cnn)

        # ------ create plots ------
        plotter.plot(OUTPUT_PATH_CNN_FURTHER + 'time', timer() - start_time)
        plotter.plot(OUTPUT_PATH_CNN_FURTHER + 'train_loss', train_loss)
        plotter.plot(OUTPUT_PATH_CNN_FURTHER + 'val_loss', val_loss)
        plotter.plot(OUTPUT_PATH_CNN_FURTHER + 'train_acc', train_acc)
        plotter.plot(OUTPUT_PATH_CNN_FURTHER + 'val_acc', val_acc)
        if epoch % PLOT_FREQ_CNN == (PLOT_FREQ_CNN - 1):
            plotter.flush(len(OUTPUT_PATH_CNN_FURTHER))
        plotter.tick()

        # ------ save -------
        saver.update('cnn', cnn)
        saver.update('cnn_w', cnn.state_dict())
        saver.update('cnn_opt', optimizer.state_dict())
        saver.update('train_loss', train_loss)
        saver.update('val_loss', val_loss)
        saver.update('train_acc', train_acc)
        saver.update('val_acc', val_acc)
        saver.update('epoch', epoch)
        if epoch % SAVE_FREQ_CNN == (SAVE_FREQ_CNN - 1):
            saver.flush()
        saver.tick()

        # ----- print info ------
        print('Epoch [{}/{}], Train Loss: {:.6f}, Train Acc: {:.2f}, Val Loss: {:.6f}, Val Acc: {:.2f}, time: {:.3f}'
              .format(epoch + 1, N_EPOCHS_BOTH, train_loss, train_acc, val_loss, val_acc, timer() - ep_start))


def validate_cnn_only(val_loader, model, criterion, show=False):
    # switch to evaluate mode
    model.eval()
    losses = np.asarray([])
    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(val_loader):
            input = batch[0]
            target = batch[1].long()
            input = input.to(device)
            target = target.to(device)
            # compute output
            output = model(input)
            _, predicted = torch.max(output.data, 1)
            loss = criterion(output, target)
            losses = np.append(losses, loss.cpu().data.numpy())
            total += target.size(0)
            correct += (predicted == target).sum().item()
    val_loss = np.mean(losses)
    val_acc = 100 * correct / total
    return val_loss, val_acc


# Reference: https://github.com/caogang/wgan-gp/blob/master/gan_cifar10.py
def train_gan(D, G):
    best_wdist = np.inf
    better_value_fnc = lambda best_wd, new_wd: new_wd < best_wd
    saver = Saver('wdist', better_value_fnc, OUTPUT_PATH_GAN, START_ITER)
    plotter = Plotter(start_iter=START_ITER, output_path=OUTPUT_PATH_GAN)
    dataloader = load_my_data(TRAIN_DIR_TRAIN, CSV_FILE_TRAIN_TRAIN, is3d=False)
    val_loader = load_my_data(VAL_DIR_TRAIN, CSV_FILE_VAL_TRAIN, is3d=False)
    dataiter = iter(dataloader)
    print('start training GAN only, mode: {}, use variance penalty: {}, iterations: {}, train dir: {}, val dir: {}'
          .format(MODE, USE_VAR_PENALTY, NUM_ITER_GAN, TRAIN_DIR_TRAIN, VAL_DIR_TRAIN))
    # freeze CNN
    for p in cnn.parameters():
        p.requires_grad_(False)  # freeze CNN

    for iteration in range(START_ITER, NUM_ITER_GAN):
        start_time = timer()
        # print("Iter: " + str(iteration))
        start = timer()

        # ---------------------TRAIN G------------------------
        for p in D.parameters():
            p.requires_grad_(False)  # freeze D

        gen_cost = None
        for i in range(GENER_ITERS):
            # print("Generator iters: " + str(i))
            G.zero_grad()
            noise = gen_rand_noise()
            noise.requires_grad_(True)
            fake_data = G(noise)
            gen_cost = D(fake_data)
            gen_cost = gen_cost.mean().view(-1)
            gen_cost.backward(mone)
            gen_cost = -gen_cost

        optimizer_g.step()
        end = timer()
        # print('---train G elapsed time: {end - start}')

        # ---------------------TRAIN D------------------------
        for p in D.parameters():  # reset requires_grad
            p.requires_grad_(True)  # they are set to False below in training G
        for i in range(CRITIC_ITERS):
            # print("Critic iter: " + str(i))

            start = timer()
            D.zero_grad()

            # gen fake dataset_iter and load real dataset_iter
            noise = gen_rand_noise()
            with torch.no_grad():
                noisev = noise  # totally freeze G, training D
            fake_data = G(noisev).detach()
            # print('fake data size: {}'.format(fake_data.size()))
            end = timer();
            # print(out_fname'---gen G elapsed time: {end-start}')
            start = timer()
            batch = next(dataiter, None)
            if batch is None:
                dataiter = iter(dataloader)
                batch = dataiter.next()
            batch = batch[0]  # batch[1] contains labels
            real_data = batch.to(device)  # TODO: modify load_data for each loading
            if MODE in [GAN_types.WGAN_GP_conv, GAN_types.WGAN_GP_conv_FCG]: real_data = cnn(real_data, conv_only=True)
            if MODE == GAN_types.WGAN_GP_fc_FCG: real_data = cnn(real_data, fc1_out=True)
            end = timer();
            # print('---load real imgs elapsed time: {end-start}')
            start = timer()

            # train with real dataset_iter
            disc_real_out = D(real_data)
            disc_real = disc_real_out.mean()
            disc_real_var = disc_real_out.var()  # include into cost function?

            # train with fake dataset_iter
            disc_fake_out = D(fake_data)
            disc_fake = disc_fake_out.mean()
            disc_fake_var = disc_fake_out.var()  # include into cost function?

            # showMemoryUsage(0)
            # train with interpolates dataset_iter
            gradient_penalty = calc_gradient_penalty(D, real_data, fake_data)
            # showMemoryUsage(0)

            # final disc cost

            disc_cost = disc_fake - disc_real  # we want this to be as close as possible -> result should be 0
            disc_cost_var = disc_real_var + disc_fake_var  # we want discriminator to have as small variance as possible
            disc_total_cost = disc_cost + gradient_penalty
            if USE_VAR_PENALTY: disc_total_cost += disc_cost_var
            disc_total_cost.backward()
            w_dist = disc_fake - disc_real
            optimizer_d.step()
            # ------------------VISUALIZATION----------
            end = timer();
            # print(out_fname'---train D elapsed time: {end-start}')

        # ---------------VISUALIZATION---------------------
        plotter.plot('time', timer() - start_time)
        plotter.plot('train_disc_cost', disc_cost.cpu().data.numpy())
        plotter.plot('train_gen_cost', gen_cost.cpu().data.numpy())
        plotter.plot('wasserstein_distance', w_dist.cpu().data.numpy())
        plotter.plot('disc_cost_var', disc_cost_var.cpu().data.numpy())
        w_dist = np.abs(w_dist.item())

        # ---------------SAVING--------------------
        saver.update('D', D)
        saver.update('D_w', D.state_dict())
        saver.update('G', G)
        saver.update('G_w', G.state_dict())
        saver.update('wdist', w_dist)
        saver.update('D_cost', disc_cost.item())
        saver.update('G_cost', disc_cost.item())
        saver.update('D_opt', optimizer_d.state_dict())
        saver.update('G_opt', optimizer_g.state_dict())

        if w_dist < best_wdist:
            best_wdist = w_dist
            torch.save(G, OUTPUT_PATH_GAN + "generator_best_abs.pt")
            torch.save(D, OUTPUT_PATH_GAN + "discriminator_best_abs.pt")

        if iteration % VAL_FREQ_GAN == (VAL_FREQ_GAN - 1):
            dev_disc_costs = []
            for _, images in enumerate(val_loader):
                imgs = torch.Tensor(images[0])
                imgs = imgs.to(device)
                if MODE == GAN_types.WGAN_GP_conv: imgs = cnn(imgs, conv_only=True)
                with torch.no_grad():
                    imgs_v = imgs
                D_res = D(imgs_v)
                _dev_disc_cost = -D_res.mean().cpu().data.numpy()
                dev_disc_costs.append(_dev_disc_cost)
            plotter.plot('dev_disc_cost', np.mean(dev_disc_costs))
            saver.update('D_cost_val', np.mean(dev_disc_costs))
            gen_images = generate_image(G, fixed_noise, conv=ISCONV)
            torchvision.utils.save_image(gen_images, OUTPUT_PATH_GAN + 'samples_{}.png'.format(iteration), nrow=8,
                                         padding=2)
            grid_images = torchvision.utils.make_grid(gen_images, nrow=8, padding=2)
            writer.add_image('images', grid_images, iteration)

            # ----------------------flush and save----------------------
            plotter.flush(print_info=True)
            saver.flush()
        if iteration % PLOT_DISTR_FREQ == (PLOT_DISTR_FREQ - 1):
            # plot Gaussian distribution of discriminator's output over train dataset (BOTTOM) and test dataset (TOP)
            plotter.compute_gan_statistics(D, val_loader, dataloader, device, OUTPUT_PATH_GAN, iteration,
                                           file=out_fname)
        plotter.tick()
        saver.tick()


def train_congan():
    best_wdist = np.inf
    better_value_fnc = lambda best_wd, new_wd: new_wd < best_wd
    saver = Saver('wdist', better_value_fnc, OUTPUT_PATH_CONGAN, START_ITER)
    plotter = Plotter(start_iter=START_ITER)
    dataloader = training_data_loader(CSV_FILE_TRAIN_TRAIN)
    val_loader = val_data_loader(CSV_FILE_VAL_TRAIN)
    dataiter = iter(dataloader)
    print('start training ConGAN only  , mode: {}, iterations: {}'.format(MODE, NUM_ITER_GAN))
    for iteration in range(START_ITER, END_ITER_GAN):
        start_time = timer()
        # print("Iter: " + str(iteration))
        start = timer()
        # ---------------------TRAIN G------------------------
        for p in D.parameters():
            p.requires_grad_(False)  # freeze D

        gen_cost = None
        for i in range(GENER_ITERS):
            # print("Generator iters: " + str(i))
            G.zero_grad()
            f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
            noise = gen_rand_noise_with_label(f_label)
            noise.requires_grad_(True)
            fake_data = G(noise)
            gen_cost, gen_aux_output = D(fake_data)

            aux_label = torch.from_numpy(f_label).long()
            aux_label = aux_label.to(device)
            aux_errG = aux_criterion(gen_aux_output, aux_label).mean()
            gen_cost = -gen_cost.mean()
            g_cost = ACGAN_SCALE_G * aux_errG + gen_cost
            g_cost.backward()

        optimizer_g.step()
        end = timer()
        # print(out_fname'---train G elapsed time: {end - start}')
        # ---------------------TRAIN D------------------------
        for p in D.parameters():  # reset requires_grad
            p.requires_grad_(True)  # they are set to False below in training G
        for i in range(CRITIC_ITERS):
            # print("Critic iter: " + str(i))

            start = timer()
            D.zero_grad()

            # gen fake dataset_iter and load real dataset_iter
            f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
            noise = gen_rand_noise_with_label(f_label)
            with torch.no_grad():
                noisev = noise  # totally freeze G, training D
            fake_data = G(noisev).detach()
            end = timer()
            # print(out_fname'---gen G elapsed time: {end-start}')
            start = timer()
            batch = next(dataiter, None)
            if batch is None:
                dataiter = iter(dataloader)
                batch = next(dataiter)
            real_data = batch[0]  # batch[1] contains labels
            real_data.requires_grad_(True)
            real_label = batch[1].long()
            end = timer()
            # print(out_fname'---load real imgs elapsed time: {end-start}')

            start = timer()
            real_data = real_data.to(device)
            if MODE == GAN_types.ConWGAN_GP_conv:
                real_data = cnn(real_data, conv_only=True)
            real_label = real_label.to(device)

            # train with real dataset_iter
            disc_real_out, aux_output = D(real_data)
            aux_errD_real = aux_criterion(aux_output, real_label)
            errD_real = aux_errD_real.mean()
            disc_real = disc_real_out.mean()
            disc_real_var = disc_real_out.var()

            # train with fake dataset_iter
            disc_fake_out, aux_output = D(fake_data)
            # aux_errD_fake = aux_criterion(aux_output, fake_label)
            # errD_fake = aux_errD_fake.mean()
            disc_fake = disc_fake_out.mean()
            disc_fake_var = disc_fake.var()

            # showMemoryUsage(0)
            # train with interpolates dataset_iter
            gradient_penalty = calc_gradient_penalty_conwgan(D, real_data, fake_data)
            # showMemoryUsage(0)

            # final disc cost
            disc_cost = disc_fake - disc_real
            disc_acgan = errD_real  # + errD_fake
            disc_cost_var = disc_real_var + disc_fake_var
            disc_total_cost = disc_cost + ACGAN_SCALE * disc_acgan + gradient_penalty
            if USE_VAR_PENALTY: disc_total_cost += disc_cost_var
            disc_total_cost.backward()
            w_dist = disc_fake - disc_real
            optimizer_d.step()

            end = timer();
            # print(out_fname'---train D elapsed time: {end-start}')
            # ---------------VISUALIZATION---------------------
            plotter.plot(OUTPUT_PATH_CONGAN + 'time', timer() - start_time)
            plotter.plot(OUTPUT_PATH_CONGAN + 'train_disc_cost', disc_total_cost.cpu().data.numpy())
            plotter.plot(OUTPUT_PATH_CONGAN + 'train_gen_cost', gen_cost.cpu().data.numpy())
            plotter.plot(OUTPUT_PATH_CONGAN + 'wasserstein_distance', w_dist.cpu().data.numpy())
            plotter.plot(OUTPUT_PATH_CONGAN + 'disc_cost_var', disc_cost_var.cpu().data.numpy())
            w_dist = np.abs(w_dist.item())

            # ---------------SAVING--------------------
            saver.update('D', D)
            saver.update('D_w', D.state_dict())
            saver.update('G', G)
            saver.update('G_w', G.state_dict())
            saver.update('wdist', w_dist)
            saver.update('D_cost', disc_cost.item())
            saver.update('G_cost', disc_cost.item())
            saver.update('D_opt', optimizer_d.state_dict())
            saver.update('G_opt', optimizer_g.state_dict())

        if w_dist < best_wdist:
            best_wdist = w_dist
            torch.save(G, OUTPUT_PATH_CONGAN + "generator_best_abs.pt")
            torch.save(D, OUTPUT_PATH_CONGAN + "discriminator_best_abs.pt")

        if iteration % VAL_FREQ_GAN == (VAL_FREQ_GAN - 1):
            dev_disc_costs = []
            for _, images in enumerate(val_loader):
                imgs = torch.Tensor(images[0])
                imgs = imgs.to(device)
                if MODE == GAN_types.WGAN_GP_conv: imgs = cnn(imgs, conv_only=True)
                with torch.no_grad():
                    imgs_v = imgs
                D_res, _ = D(imgs_v)
                _dev_disc_cost = -D_res.mean().cpu().data.numpy()
                dev_disc_costs.append(_dev_disc_cost)
            plotter.plot(OUTPUT_PATH_CONGAN + 'dev_disc_cost', np.mean(dev_disc_costs))
            gen_images = generate_image_congan(G, fixed_noise_congan)
            torchvision.utils.save_image(gen_images, OUTPUT_PATH_CONGAN + 'samples_{}.png'.format(iteration), nrow=8,
                                         padding=2)
            # ----------------------flush and save----------------------
            plotter.flush(len(OUTPUT_PATH_CONGAN), print_info=True)
            saver.flush()
        plotter.tick()
        saver.tick()


def train_congan_mnist_pairs():
    best_wdist = np.inf
    better_value_fnc = lambda best_wd, new_wd: new_wd < best_wd
    saver = Saver('wdist', better_value_fnc, OUTPUT_PATH_CONGAN, START_ITER)
    plotter = Plotter(output_path=OUTPUT_PATH_CONGAN, start_iter=START_ITER)
    dataloader = training_data_loader(CSV_FILE_TRAIN_TRAIN)
    val_loader = val_data_loader(CSV_FILE_VAL_TRAIN)
    dataiter = iter(dataloader)
    print('start training ConGAN only  , mode: {}, iterations: {}'.format(MODE, NUM_ITER_GAN))
    for iteration in range(START_ITER, END_ITER_GAN):
        start_time = timer()
        # print("Iter: " + str(iteration))
        start = timer()
        # ---------------------TRAIN G------------------------
        for p in D.parameters():
            p.requires_grad_(False)  # freeze D

        gen_cost = None
        for i in range(GENER_ITERS):
            # print("Generator iters: " + str(i))
            G.zero_grad()
            f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
            noise = gen_rand_noise_with_label(f_label)
            noise.requires_grad_(True)
            fake_data = G(noise)
            gen_cost, gen_aux_output = D(fake_data)

            aux_label = torch.from_numpy(f_label).long()
            aux_label = aux_label.to(device)
            aux_errG = aux_criterion(gen_aux_output, aux_label).mean()
            gen_cost = -gen_cost.mean()
            g_cost = ACGAN_SCALE_G * aux_errG + gen_cost
            g_cost.backward()

        optimizer_g.step()
        end = timer()
        # print(out_fname'---train G elapsed time: {end - start}')
        # ---------------------TRAIN D------------------------
        for p in D.parameters():  # reset requires_grad
            p.requires_grad_(True)  # they are set to False below in training G
        for i in range(CRITIC_ITERS):
            # print("Critic iter: " + str(i))

            start = timer()
            D.zero_grad()

            # gen fake dataset_iter and load real dataset_iter
            f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
            noise = gen_rand_noise_with_label(f_label)
            with torch.no_grad():
                noisev = noise  # totally freeze G, training D
            fake_data = G(noisev).detach()
            end = timer()
            # print(out_fname'---gen G elapsed time: {end-start}')
            start = timer()
            batch = next(dataiter, None)
            if batch is None:
                dataiter = iter(dataloader)
                batch = next(dataiter)
            real_data = batch[0]  # batch[1] contains labels
            real_data.requires_grad_(True)
            real_label = batch[1].long()
            end = timer()
            # print(out_fname'---load real imgs elapsed time: {end-start}')

            start = timer()
            real_data = real_data.to(device)
            if MODE == GAN_types.ConWGAN_GP_conv:
                real_data = cnn(real_data, conv_only=True)
            real_label = real_label.to(device)

            # train with real dataset_iter
            disc_real_out, aux_output = D(real_data)
            aux_errD_real = aux_criterion(aux_output, real_label)
            errD_real = aux_errD_real.mean()
            disc_real = disc_real_out.mean()
            disc_real_var = disc_real_out.var()

            # train with fake dataset_iter
            disc_fake_out, aux_output = D(fake_data)
            # aux_errD_fake = aux_criterion(aux_output, fake_label)
            # errD_fake = aux_errD_fake.mean()
            disc_fake = disc_fake_out.mean()
            disc_fake_var = disc_fake.var()

            # showMemoryUsage(0)
            # train with interpolates dataset_iter
            gradient_penalty = calc_gradient_penalty_conwgan(D, real_data, fake_data)
            # showMemoryUsage(0)

            # final disc cost
            disc_cost = disc_fake - disc_real
            disc_acgan = errD_real  # + errD_fake
            disc_cost_var = disc_real_var + disc_fake_var
            disc_total_cost = disc_cost + ACGAN_SCALE * disc_acgan + gradient_penalty
            if USE_VAR_PENALTY: disc_total_cost += disc_cost_var
            disc_total_cost.backward()
            w_dist = disc_fake - disc_real
            optimizer_d.step()

            end = timer();
            # print(out_fname'---train D elapsed time: {end-start}')
            # ---------------VISUALIZATION---------------------
            plotter.plot(OUTPUT_PATH_CONGAN + 'time', timer() - start_time)
            plotter.plot(OUTPUT_PATH_CONGAN + 'train_disc_cost', disc_total_cost.cpu().data.numpy())
            plotter.plot(OUTPUT_PATH_CONGAN + 'train_gen_cost', gen_cost.cpu().data.numpy())
            plotter.plot(OUTPUT_PATH_CONGAN + 'wasserstein_distance', w_dist.cpu().data.numpy())
            plotter.plot(OUTPUT_PATH_CONGAN + 'disc_cost_var', disc_cost_var.cpu().data.numpy())
            w_dist = np.abs(w_dist.item())

            # ---------------SAVING--------------------
            saver.update('D', D)
            saver.update('D_w', D.state_dict())
            saver.update('G', G)
            saver.update('G_w', G.state_dict())
            saver.update('wdist', w_dist)
            saver.update('D_cost', disc_cost.item())
            saver.update('G_cost', disc_cost.item())
            saver.update('D_opt', optimizer_d.state_dict())
            saver.update('G_opt', optimizer_g.state_dict())

        if w_dist < best_wdist:
            best_wdist = w_dist
            torch.save(G, OUTPUT_PATH_CONGAN + "generator_best_abs.pt")
            torch.save(D, OUTPUT_PATH_CONGAN + "discriminator_best_abs.pt")

        if iteration % VAL_FREQ_GAN == (VAL_FREQ_GAN - 1):
            dev_disc_costs = []
            for _, images in enumerate(val_loader):
                imgs = torch.Tensor(images[0])
                imgs = imgs.to(device)
                if MODE == GAN_types.WGAN_GP_conv: imgs = cnn(imgs, conv_only=True)
                with torch.no_grad():
                    imgs_v = imgs
                D_res, _ = D(imgs_v)
                _dev_disc_cost = -D_res.mean().cpu().data.numpy()
                dev_disc_costs.append(_dev_disc_cost)
            plotter.plot(OUTPUT_PATH_CONGAN + 'dev_disc_cost', np.mean(dev_disc_costs))
            gen_images = generate_image_congan(G, fixed_noise_congan)
            torchvision.utils.save_image(gen_images, OUTPUT_PATH_CONGAN + 'samples_{}.png'.format(iteration), nrow=8,
                                         padding=2)
            # ----------------------flush and save----------------------
            plotter.flush(len(OUTPUT_PATH_CONGAN), print_info=True)
            saver.flush()
        plotter.tick()
        saver.tick()


def train_cnn(train_loader):
    # train_iter = iter(train_loader)
    # UNfreeze CNN
    for p in cnn.parameters():
        p.requires_grad_(True)  # UNfreeze CNN
    train_losses_cnn = np.asarray([])
    total_train_cnn, correct_train_cnn = 0, 0
    optimizer_cnn.zero_grad()
    cnn.train()
    for batch in train_loader:
        # Forward pass
        images = batch[0]
        labels = batch[1].long()
        images = images.to(device)
        labels = labels.to(device)
        outputs = cnn(images)
        _, predicted = torch.max(outputs.data, 1)

        # forward pass
        loss = criterion_cnn(outputs, labels)
        train_losses_cnn = np.append(train_losses_cnn, loss.item())

        # Backward and optimize
        optimizer_cnn.zero_grad()
        loss.backward()
        optimizer_cnn.step()

        total_train_cnn += labels.size(0)
        correct_train_cnn += (predicted == labels).sum().item()
    train_acc = 100 * correct_train_cnn / total_train_cnn
    train_loss_cnn = np.mean(train_losses_cnn)
    return train_acc, train_loss_cnn


def train_D(loader, counter):
    # freeze CNN
    for p in cnn.parameters():
        p.requires_grad_(False)  # freeze CNN
    for p in D.parameters():  # reset requires_grad
        p.requires_grad_(True)  # they are set to False below in training G
    for i in range(CRITIC_ITERS):
        # print("Critic iter: " + str(i))

        start = timer()
        D.zero_grad()
        optimizer_d.zero_grad()

        # gen fake dataset_iter and load real dataset_iter
        noise = gen_rand_noise()
        with torch.no_grad():
            noisev = noise  # totally freeze G, training D
        fake_data = G(noisev).detach()

        batch = next(loader, None)
        if batch is None:
            loader = iter(loader)
            batch = loader.next()
        data = batch[0]  # batch[1] contains labels
        real_data = data.to(device)  # TODO: modify load_data for each loading
        if MODE == GAN_types.WGAN_GP_conv: real_data = cnn(real_data, conv_only=True)

        # train with real dataset_iter
        disc_real_out = D(real_data)
        disc_real = disc_real_out.mean()
        disc_real_var = disc_real_out.var()  # include into cost function?
        # track mean response of discriminator on real data over the training
        counter.add_samples(disc_real_out.cpu().data.numpy())

        # train with fake dataset_iter
        disc_fake_out = D(fake_data)
        disc_fake = disc_fake_out.mean()
        disc_fake_var = disc_fake_out.var()  # include into cost function?

        # showMemoryUsage(0)
        # train with interpolates dataset_iter
        gradient_penalty = calc_gradient_penalty(D, real_data, fake_data)
        # showMemoryUsage(0)

        # final disc cost

        disc_cost = disc_fake - disc_real  # we want this to be as close as possible -> result should be 0
        disc_cost_var = disc_real_var + disc_fake_var  # we want discriminator to have as small variance as possible
        disc_total_cost = disc_cost + gradient_penalty
        if USE_VAR_PENALTY: disc_total_cost += disc_cost_var
        disc_total_cost.backward()
        w_dist = disc_fake - disc_real
        optimizer_d.step()
    weights = compute_weights(counter)
    return disc_total_cost.cpu().data.numpy(), w_dist.cpu().data.numpy(), weights


def train_ConD(loader, counter):
    for p in D.parameters():  # reset requires_grad
        p.requires_grad_(True)  # they are set to False below in training G
    for i in range(CRITIC_ITERS):
        # print("Critic iter: " + str(i))

        start = timer()
        D.zero_grad()

        # gen fake dataset_iter and load real dataset_iter
        f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
        noise = gen_rand_noise_with_label(f_label)
        with torch.no_grad():
            noisev = noise  # totally freeze G, training D
        fake_data = G(noisev).detach()
        end = timer()
        # print(out_fname'---gen G elapsed time: {end-start}')
        start = timer()
        batch = next(dataiter, None)
        if batch is None:
            dataiter = iter(loader)
            batch = next(dataiter)
        real_data = batch[0]  # batch[1] contains labels
        real_data.requires_grad_(True)
        real_label = batch[1].long()
        end = timer()
        # print(out_fname'---load real imgs elapsed time: {end-start}')

        start = timer()
        real_data = real_data.to(device)
        if MODE == GAN_types.ConWGAN_GP_conv:
            real_data = cnn(real_data, conv_only=True)
        real_label = real_label.to(device)

        # train with real dataset_iter
        disc_real_out, aux_output = D(real_data)
        aux_errD_real = aux_criterion(aux_output, real_label)
        errD_real = aux_errD_real.mean()
        disc_real = disc_real_out.mean()
        disc_real_var = disc_real_out.var()
        # track mean response of discriminator on real data over the training
        counter.add_samples(disc_real_out.cpu().data.numpy())

        # train with fake dataset_iter
        disc_fake_out, aux_output = D(fake_data)
        # aux_errD_fake = aux_criterion(aux_output, fake_label)
        # errD_fake = aux_errD_fake.mean()
        disc_fake = disc_fake_out.mean()
        disc_fake_var = disc_fake.var()

        # showMemoryUsage(0)
        # train with interpolates dataset_iter
        gradient_penalty = calc_gradient_penalty_conwgan(D, real_data, fake_data)
        # showMemoryUsage(0)

        # final disc cost
        disc_cost = disc_fake - disc_real
        disc_acgan = errD_real  # + errD_fake
        disc_cost_var = disc_real_var + disc_fake_var
        disc_total_cost = disc_cost + ACGAN_SCALE * disc_acgan + gradient_penalty
        if USE_VAR_PENALTY: disc_total_cost += disc_cost_var
        disc_total_cost.backward()
        w_dist = disc_fake - disc_real
        optimizer_d.step()

        end = timer();
        # print(out_fname'---train D elapsed time: {end-start}')
    weights = compute_weights(counter)
    return disc_total_cost.cpu().data.numpy(), w_dist.cpu().data.numpy(), weights


def compute_weights(counter):
    '''
    Computes WEIGHTS for data sampling for CNN.
    Weights are set as an absolute distance from the mean discriminator response on real data.
    :return: list of WEIGHTS
    '''
    ts = timer()
    weights = []
    with torch.no_grad():
        for batch in iter(train_loader_gan_noshuffle):
            data = batch[0]
            data = data.to(device)
            if MODE == (GAN_types.WGAN_GP_conv or GAN_types.ConWGAN_GP_conv):
                data = cnn(data, conv_only=True)
            if MODE == (GAN_types.ConWGAN_GP or GAN_types.ConWGAN_GP_conv):
                Dout, aux_output = D(data).cpu().data.numpy()
            else:
                Dout = D(data).cpu().data.numpy()
            if WEIGHT_CRITERION == WEIGHT_CRITERION_types.WGAN:
                new_weights = np.subtract(Dout, counter.get_mean())
            elif WEIGHT_CRITERION == WEIGHT_CRITERION_types.ConWGAN and (
                            MODE == GAN_types.ConWGAN_GP or MODE == GAN_types.ConWGAN_GP_conv):
                new_weights = np.subtract(aux_output, counter.get_mean())
            else:
                raise Exception(
                    'Unsupported combinatiion of GAN MODE ({}) and WEIGHT_CRITERION ({}) in weights computation'
                        .format(MODE, WEIGHT_CRITERION))
            weights.extend(new_weights)
    # print('WEIGHTS computed in {}, '.format(timer() - ts), end='')
    return np.abs(weights).tolist()


def train_G():
    optimizer_g.zero_grad()
    # freeze D
    for p in D.parameters():
        p.requires_grad_(False)  # freeze D
    gen_cost = None
    for i in range(GENER_ITERS):
        # print("Generator iters: " + str(i))
        G.zero_grad()
        noise = gen_rand_noise()
        noise.requires_grad_(True)
        fake_data = G(noise)
        gen_cost = D(fake_data)
        gen_cost = gen_cost.mean()
        gen_cost.backward(mone)
        gen_cost = -gen_cost
    optimizer_g.step()
    return gen_cost.cpu().data.numpy()


def train_ConG():
    for p in D.parameters():
        p.requires_grad_(False)  # freeze D

    gen_cost = None
    for i in range(GENER_ITERS):
        # print("Generator iters: " + str(i))
        G.zero_grad()
        f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
        noise = gen_rand_noise_with_label(f_label)
        noise.requires_grad_(True)
        fake_data = G(noise)
        gen_cost, gen_aux_output = D(fake_data)

        aux_label = torch.from_numpy(f_label).long()
        aux_label = aux_label.to(device)
        aux_errG = aux_criterion(gen_aux_output, aux_label).mean()
        gen_cost = -gen_cost.mean()
        g_cost = ACGAN_SCALE_G * aux_errG + gen_cost
        g_cost.backward()

    optimizer_g.step()
    end = timer()
    # print(out_fname'---train G elapsed time: {end - start}')
    return gen_cost.cpu().data.numpy()


def validate_cnn(val_loader, model, criterion, show=False):
    # switch to evaluate mode
    model.eval()
    losses = np.asarray([])
    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(val_loader):
            data = batch[0]
            data = data.to(device)
            target = batch[1].long()
            target = target.to(device)
            # compute output
            output = model(data)
            _, predicted = torch.max(output.data, 1)
            loss = criterion(output, target)
            losses = np.append(losses, loss.cpu().data.numpy())
            total += target.size(0)
            correct += (predicted == target).sum().item()
    val_loss = np.mean(losses)
    val_acc = 100 * correct / total
    return val_loss, val_acc


def train_cnn_gan(counter):
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver_both = Saver('val_loss', better_value_fnc, OUTPUT_PATH_BOTH, 0)
    plotter_both = Plotter()
    print('start iter of saver_both: {}'.format(saver_both.iter))
    print('start training BOTH CNN and GAN')
    cnn_train_loader_all, sampler = load_my_data_weight_sampler(TRAIN_DIR_ALL, CSV_FILE_TRAIN_ALL, None,
                                                                num_samples=NUM_SAMPLES_WEIGHT,
                                                                batch_size=64, is3d=USE3D)
    gan_train_iter = iter(TRAIN_LOADER_ALL)
    for epoch in range(N_EPOCHS_BOTH):
        start_time = timer()

        # ---------------------TRAIN G------------------------
        # print('train G, ', end='')
        gen_cost = train_G()
        # print(timer() - start_time)
        # t = timer()

        # ---------------------TRAIN D------------------------
        # print('train D, ', end='')
        disc_cost, w_dist, weights = train_D(gan_train_iter, counter)
        # print(timer() - t)
        # t = timer()

        # --------------------TRAIN CNN-----------------------

        sampler.reweight(weights)
        train_acc, train_loss_cnn = train_cnn(cnn_train_loader_all)
        # print(timer() - t)
        # t = timer()
        # ------------------VALIDATE GAN----------------------
        # print('validate GAN, ', end='')
        cnn_val_iter = iter(VAL_LOADER_CNN_ALL)
        dev_disc_costs = []
        for _, images in enumerate(cnn_val_iter):
            imgs = torch.Tensor(images[0])
            imgs = imgs.to(device)
            if MODE in [GAN_types.WGAN_GP_conv, GAN_types.WGAN_GP_conv_FCG]: imgs = cnn(imgs, conv_only=True)
            if MODE == GAN_types.WGAN_GP_fc_FCG: imgs = cnn(imgs, fc1_out=True)
            with torch.no_grad():
                imgs_v = imgs
            D_res = D(imgs_v)
            _dev_disc_cost = -D_res.mean().cpu().data.numpy()
            dev_disc_costs.append(_dev_disc_cost)
        disc_cost_val = np.mean(dev_disc_costs)
        # print(timer() - t)
        # t = timer()

        # ----------------- VALIDATE CNN ---------------------
        # print('validate CNN, ', end='')
        cnn_val_iter = iter(VAL_LOADER_CNN_ALL)
        val_loss, val_acc = validate_cnn(cnn_val_iter, cnn, criterion_cnn)
        # print(timer() - t)
        # t = timer()

        # print('plotting, ', end='')
        # ------ create plots ------
        plotter_both.plot(OUTPUT_PATH_BOTH + 'time', timer() - start_time)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'train_loss', train_loss_cnn)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'val_loss', val_loss)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'train_acc', train_acc)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'val_acc', val_acc)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'gen_cost', gen_cost)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'disc_cost', disc_cost)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'disc_cost_val', disc_cost_val)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'w_dist', w_dist)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'mean_D_real', counter.get_mean())
        plotter_both.plot(OUTPUT_PATH_BOTH + 'var_D_real', counter.get_var())
        plotter_both.tick()
        # print(timer() - t)
        # t = timer()

        # print('saving, ', end='')
        # ------ save -------
        saver_both.update('cnn', cnn)
        saver_both.update('cnn_w', cnn.state_dict())
        saver_both.update('cnn_opt', optimizer_cnn.state_dict())
        saver_both.update('train_loss', train_loss_cnn)
        saver_both.update('val_loss', val_loss)
        saver_both.update('train_acc', train_acc)
        saver_both.update('val_acc', val_acc)
        saver_both.update('epoch', epoch)
        saver_both.update('D', D)
        saver_both.update('D_w', D.state_dict())
        saver_both.update('G', G)
        saver_both.update('G_w', G.state_dict())
        saver_both.update('gen_cost', gen_cost)
        saver_both.update('disc_cost', disc_cost)
        saver_both.update('disc_cost_val', disc_cost_val)
        saver_both.update('w_dist', w_dist)
        saver_both.update('mean_D_real', counter.get_mean())
        saver_both.update('var_D_real', counter.get_var())
        saver_both.update('counter_t', counter.get_t())
        saver_both.tick()
        # print(timer() - t)
        # t = timer()
        # print('flush, ', end='')
        if epoch % SAVE_FREQ_BOTH == (SAVE_FREQ_BOTH - 1):
            plotter_both.flush(len(OUTPUT_PATH_BOTH))
            saver_both.flush()
        # print(timer() - t)
        # ----- print info ------
        print(
            'Epoch [{}/{}]\nCNN:\tTrain Loss: {:.5f}, Train Acc: {:.2f}, Val Loss: {:.5f}, Val Acc: {:.2f}\n'
            'GAN:\tG cost: {:.5f}, D cost train: {:.5f}, D cost val: {:.5f}, W dist: {:.5f}\n\ttime: {:.3f}'.format(
                epoch + 1, N_EPOCHS_BOTH, train_loss_cnn, train_acc, val_loss, val_acc, gen_cost, disc_cost,
                disc_cost_val, w_dist, timer() - start_time))


def train_cnn_congan(counter):
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver_both = Saver('val_loss', better_value_fnc, OUTPUT_PATH_BOTH)
    plotter_both = Plotter()
    print('start iter of saver_both: {}'.format(saver_both.iter))
    print('start training BOTH CNN and GAN')
    cnn_train_loader_all, sampler = load_my_data_weight_sampler(TRAIN_DIR_ALL, CSV_FILE_TRAIN_ALL, None,
                                                                num_samples=NUM_SAMPLES_WEIGHT,
                                                                batch_size=64, is3d=USE3D)
    gan_train_iter = iter(TRAIN_LOADER_ALL)
    for epoch in range(N_EPOCHS_BOTH):
        start_time = timer()

        # ---------------------TRAIN G------------------------
        # print('train G, ', end='')
        gen_cost = train_ConG()
        # print(timer() - start_time)
        # t = timer()

        # ---------------------TRAIN D------------------------
        # print('train D, ', end='')
        disc_cost, w_dist, weights = train_ConD(gan_train_iter, counter)
        # print(timer() - t)
        # t = timer()

        # --------------------TRAIN CNN-----------------------

        sampler.reweight(weights)
        train_acc, train_loss_cnn = train_cnn(cnn_train_loader_all)
        # print(timer() - t)
        # t = timer()
        # ------------------VALIDATE GAN----------------------
        # print('validate GAN, ', end='')
        cnn_val_iter = iter(VAL_LOADER_CNN_ALL)
        dev_disc_costs = []
        for _, images in enumerate(cnn_val_iter):
            imgs = torch.Tensor(images[0])
            imgs = imgs.to(device)
            if MODE == GAN_types.WGAN_GP_conv: imgs = cnn(imgs, conv_only=True)
            with torch.no_grad():
                imgs_v = imgs
            D_res, _ = D(imgs_v)
            _dev_disc_cost = -D_res.mean().cpu().data.numpy()
            dev_disc_costs.append(_dev_disc_cost)
        disc_cost_val = np.mean(dev_disc_costs)
        # print(timer() - t)
        # t = timer()

        # ----------------- VALIDATE CNN ---------------------
        # print('validate CNN, ', end='')
        cnn_val_iter = iter(VAL_LOADER_CNN_ALL)
        val_loss, val_acc = validate_cnn(cnn_val_iter, cnn, criterion_cnn)
        # print(timer() - t)
        # t = timer()

        # print('plotting, ', end='')
        # ------ create plots ------
        plotter_both.plot(OUTPUT_PATH_BOTH + 'time', timer() - start_time)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'train_loss', train_loss_cnn)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'val_loss', val_loss)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'train_acc', train_acc)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'val_acc', val_acc)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'gen_cost', gen_cost)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'disc_cost', disc_cost)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'disc_cost_val', disc_cost_val)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'w_dist', w_dist)
        plotter_both.plot(OUTPUT_PATH_BOTH + 'mean_D_real', counter.get_mean())
        plotter_both.plot(OUTPUT_PATH_BOTH + 'var_D_real', counter.get_var())
        plotter_both.tick()
        # print(timer() - t)
        # t = timer()

        # print('saving, ', end='')
        # ------ save -------
        saver_both.update('cnn', cnn)
        saver_both.update('cnn_w', cnn.state_dict())
        saver_both.update('cnn_opt', optimizer_cnn.state_dict())
        saver_both.update('train_loss', train_loss_cnn)
        saver_both.update('val_loss', val_loss)
        saver_both.update('train_acc', train_acc)
        saver_both.update('val_acc', val_acc)
        saver_both.update('epoch', epoch)
        saver_both.update('D', D)
        saver_both.update('D_w', D.state_dict())
        saver_both.update('G', G)
        saver_both.update('G_w', G.state_dict())
        saver_both.update('gen_cost', gen_cost)
        saver_both.update('disc_cost', disc_cost)
        saver_both.update('disc_cost_val', disc_cost_val)
        saver_both.update('w_dist', w_dist)
        saver_both.update('mean_D_real', counter.get_mean())
        saver_both.update('var_D_real', counter.get_var())
        saver_both.update('counter_t', counter.get_t())
        saver_both.tick()
        # print(timer() - t)
        # t = timer()
        # print('flush, ', end='')
        if epoch % SAVE_FREQ_BOTH == (SAVE_FREQ_BOTH - 1):
            plotter_both.flush(len(OUTPUT_PATH_BOTH))
            saver_both.flush()
        # print(timer() - t)
        # ----- print info ------
        print(
            'Epoch [{}/{}]\nCNN:\tTrain Loss: {:.5f}, Train Acc: {:.2f}, Val Loss: {:.5f}, Val Acc: {:.2f}\n'
            'GAN:\tG cost: {:.5f}, D cost train: {:.5f}, D cost val: {:.5f}, W dist: {:.5f}\n\ttime: {:.3f}'.format(
                epoch + 1, N_EPOCHS_BOTH, train_loss_cnn, train_acc, val_loss, val_acc, gen_cost, disc_cost,
                disc_cost_val, w_dist, timer() - start_time))


def complete_training():
    print('GAN mode: {}, number of GAN iterations: {}, number of samples for training on complete dataset: {}'
          .format(MODE, NUM_ITER_GAN, NUM_SAMPLES_WEIGHT))
    print('bottom dataset size: {}, unlimited dataset size: {}'.format(len(TRAIN_LOADER_TRAIN.dataset),
                                                                       len(TRAIN_LOADER_ALL.dataset)))
    # CNN only
    train_cnn_only()
    torch.save(cnn, './cnn_basic.pt')
    torch.save(optimizer_cnn.state_dict(), './cnn_opt.pt')

    # GAN only
    if MODE == (GAN_types.WGAN_GP or GAN_types.WGAN_GP_conv or GAN_types.WGAN_GP_conv_FCG):
        train_gan(D, G)
    else:
        train_congan()
    # initialize counter
    counter = init_counter()

    # check CNN on the complete dataset
    total, correct = 0, 0
    with torch.no_grad():
        cnn.eval()
        for batch in TRAIN_LOADER_ALL:
            data = batch[0].to(device)
            labels = batch[1].long().to(device)
            output = cnn(data)
            _, predicted = torch.max(output.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    acc = 100 * correct / total
    print('CNN accuracy on unlimited dataset: {:.2f}'.format(acc))

    # check CNN on the training dataset
    total, correct = 0, 0
    with torch.no_grad():
        cnn.eval()
        for batch in TRAIN_LOADER_TRAIN:
            data = batch[0].to(device)
            labels = batch[1].long().to(device)
            output = cnn(data)
            _, predicted = torch.max(output.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    acc = 100 * correct / total
    print('CNN accuracy on the BOTTOM dataset: {:.2f}'.format(acc))

    # CNN and GAN/ConGAN together
    if MODE == (GAN_types.WGAN_GP or GAN_types.WGAN_GP_conv):
        train_cnn_gan(counter)
    else:
        train_cnn_congan(counter)
    print()

    print('train basic CNN further without data weighting')
    train_cnn_only_further()


def print_output_files():
    print('Output paths:')
    print('\tCNN:\t{}'.format(OUTPUT_PATH_CNN))
    print('\tGAN:\t{}'.format(OUTPUT_PATH_GAN))
    print('\tCONGAN:\t{}'.format(OUTPUT_PATH_CONGAN))
    print('\tCNN_F:\t{}'.format(OUTPUT_PATH_CNN_FURTHER))
    print('\tBOTH:\t{}'.format(OUTPUT_PATH_BOTH))


if __name__ == '__main__':
    print_output_files()
    '''
    train_cnn_only()
    # check CNN on the training dataset

    total, correct = 0, 0
    with torch.no_grad():
        cnn.eval()
        for batch in TRAIN_LOADER_TRAIN:
            data = batch[0].to(device)
            labels = batch[1].long().to(device)
            output = cnn(data)
            _, predicted = torch.max(output.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    acc = 100 * correct / total
    print('CNN accuracy on the BOTTOM dataset: {:.2f}'.format(acc))
    # check CNN on the training dataset
    total, correct = 0, 0
    with torch.no_grad():
        cnn.eval()
        for batch in TRAIN_LOADER_TEST:
            data = batch[0].to(device)
            labels = batch[1].long().to(device)
            output = cnn(data)
            _, predicted = torch.max(output.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    acc = 100 * correct / total
    print('CNN accuracy on the TOP dataset: {:.2f}'.format(acc))
    '''

    train_gan(D, G)
    # train_congan()
    # complete_training()
    out_fname.close()
    sys.stdout.close()
