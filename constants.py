import torch
from models.types import *

# ========= CONSTANTS ===========
LOAD = False
MULTIPLE_GPU = True

# CNN parameters
NUM_CLASSES = 2
N_EPOCHS_CNN = 5
LEARNING_RATE = 0.0001
LR_CNN = 1e-4
BATCH_SIZE = 100
PLOT_FREQ_CNN = 1
SAVE_FREQ_CNN = 1
MAKE_SAVES_CNN = False

# Device configuration
cuda_available = torch.cuda.is_available()
device = torch.device('cuda' if cuda_available else 'cpu')

# GAN parameters
RESTORE_MODE = False  # if True, it will load saved cnn from OUT_PATH and continue to train
WEIGHT_CRITERION = WEIGHT_CRITERION_types.WGAN
LR_G, LR_D = 1e-4, 1e-4
START_ITER = 0  # starting iteration
MODE = GAN_types.WGAN_GP  # GAN_types.WGAN_GP#GAN_types.WGAN_GP_conv
ISCONV = (MODE == GAN_types.WGAN_GP_conv or MODE == GAN_types.ConWGAN_GP_conv)
DIM = 64  # Model dimensionality
CRITIC_ITERS = 5  # How many iterations to train the critic for
GENER_ITERS = 1
N_GPUS = 1  # Number of GPUs
NUM_ITER_GAN = 50000  # How many iterations to train for
END_ITER_GAN = START_ITER + NUM_ITER_GAN
LAMBDA = 10  # Gradient penalty lambda hyperparameter
OUTPUT_DIM = 64 * 64 * 3  # Number of pixels in each image
VAL_FREQ_GAN = 100
PLOT_DISTR_FREQ = 1000
DIM_CONV = 128
IM_SIZE_CONV = 8
N_ROWS = 8
FIX_NOISE_SIZE = N_ROWS ** 2

# CONGAN parameters only
ACGAN_SCALE = 1.  # How to scale the critic's ACGAN loss relative to WGAN loss
ACGAN_SCALE_G = 1.  # How to scale generator's ACGAN loss relative to WGAN loss

# training BOTH together
N_EPOCHS_BOTH = 10
SAVE_FREQ_BOTH = 1
NUM_SAMPLES_WEIGHT = 20000

# ========= DATA DIRECTORIES ============
DATA_SPECIF = DATASET_types.GEOM_3D_LARGE
DATASET_TYPE = DATASET_types.GEOM_3D_LARGE
CSV_SPECIF_TRAIN = 'obj-012_col-rg_bb-lb'
CSV_SPECIF_TEST = 'obj-3_col-b_bb-rt'
# training files
TRAIN_DIR_TRAIN = DATA_SPECIF.value  # DATASET_types.GEOM_3D_BOTTOM.value + '/train'  # './datasets/' + DATA_SPECIF + '/train'
CSV_FILE_TRAIN_TRAIN = './datasets/{}_train.csv'.format(CSV_SPECIF_TRAIN)
VAL_DIR_TRAIN = TRAIN_DIR_TRAIN
CSV_FILE_VAL_TRAIN = './datasets/{}_val.csv'.format(CSV_SPECIF_TRAIN)
# test files
TRAIN_DIR_TEST = TRAIN_DIR_TRAIN
CSV_FILE_TRAIN_TEST = './datasets/{}_train.csv'.format(CSV_SPECIF_TEST)
# rest
TRAIN_DIR_ALL = DATASET_types.GEOM_3D_UNLIMITED.value + '/train'
CSV_FILE_TRAIN_ALL = TRAIN_DIR_ALL + '/{}_train.csv'.format(CSV_SPECIF_TRAIN)
VAL_DIR_ALL = '/home/skovirad/dataset3d/3d_unlimited/valid'
CSV_FILE_VAL_ALL = VAL_DIR_ALL + '/geom_data.csv'
# output paths
OUTPUT_PATH_CNN = './result/' + DATA_SPECIF.name + '/CNN/'
OUTPUT_PATH_GAN = './result/' + DATA_SPECIF.name + '/GAN-3D/'
OUTPUT_PATH_CONGAN = './result/' + DATA_SPECIF.name + '/CONGAN-3D_img/'
OUTPUT_PATH_BOTH = './result/' + DATA_SPECIF.name + '/BOTH/'
OUTPUT_PATH_CNN_FURTHER = './result/' + DATA_SPECIF.name + '/CNN-only-further/'
# paths to LOAD files
LOAD_FILE_CNN = './result/splitted-img/CNN/cnn_last.pt'
LOAD_FILE_G = './result/geom3D/CONGAN-3D_img/G_last.pt'
LOAD_FILE_D = './result/geom3D/CONGAN-3D_img/D_last.pt'
LOAD_FILE_CNN_OPT = './result/splitted-img/CNN/cnn_opt_last.pt'
LOAD_FILE_G_OPT = './result/geom3D/CONGAN-3D_img/G_opt_last.pt'
LOAD_FILE_D_OPT = './result/geom3D/CONGAN-3D_img/D_opt_last.pt'
CNN_PARAMS_FILE = './result/splitted-img/CNN/save_last.npy'
GAN_PARAMS_FILE = './result/geom3D/CONGAN-3D_img/save_last.npy'
USE3D = True
