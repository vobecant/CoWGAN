from training import *
from models.dataset import Dataset2D
from models.wgan import FCGenerator as Generator
from models.wgan import FCDiscriminator as Discriminator
from models.wgan import FCNetShallow as Net
from models.wgan import FCDiscriminatorCond, FCGeneratorCond
from models.conditionalGAN import ConditionalGeneratorFC, ConditionalDiscriminatorFC
import torch
from torch.optim import Adam
from tensorboardX import SummaryWriter

NOISE_DIM = 128
BATCH_SIZE = 100
INPUT_SIZE = 2
DIM_D_OUTPUT = 4
LR = 1e-4
one = torch.FloatTensor([1])
mone = one * -1
USE_VAR_PENALTY = False
output_path = './result/GEOM_3D_LARGE/GAN_2D-data/GAN/'
log_file = open(output_path + 'train-log.log', 'w')
cuda_available = torch.cuda.is_available()
device = torch.device('cuda' if cuda_available else 'cpu')
USE_MULTIPLE_GPU = False


def load_my_data(data_file, labels_file=None, batch_size=BATCH_SIZE, shuffle=True,
                 drop_last=True):
    dataset = Dataset2D(data_file, labels_file)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=shuffle, num_workers=5,
                                                 drop_last=drop_last, pin_memory=True)
    print('created dataset loader from {}, number of samples: {}, shuffle: {}'
          .format(data_file, len(dataset), shuffle))
    return dataset_loader


if __name__ == '__main__':
    train_loader = load_my_data(data_file='./datasets/blob_train_data.npy',
                                labels_file='./datasets/blob_train_labels.npy')
    val_loader = load_my_data(data_file='./datasets/blob_val_data.npy', labels_file='./datasets/blob_val_labels.npy')
    D = Discriminator(input_dim=INPUT_SIZE)  #
    # D = Net(conv_dim=INPUT_SIZE, output_dim=1)
    # ConditionalDiscriminatorFC(dim_input=INPUT_SIZE, dim_output=DIM_D_OUTPUT,n_labels=2)
    # FCDiscriminatorCond(n_classes=2, conv_dim=INPUT_SIZE, output_dim=1)
    G = Generator(input_dim=NOISE_DIM, output_dim=INPUT_SIZE)
    # G = FCGeneratorCond(conv_dim=NOISE_DIM, output_dim=INPUT_SIZE)  #
    # G = Net(conv_dim=NOISE_DIM, output_dim=INPUT_SIZE)
    # ConditionalGeneratorFC(dim_noise=NOISE_DIM, dim_output=INPUT_SIZE,dim_labels=2)  #Generator(output_dim=INPUT_SIZE)
    D = D.to(device)
    G = G.to(device)
    if torch.cuda.device_count() > 1 and USE_MULTIPLE_GPU:
        print('Use {} GPUs'.format(torch.cuda.device_count()))
        G = nn.DataParallel(G)
        D = nn.DataParallel(D)
    mone = mone.to(device)
    Dopt = Adam(D.parameters(), lr=LR)
    Gopt = Adam(G.parameters(), lr=LR)
    fixed_noise, fixed_labels = gen_rand_noise_and_label(size=200)
    fixed_labels = np.eye(INPUT_SIZE)[fixed_labels.cpu().data.numpy().astype(int)]
    fixed_labels = torch.from_numpy(fixed_labels).float()
    fixed_labels = fixed_labels.to(device)
    writer = SummaryWriter()
    test_loader = load_my_data(data_file='./datasets/blob_test.npy')
    labels = np.array([[1, 0], [0, 1]])
    criterion = nn.CrossEntropyLoss()
    # train_congan(D, G, None, Dopt, Gopt, train_loader, val_loader, mone, fixed_noise, fixed_labels, log_file,
    #             train_loader, test_loader, criterion, output_path)
    # train_conganFC(D, G, None, Dopt, Gopt, train_loader, val_loader, fixed_noise, fixed_labels, None, output_path,
    #               labels)

    train_gan(D, G, None, Dopt, Gopt, train_loader, val_loader, mone, fixed_noise, writer, log_file, train_loader,
              test_loader, output_path, use_var_penalty=USE_VAR_PENALTY)
