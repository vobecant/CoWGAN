import os, sys

sys.path.append(os.getcwd())

import time
import functools
import argparse

import numpy as np
# import sklearn.datasets

import libs as lib
import libs.plot
from tensorboardX import SummaryWriter

from matplotlib import pyplot as plt

import pdb
# import gpustat

# import models.dcgan as dcgan
from models.conwgan import *

import torch
import torchvision
from torch import nn
from torch import autograd
from torch import optim
from torchvision import transforms, datasets
from torch.autograd import grad
from timeit import default_timer as timer
from models.dataset import GeomDataset
from create_dataset import NUM_CLASSES
from torch.nn import functional as F

import torch.nn.init as init

DATA_SPECIF = 'unlimited_rg'
DATA_DIR = './datasets/' + DATA_SPECIF + '/train'
CSV_FILE_TRAIN = DATA_DIR + '/geom_data.csv'
VAL_DIR = './datasets/' + DATA_SPECIF + '/val'
CSV_FILE_VAL = VAL_DIR + '/geom_data.csv'
# OUTPUT_PATH = './result/CoGAN/' + DATA_SPECIF + '__multi-label/'  # output path where result (.e.g drawing images, cost, chart) will be stored

IMAGE_DATA_SET = 'geom'  # change this to something else, e.g. 'imagenets' or 'raw' if your dataset_iter is just a folder of raw images.
# If you use lmdb, you'll need to write the loader by yourself, see load_data
# TRAINING_CLASS = ['dining_room_train', 'bridge_train', 'restaurant_train', 'tower_train']
# VAL_CLASS = ['dining_room_val', 'bridge_val', 'restaurant_val', 'tower_val']

if len(DATA_DIR) == 0:
    raise Exception('Please specify path to dataset_iter directory in gan_64x64.py!')

RESTORE_MODE = False  # if True, it will load saved cnn from OUT_PATH and continue to train
print_timing = False
START_ITER = 0  # starting iteration
OUTPUT_PATH = './result/coWGAN/mnist_normMnist_in128_FCG_convD/'  # output path where result (.e.g drawing images, cost, chart) will be stored
MODE = 'wgan-gp'  # dcgan, wgan
DIM = 64  # Model dimensionality
CRITIC_ITERS = 5  # How many iterations to train the critic for
GENER_ITERS = 1
N_GPUS = 1  # Number of GPUs
BATCH_SIZE = 64  # Batch size. Must be a multiple of N_GPUS
END_ITER = 100000  # How many iterations to train for
LAMBDA = 10  # Gradient penalty lambda hyperparameter
LAMBDA_CNN = 10
OUTPUT_DIM = 32 * 32 * 1  # Number of pixels in each image
VAL_FREQ = 10
NOISE_DIM = 128
N_ROWS = 8
FIX_NOISE_SIZE = N_ROWS ** 2
N_TRAIN_SAMPLES = 6000
ACGAN_SCALE = 1.  # How to scale the critic's ACGAN loss relative to WGAN loss
ACGAN_SCALE_G = 1.  # How to scale generator's ACGAN loss relative to WGAN loss
IMG_SIZE = 32


def showMemoryUsage(device=1):
    gpu_stats = gpustat.GPUStatCollection.new_query()
    item = gpu_stats.jsonify()["gpus"][device]
    print('Used/total: ' + "{}/{}".format(item["memory.used"], item["memory.total"]))


def weights_init(m):
    if isinstance(m, MyConvo2d):
        if m.conv.weight is not None:
            if m.he_init:
                init.kaiming_uniform_(m.conv.weight)
            else:
                init.xavier_uniform_(m.conv.weight)
        if m.conv.bias is not None:
            init.constant_(m.conv.bias, 0.0)
    if isinstance(m, nn.Linear):
        if m.weight is not None:
            init.xavier_uniform_(m.weight)
        if m.bias is not None:
            init.constant_(m.bias, 0.0)


def load_data(path_to_folder, classes):
    data_transform = transforms.Compose([
        transforms.Scale(64),
        transforms.CenterCrop(64),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
    ])
    if IMAGE_DATA_SET == 'lsun':
        dataset = datasets.LSUN(path_to_folder, classes=classes, transform=data_transform)
    else:
        dataset = datasets.ImageFolder(root=path_to_folder, transform=data_transform)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True, num_workers=5,
                                                 drop_last=True, pin_memory=True)
    return dataset_loader


def load_my_data(path_to_folder, csvfile):
    data_transform = transforms.Compose([
        transforms.Resize(64),
        transforms.CenterCrop(64),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
    ])
    dataset = GeomDataset(csvfile, path_to_folder, data_transform)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True, num_workers=5,
                                                 drop_last=True, pin_memory=True)
    # print('loaded dataset_iter, number of samples: {}'.format(len(dataset_iter)))
    return dataset_loader


def mnist_loader(batch_size=BATCH_SIZE, shuffle=True, drop_last=True, train=True):
    dataset = datasets.MNIST('../data', train=train, download=True,
                             transform=transforms.Compose([
                                 transforms.Resize(32),
                                 transforms.ToTensor(),
                                 transforms.Normalize((0.1307,), (0.3081,))
                             ]))
    if train:
        dataset.train_data, dataset.train_labels = \
            dataset.train_data[:N_TRAIN_SAMPLES], dataset.train_labels[:N_TRAIN_SAMPLES]
    else:
        dataset.test_data, dataset.test_labels = \
            dataset.test_data[:N_TRAIN_SAMPLES], dataset.test_labels[:N_TRAIN_SAMPLES]
    loader = torch.utils.data.DataLoader(
        dataset, batch_size=batch_size, shuffle=shuffle, num_workers=5, pin_memory=True, drop_last=drop_last)
    return loader


def calc_gradient_penalty(netD, real_data, fake_data):
    alpha = torch.rand(BATCH_SIZE, 1)
    alpha = alpha.expand(BATCH_SIZE, int(real_data.nelement() / BATCH_SIZE)).contiguous()
    alpha = alpha.view(BATCH_SIZE, -1)
    alpha = alpha.to(device)

    real_data = real_data.view(BATCH_SIZE, -1)
    fake_data = fake_data.view(BATCH_SIZE, -1)
    interpolates = alpha * real_data.detach() + ((1 - alpha) * fake_data.detach())

    interpolates = interpolates.to(device)
    interpolates.requires_grad_(True)

    disc_interpolates, _ = netD(interpolates)

    gradients = autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                              grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]

    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * LAMBDA
    return gradient_penalty


def generate_image(netG, noise=None):
    if noise is None:
        rand_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
        noise, label = gen_rand_noise_with_label(rand_label)
    with torch.no_grad():
        noisev = noise
    samples = netG(noisev)
    samples = samples.view(BATCH_SIZE, 1, IMG_SIZE, IMG_SIZE)

    samples = samples * 0.5 + 0.5

    return samples


OLDGAN = False


def char2color(chars):
    table = {'r': [1, 0, 0], 'g': [0, 1, 0], 'b': [0, 0, 1]}
    colors = []
    for char in chars:
        colors.append(table[char])
    colors = np.array(colors)
    return colors


def gen_rand_noise_with_label(label=None, size=BATCH_SIZE):
    if label is None:
        label = np.random.randint(0, NUM_CLASSES, size)
    # attach label into noise
    noise = np.random.normal(0, 1, (size, NOISE_DIM))
    prefix = np.zeros((size, NUM_CLASSES))  # 5 = 2 (position) + 3 (color)
    prefix[np.arange(size), label] = 1
    noise[np.arange(size), :prefix.shape[1]] = prefix[np.arange(size)]

    noise = torch.from_numpy(noise).float()
    noise = noise.to(device)

    return noise, label


cuda_available = torch.cuda.is_available()
device = torch.device("cuda" if cuda_available else "cpu")
print("used device: {}, number of GPU: {}".format(device, N_GPUS))
fixed_label = []
for c in range(FIX_NOISE_SIZE):
    fixed_label.append(c % NUM_CLASSES)
fixed_noise, fixed_label = gen_rand_noise_with_label(fixed_label, size=FIX_NOISE_SIZE)
with open('fixed_params.log', 'w') as f:
    f.write('label\n')
    f.write('{}\n'.format(fixed_label))
print('written fixed info to file')

if RESTORE_MODE:
    aG = torch.load(OUTPUT_PATH + "generator.pt")
    aD = torch.load(OUTPUT_PATH + "discriminator.pt")
    print('restored learned WEIGHTS, START_ITER={}'.format(START_ITER))
else:
    if MODE == 'wgan-gp':
        aG = MNISTGenerator(128)#FCGenerator()  # GoodGenerator(64, 32 * 32 * 1)
        aD = GoodDiscriminator(64, NUM_CLASSES)  # FCDiscriminator(10, input_dim=32 * 32)
        OLDGAN = False
    elif MODE == 'dcgan':
        aG = FCGenerator()
        aD = DCGANDiscriminator()
        OLDGAN = False
    else:
        aG = dcgan.DCGAN_G(DIM, 128, 3, 64, 1, 0)
        aD = dcgan.DCGAN_D(DIM, 128, 3, 64, 1, 0)
        OLDGAN = True

    aG.apply(weights_init)
    aD.apply(weights_init)

LR = 1e-4
optimizer_g = torch.optim.Adam(aG.parameters(), lr=LR, betas=(0, 0.9))
optimizer_d = torch.optim.Adam(aD.parameters(), lr=LR, betas=(0, 0.9))

aux_criterion = nn.CrossEntropyLoss()  # nn.NLLLoss()
loss_position = nn.MSELoss()
loss_color = nn.MSELoss()

one = torch.FloatTensor([1])
mone = one * -1
aG = aG.to(device)
aD = aD.to(device)
one = one.to(device)
mone = mone.to(device)

writer = SummaryWriter()


def to_numpy(tensor):
    return tensor.cpu().data.numpy()


# Reference: https://github.com/caogang/wgan-gp/blob/master/gan_cifar10.py
def train():
    # writer = SummaryWriter()
    dataloader = mnist_loader(BATCH_SIZE)
    dataiter = iter(dataloader)
    plotter = libs.plot.Plotter(OUTPUT_PATH)
    for iteration in range(START_ITER, END_ITER):
        start_time = time.time()
        # print("Iter: " + str(iteration))
        start = timer()
        # ---------------------TRAIN G------------------------
        for p in aD.parameters():
            p.requires_grad_(False)  # freeze D

        gen_cost = None
        for i in range(GENER_ITERS):
            # print("Generator iters: " + str(i))
            aG.zero_grad()
            f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
            noise, fake_label = gen_rand_noise_with_label(f_label)
            noise.requires_grad_(True)
            fake_data = aG(noise).view(BATCH_SIZE, 1, IMG_SIZE, IMG_SIZE)
            gen_cost, gen_class = aD(fake_data.view(BATCH_SIZE, -1))

            aux_label = torch.from_numpy(f_label).long()
            aux_label = aux_label.to(device)
            class_errG = aux_criterion(gen_class, aux_label).mean()
            aux_errG = class_errG  # + pos_errG + col_errG
            gen_cost = -gen_cost.mean()
            g_cost = gen_cost + ACGAN_SCALE_G * aux_errG
            # print('gen_cost: {}, class_err: {} -> g_cost: {} (pos_err: {}, col_err: {})'
            #      .format(to_numpy(gen_cost), to_numpy(class_errG), to_numpy(g_cost), to_numpy(pos_errG),
            #              to_numpy(col_errG)))
            g_cost.backward()

        optimizer_g.step()
        end = timer()
        # print(out_fname'---train G elapsed time: {end - start}')
        # ---------------------TRAIN D------------------------
        for p in aD.parameters():  # reset requires_grad
            p.requires_grad_(True)  # they are set to False below in training G
        for i in range(CRITIC_ITERS):
            # print("Critic iter: " + str(i))

            start = timer()
            aD.zero_grad()

            # gen fake dataset and load real dataset
            f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
            noise, fake_class = gen_rand_noise_with_label(f_label)
            fake_class = torch.Tensor(fake_class).to(device).long()
            with torch.no_grad():
                noisev = noise  # totally freeze G, training D
            fake_data = aG(noisev).view(BATCH_SIZE, 1, IMG_SIZE, IMG_SIZE).detach()
            end = timer()
            # print(out_fname'---gen G elapsed time: {end-start}')
            start = timer()
            batch = next(dataiter, None)
            if batch is None:
                dataiter = iter(dataloader)
                batch = next(dataiter)
            real_data = batch[0]  # batch[1] contains labels
            real_data.requires_grad_(True)
            real_label = batch[1]
            # print("r_label" + str(r_label))
            end = timer()
            # print(out_fname'---load real imgs elapsed time: {end-start}')

            start = timer()
            real_data = real_data.to(device)
            real_label = real_label.to(device)

            # train with real dataset_iter
            disc_real, disc_class = aD(real_data.view(BATCH_SIZE, -1))
            class_errD_real = aux_criterion(disc_class, real_label)
            err_labels_real = class_errD_real.mean()
            disc_real = disc_real.mean()

            # train with fake dataset_iter
            disc_fake, disc_fclass = aD(fake_data.view(BATCH_SIZE, -1))
            class_errD_fake = aux_criterion(disc_fclass, fake_class)
            err_labels_fake = class_errD_fake.mean()
            disc_fake = disc_fake.mean()

            # showMemoryUsage(0)
            # train with interpolates dataset_iter
            gradient_penalty = calc_gradient_penalty(aD, real_data, fake_data)
            # showMemoryUsage(0)

            # final disc cost
            w_dist = (disc_fake - disc_real).abs()
            disc_cost = w_dist + gradient_penalty
            disc_acgan = err_labels_real + err_labels_fake
            total_cost = disc_cost #+ ACGAN_SCALE * disc_acgan
            (total_cost).backward()
            optimizer_d.step()

            # ------------------VISUALIZATION----------
            if i == CRITIC_ITERS - 1 and not OLDGAN:
                writer.add_scalar('dataset_iter/disc_cost', disc_cost, iteration)
                # writer.add_scalar('dataset_iter/disc_fake', disc_fake, iteration)
                # writer.add_scalar('dataset_iter/disc_real', disc_real, iteration)
                writer.add_scalar('dataset_iter/gradient_pen', gradient_penalty, iteration)
                writer.add_scalar('dataset_iter/ac_disc_cost', disc_acgan, iteration)
                writer.add_scalar('dataset_iter/ac_gen_cost', class_errG, iteration)

            end = timer();
            # print(out_fname'---train D elapsed time: {end-start}')
        # ---------------VISUALIZATION---------------------
        writer.add_scalar('dataset_iter/gen_cost', gen_cost, iteration)
        # if iteration %200==199:
        #   paramsG = G.named_parameters()
        #   for name, pG in paramsG:
        #       writer.add_histogram('G.' + name, pG.clone().dataset_iter.cpu().numpy(), iteration)
        # ----------------------Generate images-----------------

        plotter.plot('time', time.time() - start_time)
        plotter.plot('train_disc_cost', disc_cost.cpu().data.numpy())
        plotter.plot('train_gen_cost', gen_cost.cpu().data.numpy())
        plotter.plot('wasserstein_distance', w_dist.cpu().data.numpy())
        plotter.plot('err_labels', disc_acgan.cpu().data.numpy())
        if iteration % VAL_FREQ == (VAL_FREQ - 1):
            # print('iter {}\tdisc_cost: {:.5f}, gen_cost: {:.5f}, wdist: {:.5f}'
            #      .format(iteration, disc_cost.cpu().data.numpy(), gen_cost.cpu().data.numpy(),
            #              w_dist.cpu().data.numpy()))
            val_loader = mnist_loader(BATCH_SIZE, train=False)
            dev_disc_costs = []
            for images, _ in val_loader:
                imgs = images.view(BATCH_SIZE, -1)
                imgs = imgs.to(device)
                with torch.no_grad():
                    imgs_v = imgs

                D, _ = aD(imgs_v)
                _dev_disc_cost = -D.mean().cpu().data.numpy()
                dev_disc_costs.append(_dev_disc_cost)
            plotter.plot('dev_disc_cost.png', np.mean(dev_disc_costs))
            plotter.flush(print_info=True)
            gen_images = generate_image(aG, fixed_noise)
            torchvision.utils.save_image(gen_images, OUTPUT_PATH + 'samples_{}.png'.format(iteration), nrow=8,
                                         padding=2)
            grid_images = torchvision.utils.make_grid(gen_images, nrow=N_ROWS, padding=2)
            writer.add_image('images', grid_images, iteration)
            # gen_images = generate_image(iteration, G, persistant_noise)
            # gen_images = torchvision.utils.make_grid(torch.from_numpy(gen_images), nrow=8, padding=1)
            # writer.add_image('images', gen_images, iteration)
            # ----------------------Save cnn----------------------
            torch.save(aG, OUTPUT_PATH + "generator.pt")
            torch.save(aD, OUTPUT_PATH + "discriminator.pt")
        plotter.tick()


if __name__ == '__main__':
    train()
