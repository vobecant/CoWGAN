import os, sys

sys.path.append(os.getcwd())

import numpy as np
import matplotlib

matplotlib.use('agg')
from matplotlib import pyplot as plt
from models.CNN import CNN
import torch
import torchvision
from torch import nn
from torch import autograd
from torch import optim
from torchvision import transforms, datasets
from torch.autograd import grad
from timeit import default_timer as timer
from models.dataset import GeomDataset
from libs.save import Saver
import libs.plot as plot
from libs.plot import Plotter

# ========= DATA DIRECTORIES ============
DATA_SPECIF = 'mnist'
DATA_DIR = './datasets/' + DATA_SPECIF + '/train'
CSV_FILE_TRAIN = DATA_DIR + '/geom_data_bottom.csv'
VAL_DIR = './datasets/' + DATA_SPECIF + '/val'
CSV_FILE_VAL = VAL_DIR + '/geom_data_bottom.csv'
OUTPUT_PATH = './result/CNN/' + DATA_SPECIF + '/'

# ========= CONSTANTS ============
N_EPOCHS = 1
LEARNING_RATE = 0.001
BATCH_SIZE = 64
PLOT_FREQ = 1
SAVE_FREQ = 1
# Device configuration
cuda_available = torch.cuda.is_available()
device = torch.device('cuda:0' if cuda_available else 'cpu')


# ======== FUNCTION DEFINITIONS ==========

def load_my_data(path_to_folder, csvfile):
    resize = transforms.Resize(64)
    crop = transforms.CenterCrop(64)
    to_tensor = transforms.ToTensor()
    dataset = GeomDataset(csvfile, path_to_folder, resize, crop, to_tensor)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True, num_workers=5,
                                                 drop_last=True, pin_memory=True)
    # print('loaded dataset_iter, number of samples: {}'.format(len(dataset_iter)))
    return dataset_loader


def save_network(model, path, epoch):
    torch.save(model.state_dict(), path + '/model_{}.ckpt'.format(epoch))


def train(model, criterion, train_loader, val_loader, out_folder):
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver = Saver('val_loss', better_value_fnc, out_folder)
    print('start training')
    optimizer = optim.Adam(model.parameters(), lr=LEARNING_RATE)
    plotter = Plotter(out_folder)
    for epoch in range(N_EPOCHS):
        start_time = timer()
        # print('epoch {}\ttrain start'.format(epoch))
        ep_start = timer()
        # ------ training -------
        train_losses = np.asarray([])
        total_train, correct_train = 0, 0
        model.train()
        for i, batch in enumerate(train_loader):
            # if i == 5: break
            # Forward pass
            images = batch[0]
            labels = batch[1]
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)

            # forward pass
            loss = criterion(outputs, labels)
            train_losses = np.append(train_losses, loss.item())

            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            total_train += labels.size(0)
            correct_train += (predicted == labels).sum().item()
            if i % 100 == 0: print('iteration: {}, correct: {}'.format(i, correct_train / total_train))

        train_acc = 100 * correct_train / total_train
        train_loss = np.mean(train_losses)

        # ------ validation ------
        # print('epoch {}\tvalidation start'.format(epoch))
        val_loss, val_acc = validate(val_loader, model, criterion)

        # ------ create plots ------
        plotter.plot('time', timer() - start_time)
        plotter.plot('train_loss', train_loss)
        plotter.plot('val_loss', val_loss)
        plotter.plot('train_acc', train_acc)
        plotter.plot('val_acc', val_acc)
        if epoch % PLOT_FREQ == (PLOT_FREQ - 1):
            plotter.flush(print_info=True)
        plotter.tick()

        # ------ save -------
        saver.update('cnn', model)
        saver.update('cnn_w', model.state_dict())
        saver.update('optimizer', optimizer.state_dict())
        saver.update('train_loss', train_loss)
        saver.update('val_loss', val_loss)
        saver.update('train_acc', train_acc)
        saver.update('val_acc', val_acc)
        saver.update('epoch', epoch)
        if epoch % SAVE_FREQ == (SAVE_FREQ - 1):
            saver.flush()
        saver.tick()

        # ----- print info ------
        print('Epoch [{}/{}], Train Loss: {:.6f}, Train Acc: {:.2f}, Val Loss: {:.6f}, Val Acc: {:.2f}, time: {:.3f}'
              .format(epoch + 1, N_EPOCHS, train_loss, train_acc, val_loss, val_acc, timer() - ep_start))
    create_pairs(model, train_loader, val_loader)


def create_pairs(model, train_loader, test_loader, mu=0.1307, std=0.3081):
    codes = []
    for i, data in enumerate(train_loader.dataset):
        img = data[0].unsqueeze(0).to(device)
        code = model(img, conv_only=True).view(-1).cpu().data.numpy()
        codes.append(code)
        if (i + 1) % 1000 == 0: print('{:d} training images processed'.format(i + 1))
    np.save('./datasets/mnist_codes_train', codes)
    codes = []
    for i, data in enumerate(test_loader.dataset):
        img = data[0].unsqueeze(0).to(device)
        code = model(img, conv_only=True).view(-1).cpu().data.numpy()
        codes.append(code)
        if (i + 1) % 1000 == 0: print('{:d} test images processed'.format(i + 1))
    np.save('./datasets/mnist_codes_test', codes)


def unscale(img, mu, var):
    return img.mul(var).add(mu)


def validate(val_loader, model, criterion, show=False):
    # switch to evaluate mode
    model.eval()

    losses = np.asarray([])
    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(val_loader):
            input = batch[0]
            target = batch[1]
            input = input.to(device)
            target = target.to(device)
            # compute output
            output = model(input)
            _, predicted = torch.max(output.data, 1)
            loss = criterion(output, target)
            losses = np.append(losses, loss.cpu().data.numpy())
            total += target.size(0)
            correct += (predicted == target).sum().item()
            if show:
                fig, ax = plt.subplots()
                for sample, pred, gt in zip(input, target, predicted):
                    show_sample(sample, pred, gt, ax)
    val_loss = np.mean(losses)
    val_acc = 100 * correct / total
    return val_loss, val_acc


def show_sample(sample, pred, gt, ax):
    sample = sample.data.numpy()
    pred = pred.data.numpy()
    gt = gt.data.numpy()
    im = np.rollaxis(sample, 0, 3)
    ax.cla()
    ax.imshow(im)
    ax.set_title('pred {}, gt {}'.format(pred, gt))
    plt.pause(0.5)


if __name__ == '__main__':

    model = CNN(input_channels=1, num_classes=10, image_side=32)
    print('used device: {}'.format('gpu' if cuda_available else 'cpu'))
    if cuda_available:
        model = model.to(device)
    criterion = nn.NLLLoss()  # nn.CrossEntropyLoss()
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=True, download=True,
                       transform=transforms.Compose([
                           transforms.Resize(32),
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=BATCH_SIZE, shuffle=True, num_workers=5, pin_memory=True)
    val_loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=False, transform=transforms.Compose([
            transforms.Resize(32),
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])),
        batch_size=BATCH_SIZE, shuffle=True, num_workers=5, pin_memory=True)

    out_folder = OUTPUT_PATH
    train(model, criterion, train_loader, val_loader, out_folder)

    model = torch.load('./result/CNN/mnist/cnn_last.pt').to(device)
    create_pairs(model, train_loader, val_loader)
