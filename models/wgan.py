from torch import nn
from torch.autograd import grad
import torch
import numpy as np

DIM = 64
DIM_CONV = 128
IM_SIZE_CONV = 8
OUTPUT_DIM = 64 * 64 * 3
OUTPUT_DIM_CONV = 128 * 8 * 8
OUTPUT_DIM_FC = 128


class MyConvo2d(nn.Module):
    def __init__(self, input_dim, output_dim, kernel_size, he_init=True, stride=1, bias=True):
        super(MyConvo2d, self).__init__()
        self.he_init = he_init
        self.padding = int((kernel_size - 1) / 2)
        self.conv = nn.Conv2d(input_dim, output_dim, kernel_size, stride=1, padding=self.padding, bias=bias)

    def forward(self, input):
        output = self.conv(input)
        return output


class ConvMeanPool(nn.Module):
    def __init__(self, input_dim, output_dim, kernel_size, he_init=True):
        super(ConvMeanPool, self).__init__()
        self.he_init = he_init
        self.conv = MyConvo2d(input_dim, output_dim, kernel_size, he_init=self.he_init)

    def forward(self, input):
        output = self.conv(input)
        output = (output[:, :, ::2, ::2] + output[:, :, 1::2, ::2] + output[:, :, ::2, 1::2] + output[:, :, 1::2,
                                                                                               1::2]) / 4
        return output


class MeanPoolConv(nn.Module):
    def __init__(self, input_dim, output_dim, kernel_size, he_init=True):
        super(MeanPoolConv, self).__init__()
        self.he_init = he_init
        self.conv = MyConvo2d(input_dim, output_dim, kernel_size, he_init=self.he_init)

    def forward(self, input):
        output = input
        output = (output[:, :, ::2, ::2] + output[:, :, 1::2, ::2] + output[:, :, ::2, 1::2] + output[:, :, 1::2,
                                                                                               1::2]) / 4
        output = self.conv(output)
        return output


class DepthToSpace(nn.Module):
    def __init__(self, block_size):
        super(DepthToSpace, self).__init__()
        self.block_size = block_size
        self.block_size_sq = block_size * block_size

    def forward(self, input):
        output = input.permute(0, 2, 3, 1)
        (batch_size, input_height, input_width, input_depth) = output.size()
        output_depth = int(input_depth / self.block_size_sq)
        output_width = int(input_width * self.block_size)
        output_height = int(input_height * self.block_size)
        t_1 = output.contiguous().view(batch_size, input_height, input_width, self.block_size_sq, output_depth)
        spl = t_1.split(self.block_size, 3)
        stacks = [t_t.contiguous().view(batch_size, input_height, output_width, output_depth) for t_t in spl]
        output = torch.stack(stacks, 0).transpose(0, 1).permute(0, 2, 1, 3, 4).contiguous().view(batch_size,
                                                                                                 output_height,
                                                                                                 output_width,
                                                                                                 output_depth)
        output = output.permute(0, 3, 1, 2)
        return output


class UpSampleConv(nn.Module):
    def __init__(self, input_dim, output_dim, kernel_size, he_init=True, bias=True):
        super(UpSampleConv, self).__init__()
        self.he_init = he_init
        self.conv = MyConvo2d(input_dim, output_dim, kernel_size, he_init=self.he_init, bias=bias)
        self.depth_to_space = DepthToSpace(2)

    def forward(self, input):
        output = input
        output = torch.cat((output, output, output, output), 1)
        output = self.depth_to_space(output)
        output = self.conv(output)
        return output


class ResidualBlock(nn.Module):
    def __init__(self, input_dim, output_dim, kernel_size, resample=None, hw=DIM):
        super(ResidualBlock, self).__init__()

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.kernel_size = kernel_size
        self.resample = resample
        self.bn1 = None
        self.bn2 = None
        self.relu1 = nn.ReLU()
        self.relu2 = nn.ReLU()
        if resample == 'down':
            self.bn1 = nn.LayerNorm([input_dim, hw, hw])
            self.bn2 = nn.LayerNorm([input_dim, hw, hw])
        elif resample == 'up':
            self.bn1 = nn.BatchNorm2d(input_dim)
            self.bn2 = nn.BatchNorm2d(output_dim)
        elif resample == None:
            # TODO: ????
            self.bn1 = nn.BatchNorm2d(output_dim)
            self.bn2 = nn.LayerNorm([input_dim, hw, hw])
        else:
            raise Exception('invalid resample value')

        if resample == 'down':
            self.conv_shortcut = MeanPoolConv(input_dim, output_dim, kernel_size=1, he_init=False)
            self.conv_1 = MyConvo2d(input_dim, input_dim, kernel_size=kernel_size, bias=False)
            self.conv_2 = ConvMeanPool(input_dim, output_dim, kernel_size=kernel_size)
        elif resample == 'up':
            self.conv_shortcut = UpSampleConv(input_dim, output_dim, kernel_size=1, he_init=False)
            self.conv_1 = UpSampleConv(input_dim, output_dim, kernel_size=kernel_size, bias=False)
            self.conv_2 = MyConvo2d(output_dim, output_dim, kernel_size=kernel_size)
        elif resample == None:
            self.conv_shortcut = MyConvo2d(input_dim, output_dim, kernel_size=1, he_init=False)
            self.conv_1 = MyConvo2d(input_dim, input_dim, kernel_size=kernel_size, bias=False)
            self.conv_2 = MyConvo2d(input_dim, output_dim, kernel_size=kernel_size)
        else:
            raise Exception('invalid resample value')

    def forward(self, input):
        if self.input_dim == self.output_dim and self.resample == None:
            shortcut = input
        else:
            shortcut = self.conv_shortcut(input)

        output = input
        output = self.bn1(output)
        output = self.relu1(output)
        output = self.conv_1(output)
        output = self.bn2(output)
        output = self.relu2(output)
        output = self.conv_2(output)

        return shortcut + output


class ReLULayer(nn.Module):
    def __init__(self, n_in, n_out):
        super(ReLULayer, self).__init__()
        self.n_in = n_in
        self.n_out = n_out
        self.linear = nn.Linear(n_in, n_out)
        self.relu = nn.ReLU()

    def forward(self, input):
        output = self.linear(input)
        output = self.relu(output)
        return output


class FCGenerator(nn.Module):
    def __init__(self, FC_DIM=128, input_dim=128, output_dim=OUTPUT_DIM_FC):
        super(FCGenerator, self).__init__()
        self.relulayer1 = ReLULayer(input_dim, FC_DIM)
        self.relulayer2 = ReLULayer(FC_DIM, 2 * FC_DIM)
        self.relulayer3 = ReLULayer(2 * FC_DIM, 4 * FC_DIM)
        self.relulayer4 = ReLULayer(4 * FC_DIM, 4 * FC_DIM)
        self.linear = nn.Linear(4 * FC_DIM, output_dim)

    def forward(self, input):
        output = self.relulayer1(input)
        output = self.relulayer2(output)
        output = self.relulayer3(output)
        output = self.relulayer4(output)
        output = self.linear(output)
        return output


class FCNetShallow(nn.Module):
    def __init__(self, FC_DIM=128, input_dim=128, output_dim=OUTPUT_DIM_FC, leak_rate=0.1):
        super(FCNetShallow, self).__init__()
        self.hidden = nn.Sequential(
            nn.Linear(input_dim, FC_DIM),
            nn.LeakyReLU(leak_rate)
        )
        self.linear = nn.Linear(FC_DIM, output_dim)

    def forward(self, input):
        output = self.hidden(input)
        output = self.linear(output)
        return output


class FCDiscriminatorCond(nn.Module):
    def __init__(self, n_classes, FC_DIM=128, input_dim=128, output_dim=OUTPUT_DIM_FC, leak_rate=0.1):
        super(FCDiscriminatorCond, self).__init__()
        self.hidden = nn.Sequential(
            nn.Linear(input_dim, FC_DIM),
            nn.LeakyReLU(leak_rate)
        )
        self.linear = nn.Linear(FC_DIM, output_dim)
        self.class_output = nn.Linear(FC_DIM, n_classes)

    def forward(self, input):
        hidden = self.hidden(input)
        gan_output = self.linear(hidden)
        class_output = self.class_output(hidden)
        return gan_output, class_output


class FCGeneratorCond(nn.Module):
    def __init__(self, FC_DIM=128, input_dim=128, output_dim=OUTPUT_DIM_FC, leak_rate=0.1):
        super(FCGeneratorCond, self).__init__()
        self.hidden = nn.Sequential(
            nn.Linear(input_dim, FC_DIM),
            nn.LeakyReLU(leak_rate)
        )
        self.linear = nn.Linear(FC_DIM, output_dim)

    def forward(self, input):
        output = self.hidden(input)
        output = self.linear(output)
        return output


class FCNet(nn.Module):
    def __init__(self, FC_DIM=128, input_dim=128, output_dim=OUTPUT_DIM_FC, leak_rate=0.1):
        super(FCNet, self).__init__()
        self.hidden1 = nn.Sequential(
            nn.Linear(input_dim, FC_DIM),
            nn.LeakyReLU(leak_rate)
        )
        self.hidden2 = nn.Sequential(
            nn.Linear(FC_DIM, FC_DIM),
            nn.LeakyReLU(leak_rate)
        )
        self.linear = nn.Linear(FC_DIM, output_dim)

    def forward(self, input):
        output = self.hidden1(input)
        output = self.hidden2(output)
        output = self.linear(output)
        return output


class FCDiscriminator(nn.Module):
    def __init__(self, FC_DIM=64, input_dim=2):
        super(FCDiscriminator, self).__init__()
        self.relulayer1 = ReLULayer(input_dim, 8 * FC_DIM)
        self.relulayer2 = ReLULayer(8 * FC_DIM, 4 * FC_DIM)
        self.relulayer3 = ReLULayer(4 * FC_DIM, 2 * FC_DIM)
        self.relulayer4 = ReLULayer(2 * FC_DIM, FC_DIM)
        self.linear = nn.Linear(FC_DIM, 1)

    def forward(self, input):
        output = self.relulayer1(input)
        output = self.relulayer2(output)
        output = self.relulayer3(output)
        output = self.relulayer4(output)
        output = self.linear(output)
        return output

    '''
    def __init__(self, conv_dim=OUTPUT_DIM_FC):
        print('Creating FCDiscriminator, conv_dim: {}'.format(conv_dim))
        super(FCDiscriminator, self).__init__()
        self.layers = []
        n_layers = int(np.floor(np.log2(conv_dim)))
        first_layer = conv_dim - 2 ** n_layers
        if first_layer > 0:
            print()
            l = ReLULayer(conv_dim, conv_dim - first_layer)
            print('in {}, out: {}'.format(conv_dim,conv_dim-first_layer))
            self.layers.append(l)
        units = conv_dim - first_layer
        for _ in range(n_layers):
            units_half = int(units/2)
            print('in: {}, out: {}'.format(units,units_half))
            l = ReLULayer(units, units_half)
            units /= 2
            self.layers.append(l)
    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x
    '''


class GoodGenerator(nn.Module):
    def __init__(self, input_dim, dim=DIM, image_dim=1, image_side=64):
        super(GoodGenerator, self).__init__()

        self.dim = dim
        self.output_dim = image_side * image_side * image_dim

        self.ln1 = nn.Linear(input_dim, 4 * 4 * 8 * self.dim)
        self.rb1 = ResidualBlock(8 * self.dim, 8 * self.dim, 3, resample='up')
        self.rb2 = ResidualBlock(8 * self.dim, 4 * self.dim, 3, resample='up')
        self.rb3 = ResidualBlock(4 * self.dim, 2 * self.dim, 3, resample='up')
        self.rb4 = ResidualBlock(2 * self.dim, 1 * self.dim, 3, resample='up')
        self.bn = nn.BatchNorm2d(self.dim)

        self.conv1 = MyConvo2d(1 * self.dim, 1, 3)
        self.relu = nn.ReLU()
        self.tanh = nn.Tanh()

    def forward(self, input):
        output = self.ln1(input.contiguous())
        output = output.view(-1, 8 * self.dim, 4, 4)
        output = self.rb1(output)
        output = self.rb2(output)
        output = self.rb3(output)
        output = self.rb4(output)

        output = self.bn(output)
        output = self.relu(output)
        output = self.conv1(output)
        output = self.tanh(output)
        output = output.view(-1, self.output_dim)
        return output


class GoodGeneratorMnist(nn.Module):
    def __init__(self, input_dim, dim=DIM, image_dim=1, image_side=64):
        super(GoodGeneratorMnist, self).__init__()

        self.dim = dim
        self.output_dim = image_side * image_side * image_dim

        self.ln1 = nn.Linear(input_dim, 4 * 4 * 4 * self.dim)
        self.rb2 = ResidualBlock(4 * self.dim, 4 * self.dim, 3, resample='up')
        self.rb3 = ResidualBlock(4 * self.dim, 2 * self.dim, 3, resample='up')
        self.rb4 = ResidualBlock(2 * self.dim, 1 * self.dim, 3, resample='up')
        self.bn = nn.BatchNorm2d(self.dim)

        self.conv1 = MyConvo2d(1 * self.dim, 1, 3)
        self.relu = nn.ReLU()
        self.tanh = nn.Tanh()

    def forward(self, input):
        output = self.ln1(input.contiguous())
        output = output.view(-1, 4 * self.dim, 4, 4)
        output = self.rb2(output)
        output = self.rb3(output)
        output = self.rb4(output)

        output = self.bn(output)
        output = self.relu(output)
        output = self.conv1(output)
        output = self.tanh(output)
        #output = output.view(-1, self.output_dim)
        return output


class GoodDiscriminator(nn.Module):
    def __init__(self, dim=DIM, image_dim=3, image_side=64):
        super(GoodDiscriminator, self).__init__()

        self.dim = dim
        self.image_dim = image_dim
        self.image_side = image_side
        self.last_conv_size = int(image_side / 16)

        self.conv1 = MyConvo2d(image_dim, self.dim, 3, he_init=False)
        self.rb1 = ResidualBlock(self.dim, 2 * self.dim, 3, resample='down', hw=dim)
        self.rb2 = ResidualBlock(2 * self.dim, 4 * self.dim, 3, resample='down', hw=int(dim / 2))
        self.rb3 = ResidualBlock(4 * self.dim, 8 * self.dim, 3, resample='down', hw=int(dim / 4))
        self.rb4 = ResidualBlock(8 * self.dim, 8 * self.dim, 3, resample='down', hw=int(dim / 8))
        self.ln1 = nn.Linear(self.last_conv_size * self.last_conv_size * 8 * self.dim, 1)

    def forward(self, input):
        output = input.contiguous()
        output = output.view(-1, self.image_dim, self.image_side, self.image_side)
        output = self.conv1(output)
        output = self.rb1(output)
        output = self.rb2(output)
        output = self.rb3(output)
        output = self.rb4(output)
        output = output.view(-1, self.last_conv_size * self.last_conv_size * 8 * self.dim)
        output = self.ln1(output)
        output = output.view(-1)
        return output


class GoodGeneratorConv(nn.Module):
    def __init__(self, conv_dim, model_dim=DIM_CONV, output_dim=OUTPUT_DIM, parallel=False):
        super(GoodGeneratorConv, self).__init__()
        self.model_dim = model_dim
        self.conv_dim = conv_dim
        self.ln1 = nn.Linear(128, 1 * 1 * 4 * self.model_dim)
        self.rb1 = ResidualBlock(4 * self.model_dim, 4 * self.model_dim, 3, resample='up')
        self.rb2 = ResidualBlock(4 * self.model_dim, 2 * self.model_dim, 3, resample='up')
        self.rb3 = ResidualBlock(2 * self.model_dim, 1 * self.model_dim, 3, resample='up')
        self.relu_out = nn.ReLU()

    def forward(self, input):
        output = self.ln1(input.contiguous())
        output = output.view(-1, 4 * self.model_dim, 1, 1)
        output = self.rb1(output)
        output = self.rb2(output)
        output = self.rb3(output)
        output = self.relu_out(output)
        output = output.view(-1, self.conv_dim)
        return output


class GoodDiscriminatorConv(nn.Module):
    def __init__(self, conv_dim, net_dim=DIM, parallel=False):
        super(GoodDiscriminatorConv, self).__init__()
        self.dim = net_dim
        self.conv_dim = conv_dim
        self.conv1 = MyConvo2d(conv_dim, self.dim, 3, he_init=False)
        self.rb1 = ResidualBlock(self.dim, 2 * self.dim, 3, resample='down', hw=IM_SIZE_CONV)
        self.rb2 = ResidualBlock(2 * self.dim, 4 * self.dim, 3, resample='down', hw=int(IM_SIZE_CONV / 2))
        self.rb3 = ResidualBlock(4 * self.dim, 8 * self.dim, 3, resample='down', hw=int(IM_SIZE_CONV / 4))
        self.ln1 = nn.Linear(1 * 1 * 8 * self.dim, 1)

    def forward(self, input):
        batch_size = input.size(0)
        conv_size = int(np.sqrt(input.size(1) / (batch_size * self.conv_dim)))
        output = input.contiguous()
        output = output.view(batch_size, self.conv_dim, conv_size, conv_size)
        output = self.conv1(output)
        output = self.rb1(output)
        output = self.rb2(output)
        output = self.rb3(output)
        # output = self.rb4(output)
        output = output.view(-1, 1 * 1 * 8 * self.dim)
        output = self.ln1(output)
        output = output.view(-1)
        return output
