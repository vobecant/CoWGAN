import torch
from torch.utils.data import Dataset
import pandas as pd
import os
from skimage import io
from torchvision import transforms, datasets
from PIL import Image
import numpy as np


class MnistCodesDataset(Dataset):
    def __init__(self, data_file, n_samples=60000):
        super(MnistCodesDataset, self).__init__()
        self.root = data_file
        # self.codes = np.load(self.root)
        n_samples, samples_dim = n_samples, 8192
        self.codes = np.memmap(self.root, dtype=np.float32, mode='r', shape=(n_samples, samples_dim))

    def __len__(self):
        return len(self.codes)

    def to_uint8(self, img):
        return (img * 255).astype('uint8')

    def __getitem__(self, index):
        code = self.codes[index]
        code = torch.Tensor(code)

        return code


class MnistPairsDataset(Dataset):
    def __init__(self, data_file, n_samples=60000):
        super(MnistPairsDataset, self).__init__()
        self.root = data_file
        # self.codes = np.load(self.root)
        n_samples, samples_dim = n_samples, 8192
        self.image_transforms = transforms.Compose([transforms.Resize(32), transforms.ToTensor(),
                                                    transforms.Normalize((0.1307,), (0.3081,))])
        self.codes = np.memmap(self.root, dtype=np.float32, mode='r', shape=(n_samples, samples_dim))
        self.mnist = datasets.MNIST('../data', train=True, download=True,
                                     transform=transforms.Compose([transforms.Resize(32), transforms.ToTensor(),
                                                                   transforms.Normalize((0.1307,),
                                                                                        (0.3081,))]))

    def __len__(self):
        return len(self.codes)

    def __getitem__(self, index):
        code = self.codes[index]
        code = torch.Tensor(code)
        mnist = self.mnist[index]
        image = mnist[0]
        label = mnist[1]
        #print('code: {}, image: {}, label: {}'.format(code.size(), image.size(), label.size()))

        return code, image, label


class GeomDataset(Dataset):
    def __init__(self, csv_file, root_dir, transform):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.data_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.data_frame)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir,
                                self.data_frame.iloc[idx, 0])
        image = io.imread(img_name)
        image = image[:, :, :3]
        geom_type = self.data_frame.iloc[idx, 1]
        color = torch.Tensor(self.char2color(self.data_frame.iloc[idx, 2]))
        data = self.data_frame.iloc[idx, 3]
        obj_info = self.get_params(geom_type, data)
        position = torch.Tensor(obj_info[0])
        # sample = {'image': image, 'geom_type':geom_type,'color': color, 'dataset_iter': dataset_iter}

        image = Image.fromarray(image)
        image = self.transform(image)
        # geom_type = self.to_tensor(geom_type)
        # color = self.to_tensor(color)
        # dataset_iter = self.to_tensor(dataset_iter)

        return image, geom_type, color, position  # ,dataset_iter

    def get_params(self, geom_type, data):
        if geom_type in [0, 1]:
            position = [float(data.split(',')[0][1:]), float(data.split(',')[1])]
            if geom_type == 0:
                radius = float(data.split(',')[2][:-1])
                return position, radius
            else:
                width = float(data.split(',')[2])
                height = float(data.split(',')[3])
                angle = float(data.split(',')[4][:-1])
                return position, width, height, angle
        else:  # triangle
            splitted = data.replace('array', '').split(')')
            position = [float(splitted[0].split(',')[0][4:]), float(splitted[0].split(',')[1][:-1])]
            angle = float(splitted[-1][2:-1])
            return position, angle

    def char2color(self, char):
        table = {'r': [1, 0, 0], 'g': [0, 1, 0], 'b': [0, 0, 1]}
        return table[char]


class GeomDataset_new(Dataset):
    def __init__(self, csv_file, root_dir, transform, n_channels=3, binary=False, obj=None, color=None, xpos=None,
                 ypos=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.data_frame = pd.read_csv(csv_file)
        self.restrict(obj, color, xpos, ypos)
        self.root_dir = root_dir
        self.transform = transform
        self.n_channels = n_channels
        self.binary = binary

    def __len__(self):
        return len(self.data_frame)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir,
                                self.data_frame.iloc[idx, 0])
        image = io.imread(img_name)
        image = image[:, :, :self.n_channels]
        if self.binary:
            image = ((image[:, :, 0] > 0) & ((image[:, :, 1] > 0)) * 1).astype('uint16')
        geom_type = self.data_frame.iloc[idx, 1]
        color = self.data_frame.iloc[idx, 2]
        data = self.data_frame.iloc[idx, 3:].values
        # sample = {'image': image, 'geom_type':geom_type,'color': color, 'dataset_iter': dataset_iter}

        image = Image.fromarray(image)
        image = self.transform(image)
        # geom_type = self.to_tensor(geom_type)
        # color = self.to_tensor(color)
        # dataset_iter = self.to_tensor(dataset_iter)

        return image, geom_type  # , color, img_name, idx  # , data  # ,dataset_iter

    def restrict(self, obj, color, xpos, ypos):
        idx = np.arange(0, self.__len__())
        if obj:
            for o in obj:
                oidx = self.get_specif_idx(obj=o)
                idx = np.intersect1d(idx, oidx)
                self.data_frame = self.data_frame.iloc[idx]
            idx = np.arange(0, self.__len__())
        if color:
            for c in color:
                cidx = self.get_specif_idx(color=c)
                idx = np.intersect1d(idx, cidx)
                self.data_frame = self.data_frame.iloc[idx]
        idx = np.arange(0, self.__len__())
        if xpos:
            xidx = np.asarray([])
            for xrange in xpos:
                print('xrange: {}'.format(xrange))
                xidx = np.concatenate([xidx, self.get_specif_idx(xpos=xrange)])
                print('xidx size: {}'.format(xidx.shape))
            idx = np.intersect1d(idx, xidx)
            self.data_frame = self.data_frame.iloc[idx]
        idx = np.arange(0, self.__len__())
        if ypos:
            yidx = np.asarray([])
            for yrange in ypos:
                # print('yrange: {}'.format(yrange))
                yidx = np.concatenate([yidx, self.get_specif_idx(ypos=yrange)])

            idx = np.intersect1d(idx, yidx)
            self.data_frame = self.data_frame.iloc[idx]

    def get_specif_idx(self, obj=None, color=None, xpos=None, ypos=None):
        idx = []
        for i in range(self.__len__()):
            if obj is not None and not self.good_obj(i, obj): continue
            if color is not None and not self.good_color(i, color): continue
            if xpos is not None and not self.good_position(i, xpos, x=True): continue
            if ypos is not None and not self.good_position(i, ypos, x=False): continue
            idx.append(i)
        return idx

    def good_obj(self, i, obj):
        if int(self.data_frame.iloc[i, 1]) == obj:
            return True
        return False

    def good_color(self, i, color):
        if self.data_frame.iloc[i, 2] == color:
            return True
        return False

    def good_position(self, i, pos_range, x=True):
        pos = self.data_frame.iloc[i, 3]
        xpos, ypos = self.str2arr(pos)[:2]
        pos = xpos if x else ypos
        if self.in_range(pos, pos_range):
            return True
        print('{}={} not between {}'.format('x' if x else 'y', pos, pos_range))
        return False

    def str2arr(self, str):
        splitted = str.split(',')
        arr = []
        for i, s in enumerate(splitted):
            if i == 0:
                arr.append(float(s[1:]))
            elif i == (len(splitted) - 1):
                arr.append(float(s[:-1]))
            else:
                arr.append(float(s))
        return arr

    def in_range(self, value, range):
        if value >= range[0] and value <= range[1]:
            return True
        return False


class ConvDataset(Dataset):
    def __init__(self, labels_file, data_file):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.labels = np.load(labels_file)
        self.data = np.load(data_file)

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, idx):
        return self.data[idx, :, :, :], self.labels[idx]


class Dataset2D(Dataset):
    def __init__(self, data_file, labels_file=None):
        self.data = np.load(data_file)
        if labels_file is not None: self.labels = np.load(labels_file)

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, idx):
        return [self.data[idx, :], self.labels[idx]]

    '''
    n_samples = 4000
    centers = [(-5,0),(5,0)]
    X, y = make_blobs(n_samples=n_samples, n_features=2, cluster_std=1.0,
                      centers=centers, shuffle=False, random_state=42)
    np.save('./datasets/blob_train_data',X)
    n_values = len(centers)
    y = np.eye(n_values)[y]
    np.save('./datasets/blob_train_data',y)
    '''


class GeomDataset3d(Dataset):
    def __init__(self, csv_file, root_dir, transform, n_channels=3):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.data_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform
        self.n_channels = n_channels

    def __len__(self):
        return len(self.data_frame)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir, self.data_frame.iloc[idx, 1])
        image = io.imread(img_name)
        image = image[:, :, :self.n_channels]
        object_type = self.data_frame.iloc[idx, 2]
        color = self.data_frame.iloc[idx, 3]
        bounding_box = self.data_frame.iloc[idx, 4]
        object_name = self.data_frame.iloc[idx, 5]
        rotation = self.data_frame.iloc[idx, 6]
        scale = self.data_frame.iloc[idx, 7]

        image = Image.fromarray(image)
        image = self.transform(image)
        # object_type = transforms.ToTensor(object_type)
        # geom_type = self.to_tensor(geom_type)
        # color = self.to_tensor(color)
        # dataset_iter = self.to_tensor(dataset_iter)

        return image, object_type, color, bounding_box, object_name, rotation, scale


def to_array(string_array):
    new_array = []
    for row in string_array:
        row = row[1:-1].split(',')
        tmp = [float(n) for n in row]
        new_array.append(tmp)
    return new_array


def limit_range(values, limits):
    limited_idx = []
    for i, val in enumerate(values):
        if np.less_equal(limits[0], val).all() and np.greater_equal(limits[1], val).all():
            limited_idx.append(i)
    return limited_idx


def limit_and_divide_dataset(csv_fname_orig, csv_new_path, name='', objects=None, colors=None, bb_range=None,
                             scale_range=None, rotation_range=None):
    """
    Creates a new csv fulfilling given requirements
    :param csv_fname_orig: path to the csv file with information
    :param csv_new_path: path to the new csv folder
    :param objects: which objects to choose
    :param colors: which colors to choose
    :param bb_range: 2x4 array, first row = min values, second row = max values; -> one row: [min_X, max_X, min_Y, max_Y]
    :param scale_range: [min,max] scale_range
    :param rotation_range: 2x3 array, first row = min, second row = max, -> one row: [rot_X, rot_Y, rot_Z]
    :return: number of samples fulfilling
    """
    data_frame = pd.read_csv(csv_fname_orig, header=None)
    idx = np.arange(0, data_frame.shape[0])
    print('Dataset size: {}'.format(len(idx)))
    if objects is not None:
        print('Objects restricted to {}'.format(objects))
        geom_type = data_frame.iloc[:, 1]
        geom_idx = np.where(np.isin(geom_type, objects))
        idx = np.intersect1d(idx, geom_idx)
    if colors is not None:
        print('Colors restricted to {}'.format(colors))
        color = data_frame.iloc[:, 2]
        color_idx = np.where(np.isin(color, colors))
        idx = np.intersect1d(idx, color_idx)
    if bb_range is not None:
        print('Bounding box restricted to range from {} to {}'.format(bb_range[0], bb_range[1]))
        bb = to_array(data_frame.iloc[:, 3])
        bb_idx = limit_range(bb, bb_range)
        idx = np.intersect1d(idx, bb_idx)
    if rotation_range is not None:
        print('Rotation restricted to range from {} to {}'.format(rotation_range[0], rotation_range[1]))
        rotation = to_array(data_frame.iloc[:, 5])
        rot_idx = limit_range(rotation, rotation_range)
        idx = np.intersect1d(idx, rot_idx)
    if scale_range is not None:
        print('Scale restricted to {}'.format(scale_range))
        scale = data_frame.iloc[:, 6]
        scale_idx = np.where(np.bitwise_and(scale >= scale_range[0], scale <= scale_range[1]))
        idx = np.intersect1d(idx, scale_idx)
    split_train_val_test(data_frame, idx, [7, 2, 1], csv_new_path, name)
    return idx


def split_train_val_test(df, idx, ratio, path, name):
    '''

    :param df: pandas DataFrame with csv that is to be divided to train, val and test set
    :param ratio: Ration between the sets.
    :return:
    '''
    ratio /= np.sum(ratio)
    n_idx = len(idx)
    np.random.shuffle(idx)
    train_idx = idx[np.arange(0, int(np.floor(n_idx * ratio[0])))]
    val_idx = idx[
        np.arange(int(np.floor(n_idx * ratio[0])), int(np.floor(n_idx * ratio[0])) + int(np.floor(n_idx * ratio[1])))]
    test_idx = idx[np.arange(int(np.floor(n_idx * ratio[0])) + int(np.floor(n_idx * ratio[1])), n_idx)]
    assert len(np.intersect1d(np.intersect1d(train_idx, val_idx), test_idx)) == 0
    df.loc[train_idx].to_csv('{}/{}_train.csv'.format(path, name))
    df.loc[val_idx].to_csv('{}/{}_val.csv'.format(path, name))
    df.loc[test_idx].to_csv('{}/{}_test.csv'.format(path, name))
    print(
        'Created dataset with {} train, {} validation and {} test samples (total of {} samples) matching specifications above.'
            .format(len(train_idx), len(val_idx), len(test_idx), len(idx)))


if __name__ == '__main__':
    bb_range = [[0, 0, 0, 0], [32, 32, 32, 32]]  # [[32, 32, 32, 32],[64, 64, 64, 64]]
    # colors = ['r', 'g']
    # objects = [0, 1, 2, 3]
    name = 'bb-lb'
    # scale_range = [0.3, 0.6]
    limit_and_divide_dataset('C:/Users/Antonin-PC/school/Master/diploma thesis/CoWGAN/geom_data_large.csv', './',
                             bb_range=bb_range, name=name)
