import torch
from torch import nn
import torch.nn.functional as F

INPUT_SIZE = 64
INPUT_DIM = 3
NUM_CLASSES = 4


class CNN(nn.Module):
    def __init__(self, dropout_rate=0.5, num_classes=NUM_CLASSES, bn_momentum=0.1, input_channels=3, image_side=64):
        super(CNN, self).__init__()
        self.last_conv_side = int(image_side / 8)
        # we define convolutional layers
        self.input_channels = input_channels
        self.conv1 = nn.Conv2d(in_channels=input_channels, out_channels=32, kernel_size=3, stride=1, padding=1)
        self.bn1 = nn.BatchNorm2d(32, momentum=bn_momentum)
        self.conv2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, stride=1, padding=1)
        self.bn2 = nn.BatchNorm2d(64, momentum=bn_momentum)
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, stride=1, padding=1)
        self.bn3 = nn.BatchNorm2d(128, momentum=bn_momentum)

        # 2 fully connected layers to transform the output of the convolution layers to the final output
        self.fc1 = nn.Linear(in_features=self.last_conv_side * self.last_conv_side * 128, out_features=128)
        self.fcbn1 = nn.BatchNorm1d(128, momentum=bn_momentum)
        self.fc2 = nn.Linear(in_features=128, out_features=num_classes)
        self.dropout_rate = dropout_rate

    def forward(self, s, conv_only=False, fc1_out=False):
        # we apply the convolution layers, followed by batch normalisation,
        # maxpool and relu x 3
        batch_size = s.size(0)
        s = self.bn1(self.conv1(s))  # batch_size x 32 x 64 x 64
        s = F.relu(F.max_pool2d(s, 2))  # batch_size x 32 x 32 x 32
        s = self.bn2(self.conv2(s))  # batch_size x 64 x 32 x 32
        s = F.relu(F.max_pool2d(s, 2))  # batch_size x 64 x 16 x 16
        last_conv = self.conv3(s)
        if conv_only: return last_conv.view(batch_size, -1)
        s = self.bn3(last_conv)  # batch_size x 128 x 16 x 16
        s = F.relu(F.max_pool2d(s, 2))  # batch_size x 128 x 8 x 8

        # flatten the output for each image
        s = s.view(-1, self.last_conv_side * self.last_conv_side * 128)  # batch_size x 8*8*128

        # apply 2 fully connected layers with dropout
        s = self.fc1(s)
        if fc1_out: return s
        s = F.relu(self.fcbn1(s))
        s = F.dropout(s, p=self.dropout_rate, training=self.training)
        s = self.fc2(s)  # batch_size x 6

        return F.log_softmax(s, dim=1)


class NN_inclass(nn.Module):
    def __init__(self, hidden_units=5, dropout_rate=0.5):
        super(NN_inclass, self).__init__()
        self.layer1 = nn.Sequential([
            nn.Linear(1, hidden_units),
            nn.ReLU(),
            nn.Dropout(dropout_rate)
        ])
        self.layer2 = nn.Sequential([
            nn.Linear(hidden_units, hidden_units),
            nn.ReLU(),
            nn.Dropout(dropout_rate)
        ])
        self.out = nn.Sequential(
            nn.Linear(hidden_units, 1),
            nn.Softmax()
        )

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.out(x)
        return x
