""" Conditional DCGAN for MNIST images generations.
    Author: Moustafa Alzantot (malzantot@ucla.edu)
    All rights reserved.

    LABELS:
    A label consist of multiple parts:
        - one-hot encoded type of geometric object
        - one-hot encoded color / color encoded in three channels? (RGB, 3 positive real numbers)
        - 4 values for bounding box (positive real numbers)
        - one value for scale (positive real number)
    Total length of a label:
        - n_objects + n_colors + 4 + 1 (one-hot color) = 12 (with 4 objects and 3 different colors)
        - n_objects + 3 + 4 + 1 (RGB color) = 12 (with 4 objects)

"""

import os
import argparse
import numpy as np
import torch
from torch import nn, optim

import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader

import torchvision
from torchvision.utils import save_image
from torchvision import datasets, transforms


class CondDiscriminator(nn.Module):
    def __init__(self, input_shape, labels_dim, model_dim=32, linear_units=1024, hidden_units_labels=1000,
                 kernel_size=5):
        super(CondDiscriminator, self).__init__()
        self.img_dim = input_shape[0]
        self.img_side1 = input_shape[1]
        self.img_side2 = input_shape[2]
        self.labels_dim = labels_dim
        self.model_dim = model_dim
        self.hidden_units_labels = hidden_units_labels
        self.kernel_size = kernel_size
        self.conv1 = nn.Conv2d(self.img_dim, model_dim, kernel_size=self.kernel_size, stride=2, padding=2)
        self.bn1 = nn.BatchNorm2d(model_dim)
        self.conv2 = nn.Conv2d(model_dim, 2 * model_dim, self.kernel_size, 2, 2)
        self.conv_dim = 16
        self.bn2 = nn.BatchNorm2d(2 * model_dim)
        self.fc_labels = nn.Linear(self.labels_dim, self.hidden_units_labels)
        self.fc_hidden = nn.Linear(2 * model_dim * self.conv_dim * self.conv_dim + self.hidden_units_labels,
                                   linear_units)
        self.fc_output = nn.Linear(linear_units, self.labels_dim)

    def forward(self, x, labels):
        batch_size = x.size(0)
        x = x.view(batch_size, self.img_dim, self.img_side1, self.img_side2)  # 3x64x64
        x = self.conv1(x)  # 32x32x32
        x = self.bn1(x)
        x = F.relu(x)
        x = self.conv2(x)  # 64x16x16
        x = self.bn2(x)
        x = F.relu(x)
        x = x.view(batch_size, 2 * self.model_dim * self.conv_dim * self.conv_dim)
        y_ = self.fc_labels(labels)
        y_ = F.relu(y_)
        y_ = y_.view(batch_size, self.hidden_units_labels)  # 1x1000
        x = torch.cat([x, y_], 1)  # 1x17384
        x = self.fc_hidden(x)  # 1x1024
        x = F.relu(x)
        x = self.fc_output(x)  # 1x12
        return x
        # return F.sigmoid(x)


class CondGenerator(nn.Module):
    '''
    Generator for
    '''

    def __init__(self, z_dim, labels_size, input_img_side, input_img_dim=1, model_dim=32, hidden_units_labels=1000,
                 kernel_size=5):
        self.z_dim = z_dim
        self.labels_size = labels_size
        self.input_img_side = input_img_side
        self.input_img_dim = input_img_dim
        self.model_dim = model_dim
        self.hidden_units_labels = hidden_units_labels
        self.kernel_size = kernel_size
        self.conv_dim = 16
        super(CondGenerator, self).__init__()
        self.fc_labels = nn.Linear(labels_size, self.hidden_units_labels)
        self.fc_input = nn.Linear(self.z_dim + self.hidden_units_labels,
                                  2 * self.model_dim * self.conv_dim * self.conv_dim)
        self.bn1 = nn.BatchNorm2d(2 * self.model_dim)
        self.deconv1 = nn.ConvTranspose2d(2 * self.model_dim, self.model_dim, self.kernel_size, 2, 2, output_padding=1)
        self.bn2 = nn.BatchNorm2d(self.model_dim)
        self.deconv2 = nn.ConvTranspose2d(self.model_dim, self.input_img_dim, self.kernel_size, 2, 2, output_padding=1)

    def forward(self, x, labels):
        batch_size = x.size(0)
        y_ = self.fc_labels(labels)
        y_ = F.relu(y_).view(batch_size, self.hidden_units_labels)
        x = torch.cat([x, y_], 1)
        x = self.fc_input(x)
        x = x.view(batch_size, 2 * self.model_dim, self.conv_dim, self.conv_dim)
        x = self.bn1(x)
        x = F.relu(x)
        x = self.deconv1(x)
        x = self.bn2(x)
        x = F.relu(x)
        x = self.deconv2(x)
        x = F.tanh(x)  # Which output activation should I choose?
        return x


class ConditionalDiscriminatorFC(nn.Module):
    def __init__(self, dim_input, dim_output, n_labels):
        super(ConditionalDiscriminatorFC, self).__init__()
        self.n_labels = n_labels
        self.dim_output = dim_output
        self.layer1 = nn.Sequential(
            nn.Linear(dim_input, 64),
            nn.LeakyReLU()
        )
        self.layer2 = nn.Sequential(
            nn.Linear(64, 32),
            nn.LeakyReLU()
        )
        self.layer3 = nn.Sequential(
            nn.Linear(32, 16),
            nn.LeakyReLU()
        )
        self.fc_labels = nn.Sequential(
            nn.Linear(n_labels, 16),
            nn.LeakyReLU()
        )
        self.disc_output = nn.Linear(32, dim_output)
        self.class_output = nn.Sequential(
            nn.LeakyReLU(),
            nn.Linear(dim_output,self.n_labels),
            nn.Softmax()
        )

    def forward(self, x, y):
        batch_size = x.size(0)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        y = self.fc_labels(y)
        x = torch.cat([x, y], 1)
        disc_out = self.disc_output(x)
        class_out = self.class_output(disc_out)
        return disc_out,class_out


class ConditionalGeneratorFC(nn.Module):
    def __init__(self, dim_noise, dim_output, dim_labels):
        super(ConditionalGeneratorFC, self).__init__()
        self.fc_data = nn.Sequential(
            nn.Linear(dim_noise, 32),
            nn.LeakyReLU()
        )
        self.fc_labels = nn.Sequential(
            nn.Linear(dim_labels, 32),
            nn.LeakyReLU()
        )
        self.layer1 = nn.Sequential(
            nn.Linear(64, 32),
            nn.LeakyReLU()
        )
        self.layer2 = nn.Sequential(
            nn.Linear(32, 16),
            nn.LeakyReLU()
        )
        self.layer3 = nn.Sequential(
            nn.Linear(16, 8),
            nn.LeakyReLU()
        )
        self.output_layer = nn.Linear(8, dim_output)

    def forward(self, x, y):
        x = self.fc_data(x)
        y = self.fc_labels(y)
        x = torch.cat([x, y], 1)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.output_layer(x)
        return x


def train_G(D, G, n_iters):
    for p in D.parameters():
        p.requires_grad_(False)  # freeze D

    gen_cost = None
    for i in range(n_iters):
        # print("Generator iters: " + str(i))
        G.zero_grad()
        f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
        noise = gen_rand_noise_with_label(f_label)
        noise.requires_grad_(True)
        fake_data = G(noise)
        gen_cost, gen_aux_output = D(fake_data)

        aux_label = torch.from_numpy(f_label).long()
        aux_label = aux_label.to(device)
        aux_errG = aux_criterion(gen_aux_output, aux_label).mean()
        gen_cost = -gen_cost.mean()
        g_cost = ACGAN_SCALE_G * aux_errG + gen_cost
        g_cost.backward()


if __name__ == '__main__':
    input_shape = (3, 64, 64)
    input_size = input_shape[0] * input_shape[1] * input_shape[2]
    D = CondDiscriminator(input_shape=input_shape, labels_dim=12)
    G = CondGenerator(z_dim=100, labels_size=12, input_img_side=input_shape[1], input_img_dim=input_shape[0])
    random_input = torch.FloatTensor(input_size).normal_(0, 1)
    random_input = random_input.view(1, input_shape[0], input_shape[1], input_shape[2])
    labels = [1, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 1]
    labels = torch.Tensor(labels)
    out_D = D(random_input, labels)
    random_noise = torch.FloatTensor(100).normal_(0, 1).view(1, 100)
    out_G = G(random_noise, labels)

    parser = argparse.ArgumentParser('Conditional DCGAN')
    parser.add_argument('--batch_size', type=int, default=128,
                        help='Batch size (default=128)')
    parser.add_argument('--lr', type=float, default=0.01,
                        help='Learning rate (default=0.01)')
    parser.add_argument('--epochs', type=int, default=10,
                        help='Number of training epochs.')
    parser.add_argument('--nz', type=int, default=100,
                        help='Number of dimensions for input noise.')
    parser.add_argument('--cuda', action='store_true',
                        help='Enable cuda')
    parser.add_argument('--save_every', type=int, default=1,
                        help='After how many epochs to save the model.')
    parser.add_argument('--print_every', type=int, default=50,
                        help='After how many epochs to print loss and save output samples.')
    parser.add_argument('--save_dir', type=str, default='models',
                        help='Path to save the trained models.')
    parser.add_argument('--samples_dir', type=str, default='samples',
                        help='Path to save the output samples.')
    args = parser.parse_args()

    if not os.path.exists(args.save_dir):
        os.mkdir(args.save_dir)

    if not os.path.exists(args.samples_dir):
        os.mkdir(args.samples_dir)

    INPUT_SIZE = 784
    SAMPLE_SIZE = 80
    NUM_LABELS = 10
    train_dataset = datasets.MNIST(root='data',
                                   train=True,
                                   download=True,
                                   transform=transforms.ToTensor())
    train_loader = DataLoader(train_dataset, shuffle=True,
                              batch_size=args.batch_size)

    model_d = CondDiscriminator()
    model_g = CondGenerator(args.nz)
    criterion = nn.BCELoss()
    input = torch.FloatTensor(args.batch_size, INPUT_SIZE)
    noise = torch.FloatTensor(args.batch_size, (args.nz))

    random_input = torch.FloatTensor(SAMPLE_SIZE, args.nz).normal_(0, 1)
    fixed_labels = torch.zeros(SAMPLE_SIZE, NUM_LABELS)
    for i in range(NUM_LABELS):
        for j in range(SAMPLE_SIZE // NUM_LABELS):
            fixed_labels[i * (SAMPLE_SIZE // NUM_LABELS) + j, i] = 1.0

    label = torch.FloatTensor(args.batch_size)
    one_hot_labels = torch.FloatTensor(args.batch_size, 10)
    if args.cuda:
        model_d.cuda()
        model_g.cuda()
        input, label = input.cuda(), label.cuda()
        noise, random_input = noise.cuda(), random_input.cuda()
        one_hot_labels = one_hot_labels.cuda()
        fixed_labels = fixed_labels.cuda()

    optim_d = optim.SGD(model_d.parameters(), lr=args.lr)
    optim_g = optim.SGD(model_g.parameters(), lr=args.lr)
    random_input = Variable(random_input)
    fixed_labels = Variable(fixed_labels)

    real_label = 1
    fake_label = 0

    for epoch_idx in range(args.epochs):
        model_d.train()
        model_g.train()

        d_loss = 0.0
        g_loss = 0.0
        for batch_idx, (train_x, train_y) in enumerate(train_loader):
            batch_size = train_x.size(0)
            train_x = train_x.view(-1, INPUT_SIZE)
            if args.cuda:
                train_x = train_x.cuda()
                train_y = train_y.cuda()

            input.resize_as_(train_x).copy_(train_x)
            label.resize_(batch_size).fill_(real_label)
            one_hot_labels.resize_(batch_size, NUM_LABELS).zero_()
            one_hot_labels.scatter_(1, train_y.view(batch_size, 1), 1)
            inputv = Variable(input)
            labelv = Variable(label)

            output = model_d(inputv, Variable(one_hot_labels))
            optim_d.zero_grad()
            errD_real = criterion(output, labelv)
            errD_real.backward()
            realD_mean = output.data.cpu().mean()

            one_hot_labels.zero_()
            rand_y = torch.from_numpy(
                np.random.randint(0, NUM_LABELS, size=(batch_size, 1))).cuda()
            one_hot_labels.scatter_(1, rand_y.view(batch_size, 1), 1)
            noise.resize_(batch_size, args.nz).normal_(0, 1)
            label.resize_(batch_size).fill_(fake_label)
            noisev = Variable(noise)
            labelv = Variable(label)
            onehotv = Variable(one_hot_labels)
            g_out = model_g(noisev, onehotv)
            output = model_d(g_out, onehotv)
            errD_fake = criterion(output, labelv)
            fakeD_mean = output.data.cpu().mean()
            errD = errD_real + errD_fake
            errD_fake.backward()
            optim_d.step()

            # train the G
            noise.normal_(0, 1)
            one_hot_labels.zero_()
            rand_y = torch.from_numpy(
                np.random.randint(0, NUM_LABELS, size=(batch_size, 1))).cuda()
            one_hot_labels.scatter_(1, rand_y.view(batch_size, 1), 1)
            label.resize_(batch_size).fill_(real_label)
            onehotv = Variable(one_hot_labels)
            noisev = Variable(noise)
            labelv = Variable(label)
            g_out = model_g(noisev, onehotv)
            output = model_d(g_out, onehotv)
            errG = criterion(output, labelv)
            optim_g.zero_grad()
            errG.backward()
            optim_g.step()

            d_loss += errD.data[0]
            g_loss += errG.data[0]
            if batch_idx % args.print_every == 0:
                print(
                    "\t{} ({} / {}) mean D(fake) = {:.4f}, mean D(real) = {:.4f}".
                        format(epoch_idx, batch_idx, len(train_loader), fakeD_mean,
                               realD_mean))

                g_out = model_g(random_input, fixed_labels).data.view(
                    SAMPLE_SIZE, 1, 28, 28).cpu()
                save_image(g_out,
                           '{}/{}_{}.png'.format(
                               args.samples_dir, epoch_idx, batch_idx))

        print('Epoch {} - D loss = {:.4f}, G loss = {:.4f}'.format(epoch_idx,
                                                                   d_loss, g_loss))
        if epoch_idx % args.save_every == 0:
            torch.save({'state_dict': model_d.state_dict()},
                       '{}/model_d_epoch_{}.pth'.format(
                           args.save_dir, epoch_idx))
            torch.save({'state_dict': model_g.state_dict()},
                       '{}/model_g_epoch_{}.pth'.format(
                           args.save_dir, epoch_idx))
