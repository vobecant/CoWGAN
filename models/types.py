from enum import Enum


class GAN_types(Enum):
    WGAN_GP = 1
    WGAN_GP_conv = 2
    WGAN_GP_conv_FCG = 3
    WGAN_GP_fc_FCG = 4
    WGAN_GP_2D = 5
    ConWGAN_GP = 6
    ConWGAN_GP_conv = 7


class WEIGHT_CRITERION_types(Enum):
    WGAN = 1
    ConWGAN = 2


class DATASET_types(Enum):
    GEOM_3D = 'geom3D'
    GEOM_3D_LARGE = '/home/skovirad/dataset3D_large'
    GEOM_3D_TOP = '/home/skovirad/dataset3d/3d_top'
    GEOM_3D_BOTTOM = '/home/skovirad/dataset3d/3d_bottom'
    GEOM_3D_UNLIMITED = '/home/skovirad/dataset3d/3d_unlimited'
    GEOM_BASIC = './datasets/splitted'
    GEOM_2D_CIRCLES = './datasets/black_circles_same-size'
