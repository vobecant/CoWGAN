import torch
import matplotlib

matplotlib.use('agg')
from matplotlib import pyplot as plt
from matplotlib.pyplot import cm
import numpy as np
import torchvision
from torch import autograd
from constants import *
from timeit import default_timer as timer
from libs.mean_counter import MeanVarCounter
from models.wgan import MyConvo2d
import torch.nn.init as init
from libs.save import Saver
from libs.plot import Plotter
from torch import nn

USE_VAR_PENALTY = False


def gen_rand_noise(size=BATCH_SIZE):
    noise = torch.randn(size, 128)
    noise = noise.to(device)
    return noise


def gen_rand_noise_with_label(label=None, size=BATCH_SIZE):
    if label is None:
        label = np.random.randint(0, NUM_CLASSES, size)
    # attach label into noise
    noise = np.random.normal(0, 1, (size, 128))
    prefix = np.zeros((size, NUM_CLASSES))
    prefix[np.arange(size), label] = 1
    noise[np.arange(size), :NUM_CLASSES] = prefix[np.arange(size)]

    noise = torch.from_numpy(noise).float()
    noise = noise.to(device)

    return noise


def gen_rand_noise_and_label(label=None, size=BATCH_SIZE):
    if label is None:
        label = np.random.randint(0, NUM_CLASSES, size)
    # attach label into noise
    noise = np.random.normal(0, 1, (size, 128))
    noise = torch.from_numpy(noise).float()
    noise = noise.to(device)
    if not isinstance(label, torch.Tensor): label = torch.from_numpy(label).float()

    return noise, label


def calc_gradient_penalty(netD, real_data, fake_data):
    alpha = torch.rand(BATCH_SIZE, 1)
    alpha = alpha.expand(BATCH_SIZE, int(real_data.nelement() / BATCH_SIZE)).contiguous()
    if MODE == GAN_types.WGAN_GP_2D:
        alpha = alpha.view(BATCH_SIZE, -1)
        fake_data = fake_data.view(BATCH_SIZE, -1)
    elif MODE == GAN_types.WGAN_GP_conv:
        alpha = alpha.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
        fake_data = fake_data.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
    elif MODE == GAN_types.WGAN_GP:
        alpha = alpha.view(BATCH_SIZE, 3, DIM, DIM)
        fake_data = fake_data.view(BATCH_SIZE, 3, DIM, DIM)
    else:
        raise Exception('Invalid MODE!')
    alpha = alpha.to(device)

    interpolates = alpha * real_data.detach() + ((1 - alpha) * fake_data.detach())

    interpolates = interpolates.to(device)
    interpolates.requires_grad_(True)

    disc_interpolates = netD(interpolates)

    gradients = autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                              grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]

    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * LAMBDA
    return gradient_penalty


def calc_gradient_penalty_conwgan(netD, real_data, fake_data):
    alpha = torch.rand(BATCH_SIZE, 1)
    alpha = alpha.expand(BATCH_SIZE, int(real_data.nelement() / BATCH_SIZE)).contiguous()
    if MODE == GAN_types.ConWGAN_GP_conv:
        alpha = alpha.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
        fake_data = fake_data.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
    elif MODE == GAN_types.ConWGAN_GP:
        alpha = alpha.view(BATCH_SIZE, 3, DIM, DIM)
        fake_data = fake_data.view(BATCH_SIZE, 3, DIM, DIM)
    elif MODE == GAN_types.WGAN_GP_2D:
        alpha = alpha.view(BATCH_SIZE, 2)
        fake_data = fake_data.view(BATCH_SIZE, 2)
    else:
        raise Exception('Invalid MODE! ({})'.format(MODE))
    alpha = alpha.to(device)

    # fake_data = fake_data.view(BATCH_SIZE, 3, DIM, DIM)
    interpolates = alpha * real_data.detach() + ((1 - alpha) * fake_data.detach())

    interpolates = interpolates.to(device)
    interpolates.requires_grad_(True)

    disc_interpolates, _ = netD(interpolates)

    gradients = autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                              grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]

    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * LAMBDA
    return gradient_penalty


def calc_gradient_penalty_conwganFC(netD, real_data, real_labels, fake_data, targets):
    alpha = torch.rand(BATCH_SIZE, 1)
    alpha = alpha.expand(BATCH_SIZE, int(real_data.nelement() / BATCH_SIZE)).contiguous()
    alpha = alpha.view(BATCH_SIZE, 2)
    fake_data = fake_data.view(BATCH_SIZE, 2)
    alpha = alpha.to(device)

    labels = np.unique(real_labels)
    gradient_penalty = torch.Tensor([0])

    for label in labels:
        idx = np.where(np.equal(real_labels, label))[0]
        interpolates = alpha[idx] * real_data[idx].detach() + ((1 - alpha[idx]) * fake_data[idx].detach())

        interpolates = interpolates.to(device)
        interpolates.requires_grad_(True)

        disc_interpolates, _ = netD(interpolates, targets[idx])

        gradients = autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                                  grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                                  create_graph=True, retain_graph=True, only_inputs=True)[0]

        gradients = gradients.view(gradients.size(0), -1)
        gradient_penalty += ((gradients.norm(2, dim=1) - 1) ** 2).mean() * LAMBDA
    return gradient_penalty


def init_counter(D, loader, cnn_model):
    # print('Initializing counter.')
    st = timer()
    results, labels = [], []
    with torch.no_grad():
        for batch in loader:
            data = batch[0].to(device)
            labels.extend(batch[1].cpu().data.numpy())
            if MODE == GAN_types.WGAN_GP_conv: data = cnn_model(data, conv_only=True)
            if MODE == GAN_types.ConWGAN_GP:
                Dout, aux_out = D(data).cpu().data.numpy()
                if WEIGHT_CRITERION == WEIGHT_CRITERION_types.WGAN:
                    results.extend(Dout)
                elif WEIGHT_CRITERION == WEIGHT_CRITERION_types.ConWGAN:
                    results.extend(aux_out)
                else:
                    raise Exception('Unsupported WEIGHT_CRITERION for conwgan-gp!')
            else:
                Dout = D(data).cpu().data.numpy()
                if WEIGHT_CRITERION == WEIGHT_CRITERION_types.ConWGAN:
                    results.extend(Dout)
                else:
                    raise Exception('Unsupported WEIGHT_CRITERION for {}!'.format(MODE))
    counter = MeanVarCounter(num_classes=NUM_CLASSES, samples=results, labels=labels)
    print('Counter initialized in {}.'.format(timer() - st))
    return counter


def weights_init(m):
    if isinstance(m, MyConvo2d):
        if m.conv.weight is not None:
            if m.he_init:
                init.kaiming_uniform_(m.conv.weight)
            else:
                init.xavier_uniform_(m.conv.weight)
        if m.conv.bias is not None:
            init.constant_(m.conv.bias, 0.0)
    if isinstance(m, nn.Linear):
        if m.weight is not None:
            init.xavier_uniform_(m.weight)
        if m.bias is not None:
            init.constant_(m.bias, 0.0)


def generate_image(netG, noise=None, conv=False):
    if noise is None:
        noise = gen_rand_noise()
    with torch.no_grad():
        noisev = noise
    samples = netG(noisev)
    if MODE == GAN_types.WGAN_GP_2D:
        samples = samples.view(BATCH_SIZE, -1)
    elif not conv:
        samples = samples.view(BATCH_SIZE, 3, 64, 64)
    else:
        samples = samples.view(BATCH_SIZE, DIM_CONV, IM_SIZE_CONV, IM_SIZE_CONV)
        # print('samples: {}, {}'.format(type(samples),samples.size()))
        samples = samples[:, 0, :, :].view(BATCH_SIZE, 1, IM_SIZE_CONV, IM_SIZE_CONV)
        # print('samples: {}, {}'.format(type(samples), samples.size()))
    if MODE != GAN_types.WGAN_GP_2D: samples = samples * 0.5 + 0.5
    return samples


def generate_image_congan(netG, noise=None, rand_label=None):
    if noise is None:
        rand_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
        noise = gen_rand_noise_with_label(rand_label)
    with torch.no_grad():
        noisev = noise
    samples = netG(noisev)
    if MODE is not GAN_types.WGAN_GP_2D:
        samples = samples.view(BATCH_SIZE, 3, DIM, DIM)
        samples = samples * 0.5 + 0.5
    if rand_label.size(1) != 1:
        rand_label = torch.argmax(rand_label, dim=1).long()

    return samples, rand_label


def generate_image_conganFC(netG, noise, labels):
    with torch.no_grad():
        noisev = noise
        labelsv = labels
    samples = netG(noisev, labelsv)
    return samples


# ======= TRAINING FUNCTIONS ========

def train_cnn_only(cnn_model, optimizer_cnn, criterion_cnn, train_loader_train, output_path):
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver = Saver('val_loss', better_value_fnc, output_path)
    plotter_cnn = Plotter(output_path=output_path)
    print('start training CNN only')
    optimizer = optimizer_cnn  # optim.Adam(cnn.parameters(), lr=LEARNING_RATE)
    for epoch in range(N_EPOCHS_CNN):
        start_time = timer()
        # print('epoch {}\ttrain start'.format(epoch))
        ep_start = timer()
        # ------ training -------
        train_losses = np.asarray([])
        total_train, correct_train = 0, 0
        cnn_model.train()
        for i, batch in enumerate(train_loader_train):
            # if i == 5: break
            # Forward pass
            images = batch[0]
            labels = batch[1].long()
            # print(labels)
            images = images.to(device)
            labels = labels.to(device)
            # print(labels)
            outputs = cnn_model(images)
            _, predicted = torch.max(outputs.data, 1)

            # forward pass
            loss = criterion_cnn(outputs, labels)
            train_losses = np.append(train_losses, loss.item())

            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            total_train += labels.size(0)
            correct_train += (predicted == labels).sum().item()

        train_acc = 100 * correct_train / total_train
        train_loss = np.mean(train_losses)

        # ------ validation ------
        # print('epoch {}\tvalidation start'.format(epoch))
        val_loss, val_acc = validate_cnn_only(train_loader_train, cnn_model, criterion_cnn)

        # ------ create plots ------
        plotter_cnn.plot('time', timer() - start_time)
        plotter_cnn.plot('train_loss', train_loss)
        plotter_cnn.plot('val_loss', val_loss)
        plotter_cnn.plot('train_acc', train_acc)
        plotter_cnn.plot('val_acc', val_acc)
        if epoch % PLOT_FREQ_CNN == (PLOT_FREQ_CNN - 1):
            plotter_cnn.flush()
        plotter_cnn.tick()

        # ------ save -------
        if MAKE_SAVES_CNN:
            saver.update('cnn', cnn_model)
            saver.update('cnn_w', cnn_model.state_dict())
            saver.update('cnn_opt', optimizer.state_dict())
            saver.update('train_loss', train_loss)
            saver.update('val_loss', val_loss)
            saver.update('train_acc', train_acc)
            saver.update('val_acc', val_acc)
            saver.update('epoch', epoch)
            if epoch % SAVE_FREQ_CNN == (SAVE_FREQ_CNN - 1):
                saver.flush()
            saver.tick()

        # ----- print info ------
        print('Epoch [{}/{}], Train Loss: {:.6f}, Train Acc: {:.2f}, Val Loss: {:.6f}, Val Acc: {:.2f}, time: {:.3f}'
              .format(epoch + 1, N_EPOCHS_CNN, train_loss, train_acc, val_loss, val_acc, timer() - ep_start))


def train_cnn_only_further(cnn_optimizer, train_loader_all, criterion_cnn, output_path):
    cnn = torch.load('./cnn_basic.pt')
    optimizer = cnn_optimizer.Adam(cnn.parameters())
    optimizer.load_state_dict(torch.load('./cnn_opt.pt'))
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver = Saver('val_loss', better_value_fnc, output_path)
    plotter = Plotter(output_path=output_path)
    print('start training CNN only')
    for epoch in range(N_EPOCHS_BOTH):
        start_time = timer()
        # print('epoch {}\ttrain start'.format(epoch))
        ep_start = timer()
        # ------ training -------
        train_losses = np.asarray([])
        total_train, correct_train = 0, 0
        cnn.train()
        n_samples = 0
        for i, batch in enumerate(train_loader_all):
            # if i == 5: break
            # Forward pass
            images = batch[0]
            labels = batch[1].long()
            # print(labels)
            images = images.to(device)
            labels = labels.to(device)
            # print(labels)
            outputs = cnn(images)
            _, predicted = torch.max(outputs.data, 1)

            # forward pass
            loss = criterion_cnn(outputs, labels)
            train_losses = np.append(train_losses, loss.item())

            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            total_train += labels.size(0)
            correct_train += (predicted == labels).sum().item()
            n_samples += len(labels)
            if n_samples > NUM_SAMPLES_WEIGHT: break

        train_acc = 100 * correct_train / total_train
        train_loss = np.mean(train_losses)

        # ------ validation ------
        # print('epoch {}\tvalidation start'.format(epoch))
        val_loss, val_acc = validate_cnn_only(train_loader_all, cnn, criterion_cnn)

        # ------ create plots ------
        plotter.plot('time', timer() - start_time)
        plotter.plot('train_loss', train_loss)
        plotter.plot('val_loss', val_loss)
        plotter.plot('train_acc', train_acc)
        plotter.plot('val_acc', val_acc)
        if epoch % PLOT_FREQ_CNN == (PLOT_FREQ_CNN - 1):
            plotter.flush()
        plotter.tick()

        # ------ save -------
        saver.update('cnn', cnn)
        saver.update('cnn_w', cnn.state_dict())
        saver.update('cnn_opt', optimizer.state_dict())
        saver.update('train_loss', train_loss)
        saver.update('val_loss', val_loss)
        saver.update('train_acc', train_acc)
        saver.update('val_acc', val_acc)
        saver.update('epoch', epoch)
        if epoch % SAVE_FREQ_CNN == (SAVE_FREQ_CNN - 1):
            saver.flush()
        saver.tick()

        # ----- print info ------
        print('Epoch [{}/{}], Train Loss: {:.6f}, Train Acc: {:.2f}, Val Loss: {:.6f}, Val Acc: {:.2f}, time: {:.3f}'
              .format(epoch + 1, N_EPOCHS_BOTH, train_loss, train_acc, val_loss, val_acc, timer() - ep_start))


def validate_cnn_only(val_loader, model, criterion, show=False):
    # switch to evaluate mode
    model.eval()
    losses = np.asarray([])
    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(val_loader):
            input = batch[0]
            target = batch[1].long()
            input = input.to(device)
            target = target.to(device)
            # compute output
            output = model(input)
            _, predicted = torch.max(output.data, 1)
            loss = criterion(output, target)
            losses = np.append(losses, loss.cpu().data.numpy())
            total += target.size(0)
            correct += (predicted == target).sum().item()
    val_loss = np.mean(losses)
    val_acc = 100 * correct / total
    return val_loss, val_acc


# Reference: https://github.com/caogang/wgan-gp/blob/master/gan_cifar10.py
def train_gan(D, G, cnn_model, optimizer_d, optimizer_g, dataloader, val_loader, mone, fixed_noise, writer, log_file,
              train_loader_train, train_loader_test, output_path, use_var_penalty):
    best_wdist = np.inf
    better_value_fnc = lambda best_wd, new_wd: new_wd < best_wd
    saver = Saver('wdist', better_value_fnc, output_path, START_ITER)
    plotter = Plotter(output_path=output_path, start_iter=START_ITER)
    dataiter = iter(dataloader)
    print('start training GAN only, mode: {}, VAR penalty:{}, iterations: {}, train dir: {}, val dir: {}'
          .format(MODE, use_var_penalty, NUM_ITER_GAN, TRAIN_DIR_TRAIN, VAL_DIR_TRAIN))
    # freeze CNN
    if cnn_model is not None:
        for p in cnn_model.parameters():
            p.requires_grad_(False)  # freeze CNN

    for iteration in range(START_ITER, NUM_ITER_GAN):
        start_time = timer()
        # print("Iter: " + str(iteration))
        start = timer()

        # ---------------------TRAIN G------------------------
        for p in D.parameters():
            p.requires_grad_(False)  # freeze D

        gen_cost = None
        for i in range(GENER_ITERS):
            # print("Generator iters: " + str(i))
            G.zero_grad()
            noise = gen_rand_noise()
            noise.requires_grad_(True)
            # print('noise size: {}'.format(noise.size()))
            fake_data = G(noise)
            gen_cost = D(fake_data)
            gen_cost = gen_cost.mean()
            gen_cost.backward(mone)
            gen_cost = -gen_cost

        optimizer_g.step()
        end = timer()
        # print(out_fname'---train G elapsed time: {end - start}')

        # ---------------------TRAIN D------------------------
        for p in D.parameters():  # reset requires_grad
            p.requires_grad_(True)  # they are set to False below in training G
        for i in range(CRITIC_ITERS):
            # print("Critic iter: " + str(i))

            start = timer()
            D.zero_grad()

            # gen fake dataset_iter and load real dataset_iter
            noise = gen_rand_noise()
            with torch.no_grad():
                noisev = noise  # totally freeze G, training D
            fake_data = G(noisev).detach()
            end = timer();
            # print(out_fname'---gen G elapsed time: {end-start}')
            start = timer()
            batch = next(dataiter, None)
            if batch is None:
                dataiter = iter(dataloader)
                batch = dataiter.next()
                # print(batch[0].cpu().data.numpy())
            batch = batch[0]  # batch[1] contains labels
            real_data = batch.to(device).float()  # TODO: modify load_data for each loading
            if MODE == GAN_types.WGAN_GP_conv: real_data = cnn_model(real_data, conv_only=True)
            end = timer();
            # print(out_fname'---load real imgs elapsed time: {end-start}')
            start = timer()

            # train with real dataset_iter
            disc_real_out = D(real_data)
            disc_real = disc_real_out.mean()
            disc_real_var = disc_real_out.var()  # include into cost function?

            # train with fake dataset_iter
            disc_fake_out = D(fake_data)
            disc_fake = disc_fake_out.mean()
            disc_fake_var = disc_fake_out.var()  # include into cost function?

            # showMemoryUsage(0)
            # train with interpolates dataset_iter
            gradient_penalty = calc_gradient_penalty(D, real_data, fake_data)
            # showMemoryUsage(0)

            # final disc cost

            disc_cost = disc_fake - disc_real  # we want this to be as close as possible -> result should be 0
            disc_cost_var = disc_real_var + disc_fake_var  # we want discriminator to have as small variance as possible
            disc_total_cost = disc_cost + gradient_penalty
            if use_var_penalty: disc_total_cost += disc_cost_var
            disc_total_cost.backward()
            w_dist = disc_fake - disc_real
            optimizer_d.step()
            # ------------------VISUALIZATION----------
            end = timer();
            # print(out_fname'---train D elapsed time: {end-start}')

        # ---------------VISUALIZATION---------------------
        plotter.plot('time', timer() - start_time)
        plotter.plot('train_disc_cost', disc_cost.cpu().data.numpy())
        plotter.plot('train_gen_cost', gen_cost.cpu().data.numpy())
        plotter.plot('wasserstein_distance', w_dist.cpu().data.numpy())
        if use_var_penalty: plotter.plot('cost_var', disc_cost_var.cpu().data.numpy())
        plotter.plot('total_cost', disc_total_cost.cpu().data.numpy())
        plotter.plot('GP', gradient_penalty.cpu().data.numpy())
        w_dist = np.abs(w_dist.item())

        # ---------------SAVING--------------------
        saver.update('D', D)
        saver.update('D_w', D.state_dict())
        saver.update('G', G)
        saver.update('G_w', G.state_dict())
        saver.update('wdist', w_dist)
        saver.update('D_cost', disc_cost.item())
        saver.update('G_cost', disc_cost.item())
        saver.update('D_opt', optimizer_d.state_dict())
        saver.update('G_opt', optimizer_g.state_dict())

        if w_dist < best_wdist:
            best_wdist = w_dist
            torch.save(G, output_path + "generator_best_abs.pt")
            torch.save(D, output_path + "discriminator_best_abs.pt")

        if iteration % VAL_FREQ_GAN == (VAL_FREQ_GAN - 1):
            dev_disc_costs = []
            for _, images in enumerate(val_loader):
                imgs = torch.Tensor(images[0].float())
                imgs = imgs.to(device)
                if MODE == GAN_types.WGAN_GP_conv: imgs = cnn_model(imgs, conv_only=True)
                with torch.no_grad():
                    imgs_v = imgs
                D_res = D(imgs_v)
                _dev_disc_cost = -D_res.mean().cpu().data.numpy()
                dev_disc_costs.append(_dev_disc_cost)
            plotter.plot('dev_disc_cost', np.mean(dev_disc_costs))
            saver.update('D_cost_val', np.mean(dev_disc_costs))
            gen_images = generate_image(G, fixed_noise)
            if len(gen_images.size()) == 2:
                samples = gen_images.cpu().data.numpy()
                plt.clf()
                plt.scatter(samples[:, 0], samples[:, 1])
                plt.title('Generated distribution, FIXED, iteration {}'.format(iteration))
                plt.savefig(output_path + 'distribution_{}_fixed.png'.format(iteration))
                gen_images = generate_image(G, gen_rand_noise(200))
                samples = gen_images.cpu().data.numpy()
                plt.clf()
                plt.scatter(samples[:, 0], samples[:, 1])
                plt.title('Generated distribution, RANDOM, iteration {}'.format(iteration))
                plt.savefig(output_path + 'distribution_{}_random.png'.format(iteration))
                plt.clf()
                fake = fake_data.cpu().data.numpy()
                plt.scatter(fake[:, 0], fake[:, 1], color='red', label='fake')
                plt.hold(True)
                real = real_data.cpu().data.numpy()
                plt.scatter(real[:, 0], real[:, 1], color='blue', label='real')
                plt.legend()
                plt.title('Fake vs real data')
                plt.savefig(output_path + 'fake_vs_real_{}'.format(iteration))
                plt.clf()
                disc_fake_np = disc_fake_out.cpu().data.numpy()
                plt.scatter(disc_fake_np, np.ones(disc_fake_np.shape), color='red', label='fake')
                plt.hold(True)
                disc_real_np = disc_real_out.cpu().data.numpy()
                plt.scatter(disc_real_np, np.zeros(disc_real_np.shape), color='blue', label='real')
                plt.legend()
                plt.title('Discriminator distriburtion, iteration {}'.format(iteration))
                plt.savefig(output_path + 'D-distribution_{}'.format(iteration))
            else:
                torchvision.utils.save_image(gen_images, output_path + 'samples_{}.png'.format(iteration), nrow=8,
                                             padding=2)
            grid_images = torchvision.utils.make_grid(gen_images, nrow=8, padding=2)
            writer.add_image('images', grid_images, iteration)

            # ----------------------flush and save----------------------
            plotter.flush(print_info=True)
            saver.flush()
        '''
        if iteration % PLOT_DISTR_FREQ == (PLOT_DISTR_FREQ - 1):
            # plot Gaussian distribution of discriminator's output over train dataset (BOTTOM) and test dataset (TOP)
            plotter.compute_gan_statistics(D, train_loader_test, train_loader_train, device, output_path, iteration,
                                           file=log_file)
        '''
        plotter.tick()
        saver.tick()


def train_congan(D, G, cnn_model, optimizer_d, optimizer_g, dataloader, val_loader, mone, fixed_noise_congan,
                 fixed_label, log_file, train_loader_train, train_loader_test, aux_criterion, output_path):
    best_wdist = np.inf
    better_value_fnc = lambda best_wd, new_wd: new_wd < best_wd
    saver = Saver('wdist', better_value_fnc, output_path, START_ITER)
    plotter = Plotter(output_path=output_path, start_iter=START_ITER)
    dataiter = iter(dataloader)
    print('start training ConGAN only  , mode: {}, iterations: {}'.format(MODE, NUM_ITER_GAN))
    for iteration in range(START_ITER, END_ITER_GAN):
        start_time = timer()
        # print("Iter: " + str(iteration))
        start = timer()
        # ---------------------TRAIN G------------------------
        for p in D.parameters():
            p.requires_grad_(False)  # freeze D

        gen_cost = None
        for i in range(GENER_ITERS):
            # print("Generator iters: " + str(i))
            G.zero_grad()
            f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
            noise = gen_rand_noise_with_label(f_label)
            noise.requires_grad_(True)
            fake_data = G(noise)
            gen_cost, gen_aux_output = D(fake_data)

            aux_label = torch.from_numpy(f_label).long()
            aux_label = aux_label.to(device)
            aux_errG = aux_criterion(gen_aux_output, aux_label).mean()
            gen_cost = -gen_cost.mean()
            g_cost = ACGAN_SCALE_G * aux_errG + gen_cost
            g_cost.backward()

        optimizer_g.step()
        end = timer()
        # print(out_fname'---train G elapsed time: {end - start}')
        # ---------------------TRAIN D------------------------
        for p in D.parameters():  # reset requires_grad
            p.requires_grad_(True)  # they are set to False below in training G
        for i in range(CRITIC_ITERS):
            # print("Critic iter: " + str(i))

            start = timer()
            D.zero_grad()

            # gen fake dataset_iter and load real dataset_iter
            f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
            noise, fake_label = gen_rand_noise_and_label(f_label)
            fake_label = fake_label.long().to(device)
            # fake_label = torch.argmax(fake_label, model_dim=1).long()
            with torch.no_grad():
                noisev = noise  # totally freeze G, training D
            fake_data = G(noisev).detach()
            end = timer()
            # print(out_fname'---gen G elapsed time: {end-start}')
            start = timer()
            batch = next(dataiter, None)
            if batch is None:
                dataiter = iter(dataloader)
                batch = next(dataiter)
            real_data = batch[0].float()  # batch[1] contains labels
            real_data.requires_grad_(True)
            real_label = batch[1].long()
            real_label = torch.argmax(real_label, dim=1).long().to(device)
            end = timer()
            # print(out_fname'---load real imgs elapsed time: {end-start}')

            start = timer()
            real_data = real_data.to(device)
            if MODE == GAN_types.ConWGAN_GP_conv:
                real_data = cnn_model(real_data, conv_only=True)
            real_label = real_label.to(device)

            # train with real dataset_iter
            disc_real, aux_output = D(real_data)
            aux_errD_real = aux_criterion(aux_output, real_label)
            errD_real = aux_errD_real.mean()
            disc_real = disc_real.mean()

            # train with fake dataset_iter
            disc_fake, aux_output = D(fake_data)
            aux_errD_fake = aux_criterion(aux_output, fake_label)
            errD_fake = aux_errD_fake.mean()
            disc_fake = disc_fake.mean()

            # showMemoryUsage(0)
            # train with interpolates dataset_iter
            gradient_penalty = calc_gradient_penalty_conwgan(D, real_data, fake_data)
            # showMemoryUsage(0)

            # final disc cost
            w_dist = (disc_fake - disc_real).abs()
            disc_acgan = errD_real + errD_fake
            total_cost = w_dist + ACGAN_SCALE * disc_acgan + gradient_penalty
            (total_cost).backward()
            optimizer_d.step()

            end = timer();
            # print(out_fname'---train D elapsed time: {end-start}')
            # ---------------VISUALIZATION---------------------
            plotter.plot('time', timer() - start_time)
            plotter.plot('train_disc_cost', w_dist.cpu().data.numpy())
            plotter.plot('train_gen_cost', gen_cost.cpu().data.numpy())
            plotter.plot('wasserstein_distance', w_dist.cpu().data.numpy())
            plotter.plot('total_cost', total_cost.cpu().data.numpy())
            plotter.plot('disc_acgan', disc_acgan.cpu().data.numpy() * ACGAN_SCALE)
            plotter.plot('gradient_penalty', gradient_penalty.cpu().data.numpy())
            w_dist = np.abs(w_dist.item())

            # ---------------SAVING--------------------
            saver.update('D', D)
            saver.update('D_w', D.state_dict())
            saver.update('G', G)
            saver.update('G_w', G.state_dict())
            saver.update('wdist', w_dist)
            saver.update('D_cost', w_dist.item())
            saver.update('G_cost', w_dist.item())
            saver.update('D_opt', optimizer_d.state_dict())
            saver.update('G_opt', optimizer_g.state_dict())

        if w_dist < best_wdist:
            best_wdist = w_dist
            torch.save(G, output_path + "generator_best_abs.pt")
            torch.save(D, output_path + "discriminator_best_abs.pt")

        if iteration % VAL_FREQ_GAN == (VAL_FREQ_GAN - 1):
            dev_disc_costs = []
            for _, images in enumerate(val_loader):
                imgs = torch.Tensor(images[0].float())
                imgs = imgs.to(device)
                if MODE == GAN_types.WGAN_GP_conv: imgs = cnn_model(imgs, conv_only=True)
                with torch.no_grad():
                    imgs_v = imgs
                D_res, _ = D(imgs_v)
                _dev_disc_cost = -D_res.mean().cpu().data.numpy()
                dev_disc_costs.append(_dev_disc_cost)
            plotter.plot('dev_disc_cost', np.mean(dev_disc_costs))
            gen_images, gen_labels = generate_image_congan(G, fixed_noise_congan, fixed_label)
            if len(gen_images.size()) == 2:
                real_samples = real_data.cpu().data.numpy()
                real_labels = real_label.cpu().data.numpy()
                fake_samples = fake_data.cpu().data.numpy()
                fake_labels = fake_label.cpu().data.numpy()
                gen_images = gen_images.cpu().data.numpy()
                gen_labels = gen_labels.cpu().data.numpy()
                print_samples(real_samples=real_samples, fake_samples=fake_samples, iteration=iteration,
                              real_labels=real_labels, fake_labels=fake_labels,
                              folder='./result/GEOM_3D_LARGE/GAN_2D-data/condGAN/random')
                print_samples(real_samples=gen_images, real_labels=gen_labels, iteration=iteration,
                              folder='./result/GEOM_3D_LARGE/GAN_2D-data/condGAN/fixed')
            else:
                torchvision.utils.save_image(gen_images, OUTPUT_PATH_CONGAN + 'samples_{}.png'.format(iteration),
                                             nrow=8, padding=2)
            # ----------------------flush and save----------------------
            plotter.flush(print_info=True)
            saver.flush()
        plotter.tick()
        saver.tick()


def train_conganFC(D, G, cnn_model, optimizer_d, optimizer_g, dataloader, val_loader, fixed_noise, fixed_labels,
                   aux_criterion, output_path, labels):
    best_wdist = np.inf
    better_value_fnc = lambda best_wd, new_wd: new_wd < best_wd
    saver = Saver('wdist', better_value_fnc, output_path, START_ITER)
    plotter = Plotter(output_path=output_path, start_iter=START_ITER)
    dataiter = iter(dataloader)
    loss = nn.CrossEntropyLoss()
    print('start training ConGAN only  , mode: {}, iterations: {}'.format(MODE, NUM_ITER_GAN))
    for iteration in range(START_ITER, END_ITER_GAN):
        start_time = timer()

        # ---------------------TRAIN D------------------------
        for p in D.parameters():  # reset requires_grad
            p.requires_grad_(True)  # they are set to False below in training G
        for i in range(CRITIC_ITERS):
            D.zero_grad()
            # gen fake dataset_iter and load real dataset_iter

            batch = next(dataiter, None)
            if batch is None:
                dataiter = iter(dataloader)
                batch = next(dataiter)
            real_data = batch[0].float()  # batch[1] contains labels
            real_data.requires_grad_(True)
            real_label = batch[1].float()
            end = timer()
            # print(out_fname'---load real imgs elapsed time: {end-start}')

            f_label = real_label.detach()
            noise, fake_label = gen_rand_noise_and_label(label=f_label)
            fake_label_npy = fake_label.cpu().data.numpy()
            with torch.no_grad():
                noisev = noise  # totally freeze G, training D
                fake_labelv = fake_label
            fake_data = G(noisev, fake_labelv).detach()

            start = timer()
            real_data = real_data.to(device)
            if MODE == GAN_types.ConWGAN_GP_conv:
                real_data = cnn_model(real_data, conv_only=True)
            real_label = real_label.to(device)
            real_label_npy = real_label.cpu().data.numpy()

            # train with real dataset_iter
            disc_real, class_real = D(real_data, real_label)
            class_target_real = torch.argmax(real_label, dim=1).long()
            class_target_real_npy = class_target_real.cpu().data.numpy()
            aux_errD_real = loss(class_real, class_target_real)
            tmp = []
            for label in labels:
                idx = np.where(np.equal(real_label_npy, label).all(axis=1))[0]
                mean = disc_real[idx].mean(dim=0)
                tmp.append(mean)
            disc_real = torch.stack(tmp)
            errD_real = aux_errD_real.mean()

            # train with fake dataset_iter
            disc_fake, class_fake = D(fake_data, fake_label)
            class_target_fake = torch.argmax(fake_label, dim=1).long()
            class_target_fake_npy = class_target_fake.cpu().data.numpy()
            aux_errD_fake = loss(class_fake, class_target_fake)
            tmp = []
            for label in labels:
                idx = np.where(np.equal(fake_label_npy, label).all(axis=1))[0]
                mean = disc_fake[idx].mean(dim=0)
                tmp.append(mean)
            disc_fake = torch.stack(tmp)
            errD_fake = aux_errD_fake.mean()

            # wasserstein distance
            w_dist = (disc_fake - disc_real).mean(dim=0).sum()

            # train with interpolates dataset_iter
            gradient_penalty = calc_gradient_penalty_conwganFC(D, real_data, class_target_real_npy, fake_data,
                                                               real_label)

            # final disc cost
            disc_cost = w_dist + gradient_penalty
            disc_acgan = errD_real + errD_fake
            total_cost = disc_cost + ACGAN_SCALE * disc_acgan
            (total_cost).backward()
            optimizer_d.step()

            end = timer()
            # print(out_fname'---train D elapsed time: {end-start}')
            real_data_4plot = real_data.cpu().data.numpy()
            real_label_4plot = class_target_real_npy
            fake_data_4plot = fake_data.cpu().data.numpy()
            fake_label_4plot = class_target_fake_npy

        start = timer()
        # ---------------------TRAIN G------------------------
        for p in D.parameters():
            p.requires_grad_(False)  # freeze D

        gen_cost = None
        for i in range(GENER_ITERS):
            # print("Generator iters: " + str(i))
            G.zero_grad()
            f_label = np.random.randint(0, len(labels), BATCH_SIZE)
            f_label = np.eye(len(labels))[f_label]
            noise, fake_label = gen_rand_noise_and_label(label=f_label)
            fake_label_npy = fake_label.cpu().data.numpy()
            noise.requires_grad_(True)
            fake_data = G(noise, fake_label)
            gen_cost, gen_class = D(fake_data, fake_label)
            tmp = []
            for label in labels:
                idx = np.where(np.equal(fake_label_npy, label).all(axis=1))[0]
                mean = gen_cost[idx].mean(dim=0)
                tmp.append(mean)
            gen_cost = torch.stack(tmp)
            gen_cost = -gen_cost.sum()

            class_target_fake = torch.argmax(fake_label, dim=1).long()
            class_target_fake = class_target_fake.to(device)
            aux_errG = loss(gen_class, class_target_fake).mean()
            g_cost = ACGAN_SCALE_G * aux_errG + gen_cost
            g_cost.backward()

        optimizer_g.step()
        end = timer()
        # print(out_fname'---train G elapsed time: {end - start}')


        # ---------------VISUALIZATION---------------------
        plotter.plot('time', timer() - start_time)
        plotter.plot('train_disc_cost', disc_cost.cpu().data.numpy())
        plotter.plot('train_gen_cost', gen_cost.cpu().data.numpy())
        plotter.plot('wasserstein_distance', w_dist.cpu().data.numpy())
        plotter.plot('total_cost', total_cost.cpu().data.numpy())
        w_dist = np.abs(w_dist.item())

        # ---------------SAVING--------------------
        saver.update('D', D)
        saver.update('D_w', D.state_dict())
        saver.update('G', G)
        saver.update('G_w', G.state_dict())
        saver.update('wdist', w_dist)
        saver.update('D_cost', disc_cost.item())
        saver.update('G_cost', disc_cost.item())
        saver.update('D_opt', optimizer_d.state_dict())
        saver.update('G_opt', optimizer_g.state_dict())

        if w_dist < best_wdist:
            best_wdist = w_dist
            torch.save(G, output_path + "generator_best_abs.pt")
            torch.save(D, output_path + "discriminator_best_abs.pt")

        if iteration % VAL_FREQ_GAN == (VAL_FREQ_GAN - 1):
            dev_disc_costs = []
            for data, target in val_loader:
                imgs = torch.Tensor(data.float())
                imgs = imgs.to(device)
                target = target.float().to(device)
                if MODE == GAN_types.WGAN_GP_conv: imgs = cnn_model(imgs, conv_only=True)
                with torch.no_grad():
                    imgs_v = imgs
                    target_v = target
                D_res, _ = D(imgs_v, target_v)
                _dev_disc_cost = -D_res.mean().cpu().data.numpy()
                dev_disc_costs.append(_dev_disc_cost)
            plotter.plot('dev_disc_cost', np.mean(dev_disc_costs))
            gen_images = generate_image_conganFC(G, fixed_noise, fixed_labels)

            if len(gen_images.size()) == 2:
                print_samples(real_data_4plot, fake_data_4plot, iteration, real_label_4plot, fake_label_4plot)
            else:
                torchvision.utils.save_image(gen_images, OUTPUT_PATH_CONGAN + 'samples_{}.png'.format(iteration),
                                             nrow=8, padding=2)
            # ----------------------flush and save----------------------
            plotter.flush(print_info=True)
            saver.flush()
        plotter.tick()
        saver.tick()


def print_samples(real_samples=None, fake_samples=None, iteration=None, real_labels=None, fake_labels=None,
                  folder=None):
    unique_labels = np.unique(real_labels)
    colors = cm.rainbow(np.linspace(0, 1, 2 * len(unique_labels)))
    plt.clf()
    plt.hold(True)
    label = 0
    for i in range(0, len(colors), 2):
        if real_samples is not None:
            idx_real = np.where(real_labels == label)[0]
            plt.scatter(real_samples[idx_real, 0], real_samples[idx_real, 1], color=colors[i],
                        label='real_{}'.format(label))
        if fake_samples is not None:
            idx_fake = np.where(fake_labels == label)[0]
            plt.scatter(fake_samples[idx_fake, 0], fake_samples[idx_fake, 1], color=colors[i + 1],
                        label='fake_{}'.format(label))
        label += 1
    plt.title('Real vs fake samples')
    plt.legend()
    if folder is None:
        plt.savefig('./result/fake_vs_real_{}'.format(iteration))
    else:
        plt.savefig('{}_{}'.format(folder, iteration))


def train_cnn(cnn_model, cnn_optimizer, cnn_criterion, train_loader):
    # train_iter = iter(train_loader)
    # UNfreeze CNN
    for p in cnn_model.parameters():
        p.requires_grad_(True)  # UNfreeze CNN
    train_losses_cnn = np.asarray([])
    total_train_cnn, correct_train_cnn = 0, 0
    cnn_optimizer.zero_grad()
    cnn_model.train()
    for batch in train_loader:
        # Forward pass
        images = batch[0]
        labels = batch[1].long()
        images = images.to(device)
        labels = labels.to(device)
        outputs = cnn_model(images)
        _, predicted = torch.max(outputs.data, 1)

        # forward pass
        loss = cnn_criterion(outputs, labels)
        train_losses_cnn = np.append(train_losses_cnn, loss.item())

        # Backward and optimize
        cnn_optimizer.zero_grad()
        loss.backward()
        cnn_optimizer.step()

        total_train_cnn += labels.size(0)
        correct_train_cnn += (predicted == labels).sum().item()
    train_acc = 100 * correct_train_cnn / total_train_cnn
    train_loss_cnn = np.mean(train_losses_cnn)
    return train_acc, train_loss_cnn


def train_D(D, G, optimizer_d, cnn_model, loader, counter):
    # freeze CNN
    for p in cnn_model.parameters():
        p.requires_grad_(False)  # freeze CNN
    for p in D.parameters():  # reset requires_grad
        p.requires_grad_(True)  # they are set to False below in training G
    for i in range(CRITIC_ITERS):
        # print("Critic iter: " + str(i))

        start = timer()
        D.zero_grad()
        optimizer_d.zero_grad()

        # gen fake dataset_iter and load real dataset_iter
        noise = gen_rand_noise()
        with torch.no_grad():
            noisev = noise  # totally freeze G, training D
        fake_data = G(noisev).detach()

        batch = next(loader, None)
        if batch is None:
            loader = iter(loader)
            batch = loader.next()
        data = batch[0]  # batch[1] contains labels
        real_data = data.to(device).float()
        if MODE == GAN_types.WGAN_GP_conv: real_data = cnn_model(real_data, conv_only=True)

        # train with real dataset_iter
        disc_real = D(real_data)

        # track mean response of discriminator on real data over the training
        counter.add_samples(disc_real.cpu().data.numpy())
        disc_real = disc_real.mean()

        # train with fake dataset_iter
        disc_fake = D(fake_data)
        disc_fake = disc_fake.mean()

        # train with interpolates dataset_iter
        gradient_penalty = calc_gradient_penalty(D, real_data, fake_data)

        # final disc cost
        disc_cost = disc_fake - disc_real + gradient_penalty
        disc_cost.backward()
        w_dist = disc_fake - disc_real
        optimizer_d.step()
    weights = compute_weights(counter)
    return disc_cost.cpu().data.numpy(), w_dist.cpu().data.numpy(), weights


def train_ConD(D, G, optimizer_d, cnn_model, loader, counter, aux_criterion):
    for p in D.parameters():  # reset requires_grad
        p.requires_grad_(True)  # they are set to False below in training G
    for i in range(CRITIC_ITERS):
        # print("Critic iter: " + str(i))

        start = timer()
        D.zero_grad()

        # gen fake dataset_iter and load real dataset_iter
        f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
        noise = gen_rand_noise_with_label(f_label)
        with torch.no_grad():
            noisev = noise  # totally freeze G, training D
        fake_data = G(noisev).detach()
        end = timer()
        # print(out_fname'---gen G elapsed time: {end-start}')
        start = timer()
        batch = next(dataiter, None)
        if batch is None:
            dataiter = iter(loader)
            batch = next(dataiter)
        real_data = batch[0].float()  # batch[1] contains labels
        real_data.requires_grad_(True)
        real_label = batch[1].long()
        end = timer()
        # print(out_fname'---load real imgs elapsed time: {end-start}')

        start = timer()
        real_data = real_data.to(device)
        if MODE == GAN_types.ConWGAN_GP_conv:
            real_data = cnn_model(real_data, conv_only=True)
        real_label = real_label.to(device)

        # train with real dataset_iter
        disc_real, aux_output = D(real_data)

        # track mean response of discriminator on real data over the training
        counter.add_samples(disc_real.cpu().data.numpy())

        aux_errD_real = aux_criterion(aux_output, real_label)
        errD_real = aux_errD_real.mean()
        disc_real = disc_real.mean()

        # train with fake dataset_iter
        disc_fake, aux_output = D(fake_data)
        # aux_errD_fake = aux_criterion(aux_output, fake_label)
        # errD_fake = aux_errD_fake.mean()
        disc_fake = disc_fake.mean()

        # train with interpolates dataset_iter
        gradient_penalty = calc_gradient_penalty_conwgan(D, real_data, fake_data)

        # final disc cost
        disc_cost = disc_fake - disc_real + gradient_penalty
        disc_acgan = errD_real  # + errD_fake
        (disc_cost + ACGAN_SCALE * disc_acgan).backward()
        w_dist = disc_fake - disc_real
        optimizer_d.step()

        end = timer();
        # print(out_fname'---train D elapsed time: {end-start}')
    weights = compute_weights(counter)
    return disc_cost.cpu().data.numpy(), w_dist.cpu().data.numpy(), weights


def compute_weights(D, cnn_model, train_loader_gan_noshuffle, counter):
    '''
    Computes WEIGHTS for data sampling for CNN.
    Weights are set as an absolute distance from the mean discriminator response on real data.
    :return: list of WEIGHTS
    '''
    ts = timer()
    weights = []
    with torch.no_grad():
        for batch in iter(train_loader_gan_noshuffle):
            data = batch[0]
            data = data.to(device)
            if MODE == (GAN_types.WGAN_GP_conv or GAN_types.ConWGAN_GP_conv):
                data = cnn_model(data, conv_only=True)
            if MODE == (GAN_types.ConWGAN_GP or GAN_types.ConWGAN_GP_conv):
                Dout, aux_output = D(data).cpu().data.numpy()
            else:
                Dout = D(data).cpu().data.numpy()
            if WEIGHT_CRITERION == WEIGHT_CRITERION_types.WGAN:
                new_weights = np.subtract(Dout, counter.get_mean())
            elif WEIGHT_CRITERION == WEIGHT_CRITERION_types.ConWGAN and (
                            MODE == GAN_types.ConWGAN_GP or MODE == GAN_types.ConWGAN_GP_conv):
                new_weights = np.subtract(aux_output, counter.get_mean())
            else:
                raise Exception(
                    'Unsupported combinatiion of GAN MODE ({}) and WEIGHT_CRITERION ({}) in weights computation'
                        .format(MODE, WEIGHT_CRITERION))
            weights.extend(new_weights)
    # print('WEIGHTS computed in {}, '.format(timer() - ts), end='')
    return np.abs(weights).tolist()


def train_G(D, G, optimizer_g, mone):
    optimizer_g.zero_grad()
    # freeze D
    for p in D.parameters():
        p.requires_grad_(False)  # freeze D
    gen_cost = None
    for i in range(GENER_ITERS):
        # print("Generator iters: " + str(i))
        G.zero_grad()
        noise = gen_rand_noise()
        noise.requires_grad_(True)
        fake_data = G(noise)
        gen_cost = D(fake_data)
        gen_cost = gen_cost.mean()
        gen_cost.backward(mone)
        gen_cost = -gen_cost
    optimizer_g.step()
    return gen_cost.cpu().data.numpy()


def train_ConG(D, G, optimizer_g, aux_criterion):
    for p in D.parameters():
        p.requires_grad_(False)  # freeze D

    gen_cost = None
    for i in range(GENER_ITERS):
        # print("Generator iters: " + str(i))
        G.zero_grad()
        f_label = np.random.randint(0, NUM_CLASSES, BATCH_SIZE)
        noise = gen_rand_noise_with_label(f_label)
        noise.requires_grad_(True)
        fake_data = G(noise)
        gen_cost, gen_aux_output = D(fake_data)

        aux_label = torch.from_numpy(f_label).long()
        aux_label = aux_label.to(device)
        aux_errG = aux_criterion(gen_aux_output, aux_label).mean()
        gen_cost = -gen_cost.mean()
        g_cost = ACGAN_SCALE_G * aux_errG + gen_cost
        g_cost.backward()

    optimizer_g.step()
    end = timer()
    # print(out_fname'---train G elapsed time: {end - start}')
    return gen_cost.cpu().data.numpy()


def validate_cnn(val_loader, model, criterion, show=False):
    # switch to evaluate mode
    model.eval()
    losses = np.asarray([])
    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(val_loader):
            data = batch[0]
            data = data.to(device)
            target = batch[1].long()
            target = target.to(device)
            # compute output
            output = model(data)
            _, predicted = torch.max(output.data, 1)
            loss = criterion(output, target)
            losses = np.append(losses, loss.cpu().data.numpy())
            total += target.size(0)
            correct += (predicted == target).sum().item()
    val_loss = np.mean(losses)
    val_acc = 100 * correct / total
    return val_loss, val_acc


def train_cnn_gan(D, G, optimizer_g, optimizer_d, cnn_model, optimizer_cnn, cnn_criterion, train_loader_train,
                  cnn_train_loader_all, val_loader_cnn_all, weighted_loader_all, weighted_sampler, counter, mone,
                  output_path):
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver_both = Saver('val_loss', better_value_fnc, output_path, 0)
    plotter_both = Plotter(output_path=output_path)
    print('start iter of saver_both: {}'.format(saver_both.iter))
    print('start training BOTH CNN and GAN')
    gan_train_iter = iter(weighted_loader_all)
    for epoch in range(N_EPOCHS_BOTH):
        start_time = timer()

        # ---------------------TRAIN G------------------------
        # print('train G, ', end='')
        gen_cost = train_G(D, G, optimizer_g, mone)
        # print(timer() - start_time)
        # t = timer()

        # ---------------------TRAIN D------------------------
        # print('train D, ', end='')
        disc_cost, w_dist, weights = train_D(D, G, optimizer_d, cnn_model, train_loader_train, counter)
        # print(timer() - t)
        # t = timer()

        # --------------------TRAIN CNN-----------------------

        weighted_sampler.reweight(weights)
        train_acc, train_loss_cnn = train_cnn(cnn_model, optimizer_cnn, cnn_criterion, weighted_sampler)
        # print(timer() - t)
        # t = timer()
        # ------------------VALIDATE GAN----------------------
        # print('validate GAN, ', end='')
        cnn_val_iter = iter(val_loader_cnn_all)
        dev_disc_costs = []
        for _, images in enumerate(cnn_val_iter):
            imgs = torch.Tensor(images[0].float())
            imgs = imgs.to(device)
            if MODE == GAN_types.WGAN_GP_conv: imgs = cnn_model(imgs, conv_only=True)
            with torch.no_grad():
                imgs_v = imgs
            D_res = D(imgs_v)
            _dev_disc_cost = -D_res.mean().cpu().data.numpy()
            dev_disc_costs.append(_dev_disc_cost)
        disc_cost_val = np.mean(dev_disc_costs)
        # print(timer() - t)
        # t = timer()

        # ----------------- VALIDATE CNN ---------------------
        # print('validate CNN, ', end='')
        cnn_val_iter = iter(val_loader_cnn_all)
        val_loss, val_acc = validate_cnn(cnn_val_iter, cnn_model, cnn_criterion)
        # print(timer() - t)
        # t = timer()

        # print('plotting, ', end='')
        # ------ create plots ------
        plotter_both.plot('time', timer() - start_time)
        plotter_both.plot('train_loss', train_loss_cnn)
        plotter_both.plot('val_loss', val_loss)
        plotter_both.plot('train_acc', train_acc)
        plotter_both.plot('val_acc', val_acc)
        plotter_both.plot('gen_cost', gen_cost)
        plotter_both.plot('disc_cost', disc_cost)
        plotter_both.plot('disc_cost_val', disc_cost_val)
        plotter_both.plot('w_dist', w_dist)
        plotter_both.plot('mean_D_real', counter.get_mean())
        plotter_both.plot('var_D_real', counter.get_var())
        plotter_both.tick()
        # print(timer() - t)
        # t = timer()

        # print('saving, ', end='')
        # ------ save -------
        saver_both.update('cnn', cnn_model)
        saver_both.update('cnn_w', cnn_model.state_dict())
        saver_both.update('cnn_opt', optimizer_cnn.state_dict())
        saver_both.update('train_loss', train_loss_cnn)
        saver_both.update('val_loss', val_loss)
        saver_both.update('train_acc', train_acc)
        saver_both.update('val_acc', val_acc)
        saver_both.update('epoch', epoch)
        saver_both.update('D', D)
        saver_both.update('D_w', D.state_dict())
        saver_both.update('G', G)
        saver_both.update('G_w', G.state_dict())
        saver_both.update('gen_cost', gen_cost)
        saver_both.update('disc_cost', disc_cost)
        saver_both.update('disc_cost_val', disc_cost_val)
        saver_both.update('w_dist', w_dist)
        saver_both.update('mean_D_real', counter.get_mean())
        saver_both.update('var_D_real', counter.get_var())
        saver_both.update('counter_t', counter.get_t())
        saver_both.tick()
        # print(timer() - t)
        # t = timer()
        # print('flush, ', end='')
        if epoch % SAVE_FREQ_BOTH == (SAVE_FREQ_BOTH - 1):
            plotter_both.flush()
            saver_both.flush()
        # print(timer() - t)
        # ----- print info ------
        print(
            'Epoch [{}/{}]\nCNN:\tTrain Loss: {:.5f}, Train Acc: {:.2f}, Val Loss: {:.5f}, Val Acc: {:.2f}\n'
            'GAN:\tG cost: {:.5f}, D cost train: {:.5f}, D cost val: {:.5f}, W dist: {:.5f}\n\ttime: {:.3f}'.format(
                epoch + 1, N_EPOCHS_BOTH, train_loss_cnn, train_acc, val_loss, val_acc, gen_cost, disc_cost,
                disc_cost_val, w_dist, timer() - start_time))


def train_cnn_congan(D, G, optimizer_d, optimizer_g, cnn_model, cnn_optimizer, cnn_criterion, train_loader_train,
                     train_loader_all, cnn_train_loader_all, val_loader_cnn_all, weighted_loader_all, weighted_sampler,
                     counter, aux_criterion, output_path):
    better_value_fnc = lambda best_so_far, new_value: new_value < best_so_far
    saver_both = Saver('val_loss', better_value_fnc, OUTPUT_PATH_BOTH)
    plotter_both = Plotter(output_path=output_path)
    print('start iter of saver_both: {}'.format(saver_both.iter))
    print('start training BOTH CNN and GAN')
    gan_train_iter = iter(train_loader_all)
    for epoch in range(N_EPOCHS_BOTH):
        start_time = timer()

        # ---------------------TRAIN G------------------------
        # print('train G, ', end='')
        gen_cost = train_ConG(D, G, optimizer_g, aux_criterion)
        # print(timer() - start_time)
        # t = timer()

        # ---------------------TRAIN D------------------------
        # print('train D, ', end='')
        disc_cost, w_dist, weights = train_ConD(D, G, optimizer_d, cnn_model, train_loader_train, counter,
                                                aux_criterion)
        # print(timer() - t)
        # t = timer()

        # --------------------TRAIN CNN-----------------------

        weighted_sampler.reweight(weights)
        train_acc, train_loss_cnn = train_cnn(cnn_model, cnn_optimizer, cnn_criterion, weighted_loader_all)
        # print(timer() - t)
        # t = timer()
        # ------------------VALIDATE GAN----------------------
        # print('validate GAN, ', end='')
        cnn_val_iter = iter(val_loader_cnn_all)
        dev_disc_costs = []
        for _, images in enumerate(cnn_val_iter):
            imgs = torch.Tensor(images[0].float())
            imgs = imgs.to(device)
            if MODE == GAN_types.WGAN_GP_conv: imgs = cnn_model(imgs, conv_only=True)
            with torch.no_grad():
                imgs_v = imgs
            D_res, _ = D(imgs_v)
            _dev_disc_cost = -D_res.mean().cpu().data.numpy()
            dev_disc_costs.append(_dev_disc_cost)
        disc_cost_val = np.mean(dev_disc_costs)
        # print(timer() - t)
        # t = timer()

        # ----------------- VALIDATE CNN ---------------------
        # print('validate CNN, ', end='')
        cnn_val_iter = iter(val_loader_cnn_all)
        val_loss, val_acc = validate_cnn(cnn_val_iter, cnn_model, cnn_criterion)
        # print(timer() - t)
        # t = timer()

        # print('plotting, ', end='')
        # ------ create plots ------
        plotter_both.plot('time', timer() - start_time)
        plotter_both.plot('train_loss', train_loss_cnn)
        plotter_both.plot('val_loss', val_loss)
        plotter_both.plot('train_acc', train_acc)
        plotter_both.plot('val_acc', val_acc)
        plotter_both.plot('gen_cost', gen_cost)
        plotter_both.plot('disc_cost', disc_cost)
        plotter_both.plot('disc_cost_val', disc_cost_val)
        plotter_both.plot('w_dist', w_dist)
        plotter_both.plot('mean_D_real', counter.get_mean())
        plotter_both.plot('var_D_real', counter.get_var())
        plotter_both.tick()
        # print(timer() - t)
        # t = timer()

        # print('saving, ', end='')
        # ------ save -------
        saver_both.update('cnn', cnn_model)
        saver_both.update('cnn_w', cnn_model.state_dict())
        saver_both.update('cnn_opt', cnn_optimizer.state_dict())
        saver_both.update('train_loss', train_loss_cnn)
        saver_both.update('val_loss', val_loss)
        saver_both.update('train_acc', train_acc)
        saver_both.update('val_acc', val_acc)
        saver_both.update('epoch', epoch)
        saver_both.update('D', D)
        saver_both.update('D_w', D.state_dict())
        saver_both.update('G', G)
        saver_both.update('G_w', G.state_dict())
        saver_both.update('gen_cost', gen_cost)
        saver_both.update('disc_cost', disc_cost)
        saver_both.update('disc_cost_val', disc_cost_val)
        saver_both.update('w_dist', w_dist)
        saver_both.update('mean_D_real', counter.get_mean())
        saver_both.update('var_D_real', counter.get_var())
        saver_both.update('counter_t', counter.get_t())
        saver_both.tick()
        # print(timer() - t)
        # t = timer()
        # print('flush, ', end='')
        if epoch % SAVE_FREQ_BOTH == (SAVE_FREQ_BOTH - 1):
            plotter_both.flush()
            saver_both.flush()
        # print(timer() - t)
        # ----- print info ------
        print(
            'Epoch [{}/{}]\nCNN:\tTrain Loss: {:.5f}, Train Acc: {:.2f}, Val Loss: {:.5f}, Val Acc: {:.2f}\n'
            'GAN:\tG cost: {:.5f}, D cost train: {:.5f}, D cost val: {:.5f}, W dist: {:.5f}\n\ttime: {:.3f}'.format(
                epoch + 1, N_EPOCHS_BOTH, train_loss_cnn, train_acc, val_loss, val_acc, gen_cost, disc_cost,
                disc_cost_val, w_dist, timer() - start_time))


def complete_training(D, G, optimizer_d, optimizer_g, aux_criterion, cnn_model, cnn_optimizer, cnn_criterion,
                      train_loader_train, val_loader_train, train_loader_test, train_loader_all, val_loader_all,
                      weighted_loader_all, weighted_sampler, mone, fixed_noise, writer, log_file):
    print('GAN mode: {}, number of GAN iterations: {}, number of samples for training on complete dataset: {}'
          .format(MODE, NUM_ITER_GAN, NUM_SAMPLES_WEIGHT))
    print('bottom dataset size: {}, unlimited dataset size: {}'.format(len(train_loader_train.dataset),
                                                                       len(train_loader_all.dataset)))
    # CNN only
    train_cnn_only(cnn_model, cnn_optimizer, cnn_criterion, train_loader_train)
    torch.save(cnn_model, './cnn_basic.pt')
    torch.save(cnn_optimizer.state_dict(), './cnn_opt.pt')

    # GAN only
    if MODE == (GAN_types.WGAN_GP or GAN_types.WGAN_GP_conv):
        train_gan(D, G, cnn_model, optimizer_d, optimizer_g, train_loader_train, val_loader_train, mone, fixed_noise,
                  writer, log_file, train_loader_train, train_loader_test)
    else:
        train_congan(D, G, cnn_model, optimizer_d, optimizer_g, train_loader_train, val_loader_train, mone, fixed_noise,
                     log_file, train_loader_train, train_loader_test, aux_criterion)
    # initialize counter
    counter = init_counter(D, train_loader_train, cnn_model)

    # check CNN on the complete dataset
    total, correct = 0, 0
    with torch.no_grad():
        cnn_model.eval()
        for batch in train_loader_all:
            data = batch[0].to(device)
            labels = batch[1].long().to(device)
            output = cnn_model(data)
            _, predicted = torch.max(output.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    acc = 100 * correct / total
    print('CNN accuracy on unlimited dataset: {:.2f}'.format(acc))

    # check CNN on the training dataset
    total, correct = 0, 0
    with torch.no_grad():
        cnn_model.eval()
        for batch in train_loader_train:
            data = batch[0].to(device)
            labels = batch[1].long().to(device)
            output = cnn_model(data)
            _, predicted = torch.max(output.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    acc = 100 * correct / total
    print('CNN accuracy on the BOTTOM dataset: {:.2f}'.format(acc))

    # CNN and GAN/ConGAN together
    if MODE == (GAN_types.WGAN_GP or GAN_types.WGAN_GP_conv):
        train_cnn_gan(D, G, optimizer_g, optimizer_d, cnn_model, cnn_optimizer, cnn_criterion, train_loader_train,
                      train_loader_all, val_loader_all, weighted_loader_all, weighted_sampler, counter, mone)
    else:
        train_cnn_congan(D, G, optimizer_d, optimizer_g, cnn_model, cnn_optimizer, cnn_criterion, train_loader_train,
                         train_loader_all, cnn_train_loader_all, val_loader_all, weighted_loader_all, weighted_sampler,
                         counter, aux_criterion)
    print()

    print('train basic CNN further without data weighting')
    train_cnn_only_further()
