import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

plt.switch_backend('agg')
import torch
from torchvision import transforms, datasets
import numpy as np
from models.dataset import GeomDataset, ConvDataset
from timeit import default_timer as timer
import matplotlib.mlab as mlab
import pickle

# ====== CONSTANTS ========
BATCH_SIZE = 1
DISC_TRAIN_DATASET = -7.8445  # -7.8445 for 'basic', 6.220725636175439 for 'limited-center-down_conv'
D = None
G = None
TRAIN_GEN, TEST_GEN = None, None
cuda_available = torch.cuda.is_available()


# ===== DEFINITIONS OF FUNCTIONS ===========

def inf_train_gen(train=True, classes=None):
    gen = TRAIN_GEN if train else TEST_GEN
    while True:
        for images, targets in gen():
            yield images


def load_networks(path, epoch=None, cpu=False):
    gen = "/generator_{}.pt".format(epoch) if epoch is not None else "/generator.pt"
    dis = "/discriminator_{}.pt".format(epoch) if epoch is not None else "/discriminator.pt"
    print('Loading from {} files {} and {}. Cpu={}'.format(path, gen, dis, cpu))
    if cpu:
        G = torch.load(path + gen, map_location=lambda storage, loc: storage)
        G = G.cpu()
        D = torch.load(path + dis, map_location=lambda storage, loc: storage)
        D = D.cpu()
    else:
        G = torch.load(path + gen)
        G = G.cuda()
        D = torch.load(path + dis)
        D = D.cuda()
    print('networks loaded')
    return D, G


def get_loader(path_to_folder, csvfile, batch_size=BATCH_SIZE, shuffle=True):
    resize = transforms.Resize(64)
    crop = transforms.CenterCrop(64)
    to_tensor = transforms.ToTensor()
    dataset = GeomDataset(csvfile, path_to_folder, resize, crop, to_tensor)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=shuffle, num_workers=5,
                                                 drop_last=True, pin_memory=True)
    print('loaded dataset_iter, number of samples: {}'.format(len(dataset)))
    return dataset_loader


def load_conv_data(labels_file, data_file, drop_last=True, batch_size=BATCH_SIZE, shuffle=True):
    dataset = ConvDataset(labels_file, data_file)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=shuffle, num_workers=5,
                                                 drop_last=drop_last, pin_memory=True)
    # print('loaded CONV dataset_iter, number of samples: {}'.format(len(dataset_iter)))
    return dataset_loader


def limit_data(data, label):
    pass


def disc_train_dataset(dataiter, discriminator, spec_label=None):
    '''

    :param dataiter: loader of the dataset_iter
    :param spec_label: label of the class of which to compute the mean value
    :return: mean discriminator value over the whole dataset_iter or over just one class specified by spec_label
    '''
    vals = np.asarray([])
    completed = 0
    start = timer()
    total = len(dataiter)
    while True:
        batch = next(dataiter, None)
        if batch is None: break
        data = batch[0]
        if cuda_available: data = data.cuda()
        completed += 1
        label = batch[1]
        if spec_label is not None:
            data, label = limit_data(data, label)
        disc = discriminator(data).data.cpu().numpy()
        vals = np.append(vals, disc)
        if completed < 4:
            print('time {:f}, completed {}/{}, WD {:f}'.format(timer() - start, completed, total, np.mean(disc)))
    res = vals.data
    return res, np.mean(res)


def wdist(samples, D, disc_train, label=None):
    disc_sample = D(samples).data.cpu().numpy()
    # print(disc_train)
    wd = disc_sample
    # wd = np.abs(disc_sample - disc_train)
    # print('wd={}'.format(wd))
    return wd


def wdist_test(test_iter, D=None, n_test_iters=None, train_iter=None, n_train_iters=1, show=False, disc_train=None):
    wds_train = []
    # print('disc_train={}'.format(disc_train))
    if train_iter is not None:
        print('train iter size: {}, running for {} iterations'.format(len(train_iter), n_train_iters))
        start_time = timer()
        print('start timer')
        for i in range(n_train_iters):
            batch = next(train_iter)
            data = batch[0]
            if cuda_available: data = data.cuda()
            label = batch[1]
            wd = wdist(data, D, disc_train)
            if show:
                for d, w in zip(data, wd):
                    show_sample_wdist(d, w)
            wds_train.extend(wd)
            if i < 5: print('mean disc of train dataset_iter batch: {}'.format(np.mean(wd)))
    print('mean train wdist: {}, time: {}, wds size: {}, var: {}, min: {}, max: {}'
          .format(np.mean(wds_train), timer() - start_time, len(wds_train),
                  np.var(wds_train), np.min(wds_train), np.max(wds_train)))
    wds_test = []
    batch_n = 0
    print('test iter size: {}, running for {} iterations'.format(len(test_iter), n_test_iters))
    while True:
        batch_n += 1
        batch = next(test_iter, None)
        if batch is None or n_test_iters is not None and n_test_iters < batch_n: break
        data = batch[0]
        if cuda_available: data = data.cuda()
        label = batch[1]
        wd = wdist(data, D, disc_train)
        if show:
            for d, w in zip(data, wd):
                show_sample_wdist(d, w)
        wds_test.extend(wd)
        if batch_n < 6: print('mean disc of test dataset_iter batch: {}'.format(np.mean(wd)))
    print('mean test disc: {}, var: {}, min: {}, max: {}'.format(np.mean(wds_test), np.var(wds_test), np.min(wds_test),
                                                                 np.max(wds_test)))
    wds_test_2trainmean = np.abs(np.subtract(wds_test, np.mean(wds_train)))
    print('mean test disc 2 train mean: {}, var: {}, min: {}, max: {}'
          .format(np.mean(wds_test_2trainmean), np.var(wds_test_2trainmean), np.min(wds_test_2trainmean),
                  np.max(wds_test_2trainmean)))
    return wds_test, np.mean(wds_test), wds_train, np.mean(wds_train)


def show_sample_wdist(sample, dist):
    sample = sample.data.numpy()
    im = np.rollaxis(sample, 0, 3)
    plt.imshow(im)
    plt.title(dist)
    plt.show()
    plt.waitforbuttonpress()


def basic_test():
    # load learned critic
    net_path = './result/WGAN'
    D, G = load_networks(net_path, cpu=not cuda_available)
    # train settings
    train_type = 'basic'
    train_folder = './datasets/' + train_type + '/train'
    train_csv = train_folder + '/geom_data.csv'
    TRAIN_GEN = get_loader(train_folder, train_csv, batch_size=64)
    train_iter = iter(TRAIN_GEN)
    # test settings
    test_type = 'limit-center-up_rg'
    test_folder = './datasets/' + test_type + '/train'
    test_csv = test_folder + '/geom_data.csv'
    TEST_GEN = get_loader(test_folder, test_csv, batch_size=64)
    test_iter = iter(TEST_GEN)
    # compute mean discriminator response for train dataset_iter
    disc_train_samples, disc_train_mean = disc_train_dataset(train_iter, D)
    print('disc_train: {}'.format(disc_train_mean))
    # compute wasserstein distance of test set
    train_iter = iter(TRAIN_GEN)
    test_iter = iter(TEST_GEN)
    test_samples_wdist = wdist_test(test_iter, D=D, n_test_iters=len(test_iter), train_iter=train_iter,
                                    n_train_iters=len(train_iter), disc_train=disc_train_mean)


def conv_test():
    # load learned critic
    net_path = './result/WGAN/CONV/'
    D, G = load_networks(net_path, 16799)
    # train settings
    train_type = 'limit-center-down_rg_conv'
    data_file_train = './datasets/' + train_type + '/conv.npy'
    labels_file_train = './datasets/' + train_type + '/labels.npy'
    TRAIN_GEN = load_conv_data(labels_file_train, data_file_train, drop_last=False, batch_size=64)
    train_iter = iter(TRAIN_GEN)
    # test settings
    test_type = 'limit-center-up_rg_conv'
    data_file_test = './datasets/' + test_type + '/conv.npy'
    labels_file_test = './datasets/' + test_type + '/labels.npy'
    TEST_GEN = load_conv_data(labels_file_test, data_file_test, drop_last=False, batch_size=64)
    test_iter = iter(TEST_GEN)
    # compute mean discriminator response for train dataset_iter
    disc_train_samples, disc_train_mean = disc_train_dataset(train_iter, D)
    print('disc_train: {}'.format(disc_train_mean))
    # compute wasserstein distance of test set
    train_iter = iter(TRAIN_GEN)
    test_iter = iter(TEST_GEN)
    test_samples_wdist = wdist_test(test_iter, D=D, n_test_iters=len(test_iter), train_iter=train_iter,
                                    n_train_iters=len(train_iter), disc_train=disc_train_mean, show=False)


def gauss_test():
    # load learned critic
    net_path = './result/WGAN/CONV/'
    D, G = load_networks(net_path, 16799)

    # train settings
    train_type = 'limit-center-down_rg_conv'
    data_file_train = './datasets/' + train_type + '/conv.npy'
    labels_file_train = './datasets/' + train_type + '/labels.npy'
    TRAIN_GEN = load_conv_data(labels_file_train, data_file_train, drop_last=False, batch_size=64)
    train_iter = iter(TRAIN_GEN)

    # test settings
    test_type = 'limit-center-up_rg_conv'
    data_file_test = './datasets/' + test_type + '/conv.npy'
    labels_file_test = './datasets/' + test_type + '/labels.npy'
    TEST_GEN = load_conv_data(labels_file_test, data_file_test, drop_last=False, batch_size=64)
    test_iter = iter(TEST_GEN)

    # compute mean discriminator response for train dataset_iter
    disc_train_samples, disc_train_mean = disc_train_dataset(train_iter, D)
    print('disc_train: {}'.format(disc_train_mean))
    # compute wasserstein distance of test set
    train_iter = iter(TRAIN_GEN)
    test_iter = iter(TEST_GEN)
    wds_test, wds_test_mean, wds_train, wds_train_mean = wdist_test(test_iter, D=D, n_test_iters=len(test_iter),
                                                                    train_iter=train_iter,
                                                                    n_train_iters=len(train_iter),
                                                                    disc_train=disc_train_mean, show=False)

    wds_train_var = np.var(wds_train)
    wds_train_median = np.median(wds_train)
    wds_train_max, wds_train_min = np.max(wds_train), np.min(wds_train)
    gauss_train_x = np.linspace(wds_train_mean - 5 * wds_train_var, wds_train_mean + 5 * wds_train_var, 100)
    gauss_train_y = mlab.normpdf(gauss_train_x, wds_train_mean, wds_train_var)
    dist_sigma_train = np.subtract(wds_train, wds_train_mean)
    dist_sigma_train = np.divide(dist_sigma_train, wds_train_var)
    dist_sigma_train_mu, dist_sigma_train_sigma = np.mean(dist_sigma_train), np.var(dist_sigma_train)
    gauss_train_x_dist = np.linspace(dist_sigma_train_mu - 5 * dist_sigma_train_sigma,
                                     dist_sigma_train_mu + 5 * dist_sigma_train_sigma, 100)
    gauss_train_y_dist = mlab.normpdf(gauss_train_x_dist, dist_sigma_train_mu, dist_sigma_train_sigma)

    wds_test_var = np.var(wds_test)
    wds_test_median = np.median(wds_test)
    wds_test_max, wds_test_min = np.max(wds_test), np.min(wds_test)
    gauss_test_x = np.linspace(wds_test_mean - 5 * wds_test_var, wds_test_mean + 5 * wds_test_var, 100)
    gauss_test_y = mlab.normpdf(gauss_test_x, wds_test_mean, wds_test_var)
    dist_sigma_test = np.subtract(wds_test, wds_train_mean)
    dist_sigma_test = np.divide(dist_sigma_test, wds_train_var)
    dist_sigma_test_mu, dist_sigma_test_sigma = np.mean(dist_sigma_test), np.var(dist_sigma_test)
    gauss_test_x_dist = np.linspace(dist_sigma_test_mu - 5 * dist_sigma_test_sigma,
                                    dist_sigma_test_mu + 5 * dist_sigma_test_sigma, 100)
    gauss_test_y_dist = mlab.normpdf(gauss_test_x_dist, dist_sigma_test_mu, dist_sigma_test_sigma)

    print(
        'test: u={:.5f},sigma={:.5f}, median={:.5f}, min={:.5f}, max={:.5f}'
        '\ntrain: u={:.5f},sigma={:.5f}, median={:.5f}, min={:.5f}, max={:.5f}'.format(
            wds_test_mean, wds_test_var, wds_test_median, wds_test_min, wds_test_max,
            wds_train_mean, wds_train_var, wds_train_median, wds_train_min, wds_train_max))

    plt.plot(dist_sigma_train, np.zeros(len(dist_sigma_train)), 'rx', label='train distances')
    plt.plot(gauss_train_x_dist, gauss_train_y_dist, 'r', label='train gauss')
    plt.plot(dist_sigma_test, np.zeros(len(dist_sigma_test)), 'bx', label='test distances')
    plt.plot(gauss_test_x_dist, gauss_test_y_dist, 'b', label='test gauss')
    plt.legend(loc='best')
    plt.title('distance to the mean value of train distribution measured in variances')
    plt.savefig('./experiment_gauss_variances.jpg')

    plt.clf()

    plt.plot(wds_train, np.zeros(len(wds_train)), 'rx', label='train discriminator')
    plt.plot(gauss_train_x, gauss_train_y, 'r', label='train gauss')
    plt.plot(wds_test, np.zeros(len(wds_test)), 'bx', label='test discriminator')
    plt.plot(gauss_test_x, gauss_test_y, 'b', label='test gauss')
    plt.legend(loc='best')
    plt.title(
        'D output; train: mu={:.3f}, sigma={:.3f}; test: mu={:.3f}, sigma={:.3f}'
            .format(wds_train_mean, wds_train_var, wds_test_mean, wds_test_var))
    plt.savefig('./experiment_gauss_discriminator-output.jpg')


def cifar_dist():
    # load learned critic
    net_path = './result/WGAN'
    D, G = load_networks(net_path, cpu=not cuda_available)
    # train settings
    train_type = 'basic'
    train_folder = './datasets/' + train_type + '/train'
    train_csv = train_folder + '/geom_data.csv'
    TRAIN_GEN = get_loader(train_folder, train_csv, batch_size=64)
    train_iter = iter(TRAIN_GEN)
    # test settings
    transform = transforms.Compose(
        [transforms.Resize(64),
         transforms.CenterCrop(64),
         transforms.ToTensor()])
    trainset = datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)
    TEST_GEN = torch.utils.data.DataLoader(trainset, batch_size=4, shuffle=True, num_workers=2)
    test_iter = iter(TEST_GEN)
    # compute mean discriminator response for train dataset_iter
    disc_train_samples, disc_train_mean = disc_train_dataset(train_iter, D)
    print('disc_train: {}'.format(disc_train_mean))
    # compute wasserstein distance of test set
    train_iter = iter(TRAIN_GEN)
    test_iter = iter(TEST_GEN)
    test_samples_wdist = wdist_test(test_iter, D=D, n_test_iters=len(test_iter), train_iter=train_iter,
                                    n_train_iters=len(train_iter), disc_train=disc_train_mean)


def load_my_data(path_to_folder, csvfile):
    resize = transforms.Resize(64)
    crop = transforms.CenterCrop(64)
    to_tensor = transforms.ToTensor()
    dataset = GeomDataset(csvfile, path_to_folder, resize, crop, to_tensor)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True, num_workers=5,
                                                 drop_last=True, pin_memory=True)
    # print('loaded dataset_iter, number of samples: {}'.format(len(dataset_iter)))
    return dataset_loader


def test_whole_dataset():
    save_best = np.load('./result/CNN/splitted/save_best.pt')
    cnn_best = torch.load('./result/CNN/splitted/cnn_best.pt')
    device = torch.device('cuda:0' if cuda_available else 'cpu')
    train_loader = load_my_data('./datasets/splitted/train', './datasets/splitted/train/geom_data_all.csv')
    val_loader = load_my_data('./datasets/splitted/val', './datasets/splitted/val/geom_data_all.csv')
    cnn_best.eval()

    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(train_loader):
            input = batch[0]
            target = batch[1]
            input = input.to(device)
            target = target.to(device)
            # compute output
            output = cnn_best(input)
            _, predicted = torch.max(output.data, 1)
            total += target.size(0)
            correct += (predicted == target).sum().item()
    train_acc = 100 * correct / total
    print('train accuracy: {}'.format(train_acc))

    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(val_loader):
            input = batch[0]
            target = batch[1]
            input = input.to(device)
            target = target.to(device)
            # compute output
            output = cnn_best(input)
            _, predicted = torch.max(output.data, 1)
            total += target.size(0)
            correct += (predicted == target).sum().item()
    train_acc = 100 * correct / total
    print('val accuracy: {}'.format(train_acc))


if __name__ == '__main__':
    # print('BASIC:')
    # basic_test()
    # print('\nCONV:')
    # conv_test()
    gauss_test()
    #print('CIFAR10 test:')
    #cifar_dist()
    test_whole_dataset()
