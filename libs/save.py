import numpy as np

import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

import collections
import time
# import cPickle as pickle
import pickle
import torch
from timeit import default_timer as timer


class Saver():
    _since_beginning = collections.defaultdict(lambda: {})
    _since_last_flush = collections.defaultdict(lambda: {})
    best_crit = None
    new_best = lambda best_so_far, new_value: None
    cnn, cnn_w, cnn_opt, D, D_w, D_opt, G, G_w, G_opt = None, None, None, None, None, None, None, None, None
    cnn_best, cnn_w_best, cnn_opt_best, D_best, D_w_best, D_opt_best, G_best, G_w_best, G_opt_best = None, None, None, None, None, None, None, None, None
    net_last = {'cnn': cnn, 'cnn_w': cnn_w, 'cnn_opt': cnn_opt, 'D': D, 'D_w': D_w, 'D_opt': D_opt, 'G': G, 'G_w': G_w,
                'G_opt': G_opt}
    net_best = {'cnn': cnn_best, 'cnn_w': cnn_w_best, 'cnn_opt': cnn_opt_best, 'D': D_best, 'D_w': D_w_best,
                'D_opt': D_opt_best, 'G': G_best, 'G_w': G_w_best, 'G_opt': G_opt_best}
    used_spec = set()
    best_iter = 0
    do_save_best = False

    def __init__(self, crit, better_value_fnc, out_folder, start_iter=0):
        '''
        Saver init.
        :param crit: Name of optimized variable.
        :param better_value_fnc: Function comparing optimized variable
        :param out_folder: path to the output folder
        '''
        self._crit = crit
        self.better_value_fnc = better_value_fnc
        self.out_folder = out_folder
        self.iter = [start_iter]
        for key in self.net_last.keys():
            self.net_last[key] = None
        for key in self.net_best.keys():
            self.net_best[key] = None

    def tick(self, ):
        if self.new_best:  # this iteration was the best so far, save it
            self.best_iter = self.iter[0]
            # print('used spec: {}'.format(self.used_spec))
            for name in self.used_spec:
                self.net_best[name] = self.net_last[name]
        # prepare for next iter
        self.iter[0] += 1
        self.new_best = False  # reset tracker

    def set_crit(self, crit):
        """
        Sets criterium for evaluating.
        :param crit: choosen criterium
        :return: nothing
        """
        self._crit = crit

    def update(self, name, value):
        if name in self.net_last.keys():
            self.net_last[name] = value
            self.used_spec.add(name)
        else:
            self._since_last_flush[name][self.iter[0]] = value
        if name == self._crit and (self.best_crit is None or self.better_value_fnc(self.best_crit, value)):
            print('iter {}: new better value for crit ({}={}->{})'.format(self.iter[0], name, self.best_crit, value))
            # print('{} is better than {} wrt {}'.format(value, self.best_crit, name))
            self.best_crit = value
            self.new_best = True
            self.do_save_best = True
            self.best_iter = self.iter[0]

    def flush(self):
        for name, vals in self._since_last_flush.items():
            if name in self.net_last.keys():
                self._since_beginning[name] = vals
            else:
                self._since_beginning[name].update(vals)

        if self.do_save_best:
            self.save_best(self._since_beginning)
        self.save_all(self._since_beginning)

        self._since_last_flush.clear()
        self.do_save_best = False  # reset

        # with open('log.pkl', 'wb') as out_fname:
        #    pickle.dump(dict(self._since_beginning), out_fname, pickle.HIGHEST_PROTOCOL)

    def save_all(self, to_save):
        for name in self.net_last.keys():
            if self.net_last[name] is not None:
                fname = self.out_folder + '/{}_last.pt'.format(name)
                st = timer()
                torch.save(self.net_last[name], fname)
                # print('saved last {} in {}'.format(name, timer() - st))
        fname = self.out_folder + '/save_last'
        st = timer()
        dict2save = {}
        for name, value in to_save.items():
            # print(name, value)
            dict2save[name] = value
        np.save(fname, dict(dict2save))
        # print('saved last dict in {}'.format(timer() - st))

    def save_best(self, dict_all):
        for name in self.net_best.keys():
            if self.net_best[name] is not None:
                fname = self.out_folder + '/{}_best.pt'.format(name)
                st = timer()
                torch.save(self.net_last[name], fname)
                # print('saved best {} in {}'.format(name, timer() - st))
        fname = self.out_folder + '/save_best'
        dict2save = {}
        for name, value in dict_all.items():
            # print(name, value)
            dict2save[name] = [value[x] for x in range(0, self.best_iter + 1) if x in value.keys()]
        st = timer()
        np.save(fname, dict2save)
        # print('saved best dict in {}'.format(timer() - st))
