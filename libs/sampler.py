import torch
import numpy as np
from torch._six import int_classes as _int_classes
from torch.utils.data.sampler import Sampler
from torch.utils.data.dataloader import DataLoader
from torch.utils.data.dataloader import _DataLoaderIter
from torch.utils.data.dataloader import *
from timeit import default_timer as timer


class MyWeightedRandomSampler(Sampler):
    r"""Samples elements from [0,..,len(WEIGHTS)-1] with given probabilities (WEIGHTS).

    Arguments:
        weights (list)   : a list of WEIGHTS, not necessary summing up to one
        num_samples (int): number of samples to draw
        replacement (bool): if ``True``, samples are drawn with replacement.
            If not, they are drawn without replacement, which means that when a
            sample index is drawn for a row, it cannot be drawn again for that row.
    """

    def __init__(self, weights, num_samples, replacement=False):
        if not isinstance(num_samples, _int_classes) or isinstance(num_samples, bool) or \
                        num_samples <= 0:
            raise ValueError("num_samples should be a positive integeral "
                             "value, but got num_samples={}".format(num_samples))
        if not isinstance(replacement, bool):
            raise ValueError("replacement should be a boolean value, but got "
                             "replacement={}".format(replacement))
        if weights is None:
            weights = np.ones(num_samples)
        self.weights = torch.tensor(weights, dtype=torch.double)
        self.num_samples = num_samples
        self.replacement = replacement

    def reweight(self, new_weights):
        self.weights = torch.tensor(new_weights, dtype=torch.double)

    def __iter__(self):
        return iter(torch.multinomial(self.weights, self.num_samples, self.replacement))

    def __len__(self):
        return self.num_samples


class MyDataLoader(object):
    r"""
    Data loader. Combines a dataset and a SAMPLER, and provides
    single- or multi-process iterators over the dataset.

    Arguments:
        dataset (Dataset): dataset from which to load the data.
        batch_size (int, optional): how many samples per batch to load
            (default: 1).
        shuffle (bool, optional): set to ``True`` to have the data reshuffled
            at every epoch (default: False).
        sampler (Sampler, optional): defines the strategy to draw samples from
            the dataset. If specified, ``shuffle`` must be False.
        batch_sampler (Sampler, optional): like SAMPLER, but returns a batch of
            indices at a time. Mutually exclusive with batch_size, shuffle,
            SAMPLER, and drop_last.
        num_workers (int, optional): how many subprocesses to use for data
            loading. 0 means that the data will be loaded in the main process.
            (default: 0)
        collate_fn (callable, optional): merges a list of samples to form a mini-batch.
        pin_memory (bool, optional): If ``True``, the data loader will copy tensors
            into CUDA pinned memory before returning them.
        drop_last (bool, optional): set to ``True`` to drop the last incomplete batch,
            if the dataset size is not divisible by the batch size. If ``False`` and
            the size of dataset is not divisible by the batch size, then the last batch
            will be smaller. (default: False)
        timeout (numeric, optional): if positive, the timeout value for collecting a batch
            from workers. Should always be non-negative. (default: 0)
        worker_init_fn (callable, optional): If not None, this will be called on each
            worker subprocess with the worker id (an int in ``[0, num_workers - 1]``) as
            input, after seeding and before data loading. (default: None)

    .. note:: By default, each worker will have its PyTorch seed set to
              ``base_seed + worker_id``, where ``base_seed`` is a long generated
              by main process using its RNG. However, seeds for other libraies
              may be duplicated upon initializing workers (w.g., NumPy), causing
              each worker to return identical random numbers. (See
              :ref:`dataloader-workers-random-seed` section in FAQ.) You may
              use ``torch.initial_seed()`` to access the PyTorch seed for each
              worker in :attr:`worker_init_fn`, and use it to set other seeds
              before data loading.

    .. warning:: If ``spawn`` start method is used, :attr:`worker_init_fn` cannot be an
                 unpicklable object, e.g., a lambda function.
    """

    __initialized = False

    def __init__(self, dataset, batch_size=1, shuffle=False, sampler=None, batch_sampler=None,
                 num_workers=0, collate_fn=default_collate, pin_memory=False, drop_last=False,
                 timeout=0, worker_init_fn=None):
        self.dataset = dataset
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.collate_fn = collate_fn
        self.pin_memory = pin_memory
        self.drop_last = drop_last
        self.timeout = timeout
        self.worker_init_fn = worker_init_fn

        if timeout < 0:
            raise ValueError('timeout option should be non-negative')

        if batch_sampler is not None:
            if batch_size > 1 or shuffle or sampler is not None or drop_last:
                raise ValueError('batch_sampler option is mutually exclusive '
                                 'with batch_size, shuffle, SAMPLER, and '
                                 'drop_last')
            self.batch_size = None
            self.drop_last = None

        if sampler is not None and shuffle:
            raise ValueError('SAMPLER option is mutually exclusive with '
                             'shuffle')

        if self.num_workers < 0:
            raise ValueError('num_workers option cannot be negative; '
                             'use num_workers=0 to disable multiprocessing.')

        if batch_sampler is None:
            if sampler is None:
                if shuffle:
                    sampler = RandomSampler(dataset)
                else:
                    sampler = SequentialSampler(dataset)
            batch_sampler = BatchSampler(sampler, batch_size, drop_last)

        self.sampler = sampler
        self.batch_sampler = batch_sampler
        self.__initialized = True

    def __setattr__(self, attr, val):
        if self.__initialized and attr in ('batch_size', 'SAMPLER', 'drop_last'):
            raise ValueError('{} attribute should not be set after {} is '
                             'initialized'.format(attr, self.__class__.__name__))

        super(MyDataLoader, self).__setattr__(attr, val)

    def __iter__(self):
        return MyDataLoaderIter(self)

    def __len__(self):
        return len(self.batch_sampler)


class MyDataLoaderIter(_DataLoaderIter):
    def __init__(self, loader):
        super(MyDataLoaderIter, self).__init__(loader=loader)

    def __next__(self):
        if self.num_workers == 0:  # same-process loading
            indices = next(self.sample_iter)  # may raise StopIteration
            batch = self.collate_fn([self.dataset[i] for i in indices])
            if self.pin_memory:
                batch = pin_memory_batch(batch)
            return batch, indices
        else:
            raise Exception("Number of workers is not 0!")


def get_batch(old_data_loader, old_data_iter, new_data_iter):
    """
    Returns batch that consists of both old and new data.
    Epoch is considered as a cycle over new data.
    :param old_data_iter:
    :param new_data_iter:
    :return: batch = (img,label)

    Example of usage:
    old_data_loader, new_data_loader = ...
    old_data_iter = iter(old_data_loader)
    for _ in range(n_epochs):
        new_data_iter = iter(new_data_loader)
        while batch is not None:
            img = batch[0]
            label = batch[1]
            ...
    """
    new_data = next(new_data_iter, None)
    new_img = new_data[0]
    new_label = new_data[1]
    if new_data is None:
        return None  # signalizes end of an epoch
    old_data = next(old_data_iter, None)
    if old_data is None:
        old_data_iter = iter(old_data_loader)
        old_data = next(old_data_iter)
    old_img = old_data[0]
    old_label = old_data[1]
    img = torch.cat([old_img, new_img])
    label = torch.cat([old_label, new_label])
    return (img, label)