import numpy as np
import sys

import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

import collections
import time
# import cPickle as pickle
import pickle
from timeit import default_timer as timer
import matplotlib.mlab as mlab
from scipy.stats import norm


class Plotter():
    def __init__(self, output_path, start_iter=0):
        self._iter = [start_iter]
        self._since_beginning = collections.defaultdict(lambda: {})
        self._since_last_flush = collections.defaultdict(lambda: {})
        self.output_path = output_path

    def tick(self):
        self._iter[0] += 1

    def plot(self, name, value):
        self._since_last_flush[name][self._iter[0]] = value

    def flush(self, new_best=None, print_info=False):
        prints = []

        for name, vals in self._since_last_flush.items():
            prints.append("{} {:.3f}".format(name, np.mean(list(vals.values()))))
            self._since_beginning[name].update(vals)

            x_vals = np.sort(list(self._since_beginning[name].keys()))
            y_vals = [self._since_beginning[name][x] for x in x_vals]

            plt.clf()
            plt.plot(x_vals, y_vals)
            plt.xlabel('iteration')
            plt.ylabel(name)
            plt.savefig(self.output_path + name.replace(' ', '_') + '.jpg')

        if new_best is not None:
            prints.append(" new_best {}".format(new_best))

        if print_info: print("iter {}\t{}".format(self._iter[0], "; ".join(prints)))
        self._since_last_flush.clear()

        # with open('log.pkl', 'wb') as out_fname:
        #	pickle.dump(dict(_since_beginning), out_fname, pickle.HIGHEST_PROTOCOL)

    def compute_gan_statistics(self, D, loader_test, loader_train, device, path, iteration, cnn=None, file=None):
        num_classes = 1
        wds_test, means_test, wds_train, means_train \
            = self.wdist_test(iter(loader_test), device, D=D, cnn=cnn, n_test_iters=len(loader_test),
                              train_iter=iter(loader_train), n_train_iters=len(loader_train), disc_train=None,
                              info=False, num_classes=num_classes)
        self.plot_statistics(wds_train, means_train, wds_test, means_test, path, iteration,
                             mode='normal' if cnn is None else 'conv', file=file)

    def wdist_test(self, test_iter, device, D=None, cnn=None, n_test_iters=None, train_iter=None, n_train_iters=1,
                   info=False, disc_train=None, num_classes=1):
        wds_train = {key: [] for key in range(num_classes)}
        start_time = timer()
        if train_iter is not None:
            for i in range(n_train_iters):
                batch = next(train_iter)
                data = batch[0].float()
                data = data.to(device)
                if cnn is not None: data = cnn(data, conv_only=True)
                if len(batch) == 2: label = batch[1].cpu().data.numpy()
                if num_classes == 1: label = np.zeros(data.size(0))
                wd = self.wdist(data, D, disc_train)
                for w, l in zip(wd, label):
                    wds_train[l].append(w)
        means_train = {}
        for label in wds_train.keys():
            means_train[label] = np.mean(wds_train[label])
            if info:
                print('class {}:\n\tmean test disc: {}, var: {}, min: {}, max: {}, time: {}'
                      .format(label, means_train[label], np.var(wds_train[label]), np.min(wds_train[label]),
                              np.max(wds_train[label]), timer() - start_time))
        wds_test = {key: [] for key in range(num_classes)}  # []
        batch_n = 0
        while True:
            batch_n += 1
            batch = next(test_iter, None)
            if batch is None or n_test_iters is not None and n_test_iters < batch_n: break
            data = batch[0].float()
            data = data.to(device)
            if cnn is not None: data = cnn(data, conv_only=True)
            if len(batch) == 2: label = batch[1].cpu().data.numpy()
            if num_classes == 1: label = np.zeros(data.size(0))
            wd = self.wdist(data, D, disc_train)
            for w, l in zip(wd, label):
                wds_test[l].append(w)
        means_test = {}
        for label in wds_test.keys():
            means_test[label] = np.mean(wds_test[label])
            if info:
                print('class {}:\n\tmean test disc: {}, var: {}, min: {}, max: {}'
                      .format(label, means_test[label], np.var(wds_test[label]), np.min(wds_test[label]),
                              np.max(wds_test[label])))
                wds_top_2trainmean = np.abs(np.subtract(wds_test[label], np.mean(wds_train[label])))
                print('\tmean test disc 2 train mean: {}, var: {}, min: {}, max: {}'
                      .format(np.mean(wds_top_2trainmean), np.var(wds_top_2trainmean), np.min(wds_top_2trainmean),
                              np.max(wds_top_2trainmean)))
        return wds_test, means_test, wds_train, means_train

    def wdist(self, samples, D, cnn=None):
        if cnn is not None:
            samples = cnn(samples, conv_only=True)
        disc_sample = D(samples).data.cpu().numpy()
        # print(disc_train)
        wd = disc_sample
        # wd = np.abs(disc_sample - disc_train)
        return wd

    def plot_statistics(self, wds_train, wds_train_mean, wds_test, wds_test_mean, path, iteration, mode='normal',
                        file=None):
        # statistics of Wassertein distance on the BOTTOM set
        for label in wds_train.keys():
            wds_train_var = np.var(wds_train[label])
            wds_train_median = np.median(wds_train[label])
            wds_train_max, wds_train_min = np.max(wds_train[label]), np.min(wds_train[label])
            gauss_train_x = np.linspace(wds_train_mean[label] - 5 * wds_train_var,
                                        wds_train_mean[label] + 5 * wds_train_var, 100)
            gauss_train_y = mlab.normpdf(gauss_train_x, wds_train_mean[label], wds_train_var)
            dist_sigma_train = np.subtract(wds_train[label], wds_train_mean[label])
            dist_sigma_train = np.divide(dist_sigma_train, wds_train_var)
            dist_sigma_train_mu, dist_sigma_train_sigma = np.mean(dist_sigma_train), np.var(dist_sigma_train)
            gauss_train_x_dist = np.linspace(dist_sigma_train_mu - 5 * dist_sigma_train_sigma,
                                             dist_sigma_train_mu + 5 * dist_sigma_train_sigma, 100)
            gauss_train_y_dist = mlab.normpdf(gauss_train_x_dist, dist_sigma_train_mu, dist_sigma_train_sigma)

            # evaluate Wasserstein distance of the UPPER set
            wds_test_var = np.var(wds_test[label])
            wds_test_median = np.median(wds_test[label])
            wds_test_max, wds_test_min = np.max(wds_test[label]), np.min(wds_test[label])
            gauss_test_x = np.linspace(wds_test_mean[label] - 5 * wds_test_var, wds_test_mean[label] + 5 * wds_test_var,
                                       100)
            gauss_test_y = mlab.normpdf(gauss_test_x, wds_test_mean[label], wds_test_var)
            dist_sigma_test = np.subtract(wds_test[label], wds_train_mean[label])
            dist_sigma_test = np.divide(dist_sigma_test, wds_train_var)
            dist_sigma_test_mu, dist_sigma_test_sigma = np.mean(dist_sigma_test), np.var(dist_sigma_test)
            gauss_test_x_dist = np.linspace(dist_sigma_test_mu - 5 * dist_sigma_test_sigma,
                                            dist_sigma_test_mu + 5 * dist_sigma_test_sigma, 100)
            gauss_test_y_dist = mlab.normpdf(gauss_test_x_dist, dist_sigma_test_mu, dist_sigma_test_sigma)

            std_train, std_test = np.sqrt(wds_train_var), np.sqrt(wds_test_var)
            if file is not None:
                file.write('test: u={:.5f},sigma={:.5f}, median={:.5f}, min={:.5f}, max={:.5f}\n'
                           'train: u={:.5f},sigma={:.5f}, median={:.5f}, min={:.5f}, max={:.5f}\n'
                           'overlap of train in {}\n'.format(
                    wds_test_mean[label], wds_test_var, wds_test_median, wds_test_min, wds_test_max,
                    wds_train_mean[label], wds_train_var, wds_train_median, wds_train_min, wds_train_max,
                    self.intersect_area(self.find_itersect_points(m1=wds_train_mean[label], m2=wds_test_mean[label],
                                                                  std1=std_train, std2=std_test),
                                        mean_train=wds_train_mean[label], mean_test=wds_test_mean[label],
                                        std_train=std_train)))
                file.flush()
            print('test: u={:.5f},sigma={:.5f}, median={:.5f}, min={:.5f}, max={:.5f}\n'
                  'train: u={:.5f},sigma={:.5f}, median={:.5f}, min={:.5f}, max={:.5f}\n'
                  'overlap of train in {}'.format(
                wds_test_mean[label], wds_test_var, wds_test_median, wds_test_min, wds_test_max,
                wds_train_mean[label], wds_train_var, wds_train_median, wds_train_min, wds_train_max,
                self.intersect_area(self.find_itersect_points(m1=wds_train_mean[label], m2=wds_test_mean[label],
                                                              std1=std_train, std2=std_test),
                                    mean_train=wds_train_mean[label], mean_test=wds_test_mean[label],
                                    std_train=std_train)))

            plt.clf()
            plt.plot(dist_sigma_train, np.zeros(len(dist_sigma_train)), 'rx', label='train distances')
            plt.plot(gauss_train_x_dist, gauss_train_y_dist, 'r', label='train gauss')
            plt.plot(dist_sigma_test, np.zeros(len(dist_sigma_test)), 'bx', label='test distances')
            plt.plot(gauss_test_x_dist, gauss_test_y_dist, 'b', label='test gauss')
            plt.legend(loc='best')
            plt.title('distance to the mean value of train distribution measured in variances\nlabel: {}, iteration: {}'
                      .format(label, iteration))
            if len(wds_train.keys()) == 1:
                fname = '{}/experiment_gauss_variances_{}_{}.jpg'.format(path, mode, iteration)
                plt.savefig(fname)
                # print('saved fig to {}'.format(fname))
            else:
                fname = '{}/experiment_gauss_variances_{}_label_{}_.jpg'.format(path, mode, label, iteration)
                plt.savefig(fname)
                # print('saved fig to {}'.format(fname))

            plt.clf()
            plt.plot(wds_train[label], np.zeros(len(wds_train[label])), 'rx', label='train discriminator')
            plt.plot(gauss_train_x, gauss_train_y, 'r', label='train gauss')
            plt.plot(wds_test[label], np.zeros(len(wds_test[label])), 'bx', label='test discriminator')
            plt.plot(gauss_test_x, gauss_test_y, 'b', label='test gauss')
            plt.legend(loc='best')
            plt.title(
                'D output, label: {}, iteration: {}\ntrain: mu={:.3f}, sigma={:.3f}; test: mu={:.3f}, sigma={:.3f}'
                    .format(label, iteration, wds_train_mean[label], wds_train_var, wds_test_mean[label], wds_test_var))
            if len(wds_train.keys()) == 1:
                fname = '{}/experiment_gauss_discriminator-output_{}_{}.jpg'.format(path, mode, iteration)
                plt.savefig(fname)
                # print('saved fig to {}'.format(fname))
            else:
                fname = '{}/experiment_gauss_discriminator-output_{}_label_{}_{}.jpg'.format(path, mode, label,
                                                                                             iteration)
                plt.savefig(fname)
                # print('saved fig to {}'.format(fname))

    def intersect_area(self, intersections, mean_train, mean_test, std_train):
        intersections -= mean_train
        intersections /= std_train
        intersections = np.sort(intersections)
        if len(intersections) == 2:
            res = norm.cdf(intersections[0]) + 1 - norm.cdf(intersections[1])
        else:
            if mean_train > mean_test:
                res = norm.cdf(intersections[0])
            else:
                res = 1 - norm.cdf(intersections[0])
        return res * 100

    def find_itersect_points(self, m1, m2, std1, std2):
        a = 1 / (2 * std1 ** 2) - 1 / (2 * std2 ** 2)
        b = m2 / (std2 ** 2) - m1 / (std1 ** 2)
        c = m1 ** 2 / (2 * std1 ** 2) - m2 ** 2 / (2 * std2 ** 2) - np.log(std2 / std1)
        return np.roots([a, b, c])
