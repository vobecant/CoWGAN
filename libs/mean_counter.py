import numpy as np


class MeanVarCounter():
    def __init__(self, mean_init=0, var_init=0, sse_init=0, samples_used=0, num_classes=1, samples=None, labels=None):
        if (mean_init != 0 or var_init != 0 or sse_init != 0) and samples_used <= 0:
            raise Exception('Initial mean couldn\'t be computed from {} samples!'.format(samples_used))
        if samples_used < 0:
            raise Exception('samples_used must be a positive integer!')
        self.num_classes = num_classes
        # ROZDELIT NA TRIDY!!!!
        self.t = {key: 0 for key in range(num_classes)}
        self.mean = {key: 0 for key in range(num_classes)}
        self.var = {key: 0 for key in range(num_classes)}
        self.sse = {key: 0 for key in range(num_classes)}
        if samples is not None and labels is not None:
            self.add_samples(samples, labels)

    def add_samples(self, samples, labels=None):
        '''
        Inspired by https://stats.stackexchange.com/questions/72212/updating-variance-of-a-dataset
        :param sample:
        :return:
        '''
        if labels == None or self.num_classes == 1:
            labels = np.zeros(len(samples))
        for sample, label in zip(samples, labels):
            self.t[label] += 1
            e = sample - self.mean[label]
            self.mean[label] = self.mean[label] + e / self.t[label]
            self.sse[label] = self.sse[label] + e * (sample - self.mean[label])
            if self.t[label] > 1:
                self.var[label] = self.sse[label] / (self.t[label] - 1)
            else:
                self.var[label] = self.sse[label] / self.t[label]
        pass

    def get_mean(self):
        return self.mean

    def get_var(self):
        return self.var

    def get_t(self):
        return self.t
