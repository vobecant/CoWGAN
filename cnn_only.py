import os, sys

import numpy as np
import matplotlib

matplotlib.use('agg')
from matplotlib import pyplot as plt
from models.CNN import CNN
import torch
import torchvision
from torch import nn
from torch import autograd
from torch import optim
from torchvision import transforms, datasets
from torch.autograd import grad
from timeit import default_timer as timer
from models.dataset import Dataset2D
from libs.save import Saver
import libs.plot as plot
from mpl_toolkits.mplot3d.axes3d import Axes3D, get_test_data
from matplotlib import cm

# Device configuration
cuda_available = torch.cuda.is_available()
device = torch.device('cuda:0' if cuda_available else 'cpu')

n_classes = 2


class CNN(nn.Module):
    def __init__(self, n_classes, project_to=2, hidden_units=128):
        super(CNN, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Linear(2, hidden_units),
            nn.LeakyReLU()
        )
        self.layer2 = nn.Sequential(
            nn.Linear(hidden_units, hidden_units),
            nn.LeakyReLU()
        )
        self.projection = nn.Linear(hidden_units, project_to)
        self.leaky = nn.LeakyReLU()
        self.out = nn.Sequential(
            nn.Linear(project_to, n_classes),
            nn.Softmax()
        )

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        projection = self.projection(x)
        x = self.leaky(projection)
        out = self.out(x)
        return out, projection


def load_my_data(data_file, labels_file, batch_size):
    resize = transforms.Resize(64)
    crop = transforms.CenterCrop(64)
    to_tensor = transforms.ToTensor()
    dataset = Dataset2D(data_file, labels_file)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=5,
                                                 drop_last=True, pin_memory=True)
    # print('loaded dataset_iter, number of samples: {}'.format(len(dataset_iter)))
    return dataset_loader


x = torch.from_numpy(np.load('./datasets/blob_train_data.npy')).float()
xx = np.arange(-5, 5, 0.25)
yy = np.arange(-5, 5, 0.25)
xx, yy = np.meshgrid(xx, yy)
X, Y = xx, yy
xx = xx.reshape(-1)
yy = yy.reshape(-1)
xx_t = torch.from_numpy(xx).view(-1, 1)
yy_t = torch.from_numpy(yy).view(-1, 1)
xy = torch.cat([xx_t, yy_t], dim=1).float()
n = xx.shape[0]
cl0 = torch.from_numpy(np.zeros([n, n_classes], np.float32))
cl0[:, 0] = 1
cl1 = torch.from_numpy(np.zeros([n, n_classes], np.float32))
cl1[:, 1] = 1
cl = np.zeros(cl0.size(0))
cl[cl0.size(0):]=1


def print_distribution(net, epoch, folder):
    sc01 = net(xy)[0].data.numpy()
    sc0, sc1 = sc01[:, 0].reshape(X.shape), sc01[:, 1].reshape(X.shape)

    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax = fig.add_subplot(1, 2, 1, projection='3d')
    surf0 = ax.plot_surface(X, Y, sc0, rstride=1, cstride=1, cmap=cm.coolwarm)
    ax.set_title('class 0')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('disc')
    fig.colorbar(surf0, shrink=0.5, aspect=10)

    ax = fig.add_subplot(1, 2, 2, projection='3d')
    surf1 = ax.plot_surface(X, Y, sc1, rstride=1, cstride=1, cmap=cm.coolwarm)
    fig.colorbar(surf1, shrink=0.5, aspect=10)
    ax.set_title('class 1')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('disc')
    plt.savefig('{}/dist_surf_{:03d}.png'.format(folder, epoch))

    plt.close()
    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax = fig.add_subplot(1, 2, 1)
    CS = ax.contour(X, Y, sc0)
    ax.clabel(CS, inline=1, fontsize=10)
    ax.set_title('class 0')

    ax = fig.add_subplot(1, 2, 2)
    CS = ax.contour(X, Y, sc1)
    ax.clabel(CS, inline=1, fontsize=10)
    ax.set_title('class 1')
    plt.savefig('{}/distr_cont_{:03d}.png'.format(folder, epoch))


def print_projection(net, iteration, folder):
    projection = net(x)[1].cpu().data.numpy()
    plt.clf()
    plt.scatter(projection[:,0],projection[:,1])
    plt.title('Last FC layer, iteration {}'.format(iteration))
    plt.savefig('{}/lastFC_{}.png'.format(folder,iteration))

def train(net, loader, criterion, optimizer):
    train_losses = np.asarray([])
    total_train, correct_train = 0, 0
    net.train()
    for i, batch in enumerate(loader):
        # if i == 5: break
        # Forward pass
        images = batch[0].float()
        labels = torch.argmax(batch[1], dim=1).long()
        images = images.to(device)
        labels = labels.to(device)
        outputs, projections = net(images)
        _, predicted = torch.max(outputs.data, 1)

        # forward pass
        loss = criterion(outputs, labels)
        train_losses = np.append(train_losses, loss.item())

        # Backward and optimize
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        total_train += labels.size(0)
        correct_train += (predicted == labels).sum().item()

    train_acc = 100 * correct_train / total_train
    train_loss = np.mean(train_losses)
    return train_acc, train_loss


def validate(model, val_loader, criterion, show=False):
    # switch to evaluate mode
    model.eval()
    losses = np.asarray([])
    total, correct = 0, 0
    with torch.no_grad():
        for i, batch in enumerate(val_loader):
            input = batch[0]
            target = batch[1]
            input = input.to(device).float()
            target = torch.argmax(target.to(device), dim=1).long()
            # compute output
            output, projections = model(input)
            _, predicted = torch.max(output.data, 1)
            loss = criterion(output, target)
            losses = np.append(losses, loss.cpu().data.numpy())
            total += target.size(0)
            correct += (predicted == target).sum().item()
    val_loss = np.mean(losses)
    val_acc = 100 * correct / total
    return val_acc, val_loss


if __name__ == '__main__':
    net = CNN(2)
    data_file_train = './datasets/blob_train_data.npy'
    labels_file_train = './datasets/blob_train_labels.npy'
    data_file_val = './datasets/blob_val_data.npy'
    labels_file_val = './datasets/blob_val_labels.npy'
    folder = './result/CNN/data2D/'
    batch_size = 64
    n_epochs = 10
    train_loader = load_my_data(data_file_train, labels_file_train, batch_size)
    val_loader = load_my_data(data_file_val, labels_file_val, batch_size)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(net.parameters(), lr=1e-4)
    for epoch in range(n_epochs):
        ep_start = timer()
        acc_tr, loss_tr = train(net, train_loader, criterion, optimizer)
        acc_val, loss_val = validate(net, val_loader, criterion)
        print('Epoch [{}/{}], Train Loss: {:.6f}, Train Acc: {:.2f}, Val Loss: {:.6f}, Val Acc: {:.2f}, time: {:.3f}'
              .format(epoch + 1, n_epochs, loss_tr, acc_tr, loss_val, acc_val, timer() - ep_start))
        print_distribution(net, epoch, folder)
        print_projection(net, epoch, folder)
