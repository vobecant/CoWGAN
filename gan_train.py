import os, sys

sys.path.append(os.getcwd())

import time
import functools
import argparse

import numpy as np
# import sklearn.datasets

import libs as lib
import libs.plot
from tensorboardX import SummaryWriter

import pdb
# import gpustat

# import models.dcgan as dcgan
from models.wgan import *

import torch
import torchvision
from torch import nn
from torch import autograd
from torch import optim
from torchvision import transforms, datasets
from torch.autograd import grad
from timeit import default_timer as timer
from models.dataset import GeomDataset, GeomDataset_new, GeomDataset3d, MnistPairsDataset
from create_dataset import NUM_CLASSES
from libs.plot import Plotter

import torch.nn.init as init

DATA_SPECIF = 'black_circles_same-size'
DATA_DIR = './datasets/black_circles_same-size/train'
CSV_FILE_TRAIN = DATA_DIR + '/geom_data.csv'
VAL_DIR = './datasets/black_circles_same-size/val'
CSV_FILE_VAL = VAL_DIR + '/geom_data.csv'
cnn_file = './result/CNN/mnist/cnn_last.pt'

IMAGE_DATA_SET = 'circles-2D'  # change this to something else, e.g. 'imagenets' or 'raw' if your dataset_iter is just a folder of raw images.
# If you use lmdb, you'll need to write the loader by yourself, see load_data
# TRAINING_CLASS = ['dining_room_train', 'bridge_train', 'restaurant_train', 'tower_train']
# VAL_CLASS = ['dining_room_val', 'bridge_val', 'restaurant_val', 'tower_val']

if len(DATA_DIR) == 0:
    raise Exception('Please specify path to dataset_iter directory in gan_64x64.py!')

RESTORE_MODE = False  # if True, it will load saved cnn from OUT_PATH and continue to train
print_timing = False
START_ITER = 0  # starting iteration
OUTPUT_PATH = './result/WGAN/conv2mnist/'  # output path where result (.e.g drawing images, cost, chart) will be stored
MODE = 'wgan-gp'  # dcgan, wgan
DIM = 64  # Model dimensionality
CRITIC_ITERS = 5  # How many iterations to train the critic for
GENER_ITERS = 1
N_GPUS = 1  # Number of GPUs
BATCH_SIZE = 64  # Batch size. Must be a multiple of N_GPUS
END_ITER = 100000  # How many iterations to train for
LAMBDA = 10  # Gradient penalty lambda hyperparameter
LAMBDA_CNN = 10
OUTPUT_DIM = 64 * 64 * 3  # Number of pixels in each image
VAL_FREQ = 10


def showMemoryUsage(device=1):
    gpu_stats = gpustat.GPUStatCollection.new_query()
    item = gpu_stats.jsonify()["gpus"][device]
    print('Used/total: ' + "{}/{}".format(item["memory.used"], item["memory.total"]))


def weights_init(m):
    if isinstance(m, MyConvo2d):
        if m.conv.weight is not None:
            if m.he_init:
                init.kaiming_uniform_(m.conv.weight)
            else:
                init.xavier_uniform_(m.conv.weight)
        if m.conv.bias is not None:
            init.constant_(m.conv.bias, 0.0)
    if isinstance(m, nn.Linear):
        if m.weight is not None:
            init.xavier_uniform_(m.weight)
        if m.bias is not None:
            init.constant_(m.bias, 0.0)


def load_my_data(path_to_folder, csvfile, batch_size=BATCH_SIZE, shuffle=True, drop_last=True, is3d=False):
    data_transform = transforms.Compose([
        transforms.Resize(64),
        transforms.CenterCrop(64),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
    ])
    if is3d:
        dataset = GeomDataset3d(csvfile, path_to_folder, data_transform)
    else:
        dataset = GeomDataset_new(csvfile, path_to_folder, data_transform)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=shuffle, num_workers=5,
                                                 drop_last=drop_last, pin_memory=True)
    return dataset_loader


def training_data_loader():
    return load_my_data(DATA_DIR, CSV_FILE_TRAIN)


def data_loader_pairs(folder='train', batch_size=BATCH_SIZE, shuffle=True, drop_last=True):
    dataset = MnistPairsDataset('./datasets/mnist_codes_{}.npy'.format(folder))
    loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=shuffle, num_workers=5,
                                         drop_last=drop_last, pin_memory=True)
    return loader


def mnist_loader(batch_size=BATCH_SIZE, shuffle=True, drop_last=True):
    loader = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=True, download=True,
                       transform=transforms.Compose([
                           transforms.Resize(32),
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=batch_size, shuffle=shuffle, num_workers=5, pin_memory=True, drop_last=drop_last)
    return loader


def val_data_loader():
    return load_my_data(VAL_DIR, CSV_FILE_VAL)


def calc_gradient_penalty(netD, real_data, fake_data, img_size=64, img_dim=3):
    alpha = torch.rand(BATCH_SIZE, 1)
    alpha = alpha.expand(BATCH_SIZE, int(real_data.nelement() / BATCH_SIZE)).contiguous()
    alpha = alpha.view(BATCH_SIZE, img_dim, img_size, img_size)
    alpha = alpha.to(device)

    fake_data = fake_data.view(BATCH_SIZE, img_dim, img_size, img_size)
    interpolates = alpha * real_data.detach() + ((1 - alpha) * fake_data.detach())

    interpolates = interpolates.to(device)
    interpolates.requires_grad_(True)

    disc_interpolates = netD(interpolates)

    gradients = autograd.grad(outputs=disc_interpolates, inputs=interpolates,
                              grad_outputs=torch.ones(disc_interpolates.size()).to(device),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]

    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean() * LAMBDA
    return gradient_penalty


def generate_image(netG, noise=None, img_dim=3, img_side=64):
    if noise is None:
        noise = gen_rand_noise()

    with torch.no_grad():
        noisev = noise
    samples = netG(noisev)
    samples = samples.view(noise.size(0), img_dim, img_side, img_side)
    samples = samples * 0.5 + 0.5
    return samples


OLDGAN = False


def gen_rand_noise(specif_input=False):
    if specif_input:
        dataset = data_loader_pairs().dataset
        codes_dset = dataset.codes
        mnist_images = dataset.mnist.train_data
        mnist_labels = dataset.mnist.train_labels
        idx = []

        for label in range(10):
            label_idx = np.where(np.equal(label, mnist_labels))[0]
            tmp_idx = np.random.randint(0, len(label_idx), 10)
            idx.extend(label_idx[tmp_idx])
        idx = np.asarray(idx)
        # print('idx shape: {}'.format(idx.shape))
        noise = codes_dset[idx]
        imgs = mnist_images[idx].unsqueeze(1)
        torchvision.utils.save_image(imgs, OUTPUT_PATH + 'real_samples.png', nrow=10, padding=2)
    elif OLDGAN:
        noise = torch.FloatTensor(BATCH_SIZE, 128, 1, 1)
        noise.resize_(BATCH_SIZE, 128, 1, 1).normal_(0, 1)
    else:
        noise = torch.randn(BATCH_SIZE, 128)
    noise = torch.Tensor(noise)
    noise = noise.to(device)

    return noise


cuda_available = torch.cuda.is_available()
device = torch.device("cuda" if cuda_available else "cpu")
print('Used device: {}'.format(device))
fixed_noise = gen_rand_noise(specif_input=True)

if RESTORE_MODE:
    aG = torch.load(OUTPUT_PATH + "generator.pt")
    aD = torch.load(OUTPUT_PATH + "discriminator.pt")
    print('restored learned WEIGHTS, START_ITER={}'.format(START_ITER))
else:
    if MODE == 'wgan-gp':
        aG = GoodGeneratorMnist(8192, dim=32, image_dim=1, image_side=32)
        aD = GoodDiscriminator(dim=32, image_dim=1, image_side=32)
        OLDGAN = False
    elif MODE == 'dcgan':
        aG = FCGenerator()
        aD = DCGANDiscriminator()
        OLDGAN = False
    else:
        aG = dcgan.DCGAN_G(DIM, 128, 3, 64, 1, 0)
        aD = dcgan.DCGAN_D(DIM, 128, 3, 64, 1, 0)
        OLDGAN = True

    aG.apply(weights_init)
    aD.apply(weights_init)
cnn = torch.load(cnn_file).to(device)

LR = 1e-4
optimizer_g = torch.optim.Adam(aG.parameters(), lr=LR, betas=(0, 0.9))
optimizer_d = torch.optim.Adam(aD.parameters(), lr=LR, betas=(0, 0.9))
one = torch.FloatTensor([1])
mone = one * -1
aG = aG.to(device)
aD = aD.to(device)
one = one.to(device)
mone = mone.to(device)

writer = SummaryWriter()


# Reference: https://github.com/caogang/wgan-gp/blob/master/gan_cifar10.py
def train():
    start_time = timer()
    dataloader_crit = data_loader_pairs()
    dataloader_gen = data_loader_pairs()
    dataiter_crit = iter(dataloader_crit)
    dataiter_gen = iter(dataloader_gen)
    plotter = Plotter(OUTPUT_PATH)
    l1_loss = nn.L1Loss(reduction='sum')
    cnn_loss = nn.NLLLoss()
    if print_timing: print('Iterators initialized in {}'.format(timer() - start_time))
    for iteration in range(START_ITER, END_ITER):
        start_time_iter = timer()
        if print_timing: print("Iter: " + str(iteration))
        start = timer()
        # ---------------------TRAIN G------------------------
        for p in aD.parameters():
            p.requires_grad_(False)  # freeze D

        gen_cost = None
        for i in range(GENER_ITERS):
            if print_timing: print("Generator iters: " + str(i))
            aG.zero_grad()
            batch = next(dataiter_gen, None)
            if batch is None:
                dataiter_gen = iter(dataloader_gen)
                batch = next(dataiter_gen)
            gen_input = batch[0].to(device)
            gen_input.requires_grad_(True)
            fake_data = aG(gen_input)
            real_data = batch[1].to(device)
            real_label = batch[2].to(device)
            gen_cost = 0  # aD(fake_data).mean().view(-1)
            real_detached, fake_detached = real_data.detach(), fake_data.view(-1, 1, 32, 32).detach()
            # reconstruction_cost = l1_loss(real_detached, fake_detached)
            cnn_output = cnn(fake_detached)
            cnn_cost = cnn_loss(cnn_output, real_label) * LAMBDA_CNN
            # gen_cost = gen_cost.mean().view(-1) + cnn_cost  # reconstruction_cost
            # gen_cost.backward(mone)
            # gen_cost = -gen_cost
            gen_cost = -gen_cost + cnn_cost  # reconstruction_cost
            gen_cost.backward()

        optimizer_g.step()
        end = timer()
        if print_timing: print(f'---train G elapsed time: {end - start}')
        # ---------------------TRAIN D------------------------
        for p in aD.parameters():  # reset requires_grad
            p.requires_grad_(True)  # they are set to False below in training G
        for i in range(CRITIC_ITERS):
            if print_timing: print("Critic iter: " + str(i))

            start = timer()
            aD.zero_grad()

            batch = next(dataiter_crit, None)
            if batch is None:
                dataiter_crit = iter(dataloader_crit)
                batch = next(dataiter_crit)
            gen_input = batch[0].to(device)
            real_data = batch[1].to(device)

            # gen fake dataset_iter and load real dataset_iter
            with torch.no_grad():
                noisev = gen_input  # totally freeze G, training D
            fake_data = aG(noisev).detach()

            # train with real dataset_iter
            disc_real = aD(real_data)
            disc_real = disc_real.mean()

            # train with fake dataset_iter
            disc_fake = aD(fake_data)
            disc_fake = disc_fake.mean()

            # showMemoryUsage(0)
            # train with interpolates dataset_iter
            gradient_penalty = calc_gradient_penalty(aD, real_data, fake_data, img_dim=1, img_size=32)
            # showMemoryUsage(0)

            # final disc cost
            disc_cost = disc_fake - disc_real + gradient_penalty
            disc_cost.backward()
            w_dist = disc_fake - disc_real
            optimizer_d.step()

        # ------------------VISUALIZATION----------
        '''
        writer.add_scalar('dataset_iter/disc_cost', disc_cost, iteration)
        writer.add_scalar('dataset_iter/gradient_pen', gradient_penalty, iteration)
        if iteration % VAL_FREQ == (VAL_FREQ - 1):
            body_model = [i for i in aD.children()][0]
            layer1 = body_model.conv
            xyz = layer1.weight.data.clone()
            tensor = xyz.cpu()
            tensors = torchvision.utils.make_grid(tensor, nrow=8, padding=1)
            writer.add_image('D/conv1', tensors, iteration)
        '''
        end = timer();
        if print_timing: print(f'---train D elapsed time: {end-start}')

        # ---------------VISUALIZATION---------------------
        writer.add_scalar('dataset_iter/gen_cost', gen_cost, iteration)

        plotter.plot('time', timer() - start_time_iter)
        plotter.plot('train_disc_cost', disc_cost.cpu().data.numpy())
        plotter.plot('train_gen_cost', gen_cost.cpu().data.numpy())
        plotter.plot('wasserstein_distance', w_dist.cpu().data.numpy())
        plotter.plot('GP', gradient_penalty.cpu().data.numpy())
        # plotter.plot('reconstruction', reconstruction_cost.cpu().data.numpy())
        plotter.plot('cnn_cost', cnn_cost.cpu().data.numpy())
        if iteration % VAL_FREQ == (VAL_FREQ - 1):
            val_start = timer()
            do_val = False
            if do_val:
                val_loader = val_data_loader()
                dev_disc_costs = []
                for _, images in enumerate(val_loader):
                    imgs = torch.Tensor(images[0])
                    imgs = imgs.to(device)
                    with torch.no_grad():
                        imgs_v = imgs

                    D = aD(imgs_v)
                    _dev_disc_cost = -D.mean().cpu().data.numpy()
                    dev_disc_costs.append(_dev_disc_cost)
                plotter.plot('dev_disc_cost.png', np.mean(dev_disc_costs))
            plotter.flush(print_info=True)
            gen_images = generate_image(aG, fixed_noise, img_dim=1, img_side=32)
            n_images = gen_images.size(0)
            n_rows = int(np.sqrt(n_images))
            torchvision.utils.save_image(gen_images, OUTPUT_PATH + 'samples_{}.png'.format(iteration), nrow=n_rows,
                                         padding=2)
            grid_images = torchvision.utils.make_grid(gen_images, nrow=8, padding=2)
            writer.add_image('images', grid_images, iteration)
            # ----------------------Save cnn----------------------
            torch.save(aG, OUTPUT_PATH + "generator.pt")
            torch.save(aD, OUTPUT_PATH + "discriminator.pt")
            if print_timing: print('Evaluation took {}'.format(timer() - val_start))
        plotter.tick()


if __name__ == '__main__':
    train()
