import matplotlib

matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
from numpy.random import choice
import csv
from torchvision import transforms, datasets
import torch
from models.dataset import GeomDataset
import pickle
from models.CNN import CNN as Model

DATA_SPECIF = 'black_circles_same-size'
# grayscale = True
DATA_DIR = './datasets/' + DATA_SPECIF + '/train'
CSV_FILE_TRAIN = DATA_DIR + '/geom_data.csv'
# OUT_DIR = './datasets/limit-center-up_rg_conv'
base_folder = './datasets/' + DATA_SPECIF
file_path_train = base_folder + '/train'
file_path_val = base_folder + '/val'
n_obj_train = [1000, 0, 0]
n_obj_val = [100, 0, 0]
label_type = 'basic'
BATCH_SIZE = 64
# Device configuration
cuda_available = False  # torch.cuda.is_available()
device = torch.device('cuda:0' if cuda_available else 'cpu')

LABELS_BASIC = {('circ'): 0, ('rect'): 1, ('tri'): 2}

LABELS_COLOR = {('circ', 'r'): 0, ('circ', 'g'): 1, ('circ', 'b'): 2,
                ('rect', 'r'): 3, ('rect', 'g'): 4, ('rect', 'b'): 5,
                ('tri', 'r'): 6, ('tri', 'g'): 7, ('tri', 'b'): 8}

OBJECTS = ['rect']

COLORS = ['k']  # ['r', 'g']
CENTER_X = [-4, 4]
CENTER_Y = [-4, 4]
CENTER_Y_BOTTOM = [-3, 0]
CENTER_Y_TOP = [0, 3]
TRI_SCALE = [2, 3]
CIRC_R = [1, 1]
RECT_W = [2, 4]
IMRANGE = [-5, 5]
DPI = 120

LABELS_USED = LABELS_BASIC if label_type == 'basic' else LABELS_COLOR
NUM_CLASSES = 10


def get_label(obj, color):
    if label_type == 'basic':
        return LABELS_USED[obj]
    else:
        return LABELS_USED[(obj, color)]


def get_num_classes(): return NUM_CLASSES


def compute_center(center, width, height):
    x = center[0] - width / 2
    y = center[1] - height / 2
    return (x, y)


def make_circle(center, radius, color, idx, file_path, dpi, fw, size=480, fw2=None):
    plt.figure(figsize=(size / dpi, size / dpi), dpi=dpi)
    circle = plt.Circle(center, radius=radius, fc=color)
    plt.gca().add_patch(circle)
    # plt.set_cmap('gray')
    plt.axis('scaled')
    plt.axis('off')
    name = '{}_circ'.format(idx)
    to_write = ['{}.png'.format(name), get_label('circ', color), color, [center[0], center[1], radius]]
    save_object(name, file_path, fw, to_write, fw2=fw2)


def make_rectangle(center, width, height, color, angle, idx, file_path, dpi, fw, size=480, fw2=None):
    plt.figure(figsize=(size / dpi, size / dpi), dpi=dpi)
    center = compute_center(center, width, height)
    rectangle = plt.Rectangle(center, width, height, angle, fc=color)
    plt.gca().add_patch(rectangle)
    plt.axis('scaled')
    plt.axis('off')
    name = '{}_rect'.format(idx)
    to_write = ['{}.png'.format(name), get_label('rect', color), color, [center[0], center[1], width, height, angle]]
    save_object(name, file_path, fw, to_write, fw2=fw2)


def make_triangle(corners, angle, color, idx, file_path, dpi, fw, size=480, fw2=None):
    plt.figure(figsize=(size / dpi, size / dpi), dpi=dpi)
    trianagle = plt.Polygon(corners, color=color)
    plt.gca().add_patch(trianagle)
    plt.axis('scaled')
    plt.axis('off')
    name = '{}_tri'.format(idx)
    to_write = ['{}.png'.format(name), get_label('tri', color), color, [corners, angle]]
    save_object(name, file_path, fw, to_write, fw2=fw2)


def save_object(name, file_path, fw, to_write, fw2=None):
    plt.xlim(IMRANGE[0], IMRANGE[1])
    plt.ylim(IMRANGE[0], IMRANGE[1])
    pth = "{}/{}".format(file_path, name)
    plt.savefig(pth)
    plt.close()
    fw.writerow(to_write)
    if fw2 is not None:
        fw2.writerow(to_write)


def rotate(pt, center, angle):
    x, y = pt - center
    new_x = np.cos(angle) * x - np.sin(angle) * y
    new_y = np.sin(angle) * x + np.cos(angle) * y
    return [new_x, new_y] + center


def rand_right_triangle(scale, pt1):
    angle = np.random.uniform(0, np.pi)
    # angle = np.pi/2
    pt1 = pt1
    pt2 = pt1 + [scale, 0]
    pt3 = pt1 + [0, scale]
    pt2 = rotate(pt2, pt1, angle)
    pt3 = rotate(pt3, pt1, angle)
    return [pt1, pt2, pt3], angle


def create_train():
    with open(file_path_train + '/geom_data.csv', 'w') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',')
        dpi = 120
        size = 64
        n_circle = n_obj_train[0]
        n_rect = n_obj_train[1]
        n_trainagle = n_obj_train[2]
        colors = COLORS
        circ_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_circle, 1))
        circ_cy = np.random.uniform(CENTER_Y[0], CENTER_Y[1], (n_circle, 1))
        circ_c = np.append(circ_cx, circ_cy, axis=1)
        circ_r = np.random.uniform(CIRC_R[0], CIRC_R[1], n_circle)
        rect_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_rect, 1))
        rect_cy = np.random.uniform(CENTER_Y[0], CENTER_Y[0], (n_rect, 1))
        rect_c = np.append(rect_cx, rect_cy, axis=1)
        rect_w = np.random.uniform(RECT_W[0], RECT_W[1], n_rect)
        tri_pt1x = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_trainagle, 1))
        tri_pt1y = np.random.uniform(np.max([CENTER_Y[0], IMRANGE[0] + TRI_SCALE[1]]),
                                     np.min([CENTER_Y[1], IMRANGE[1] - TRI_SCALE[1]]), (n_trainagle, 1))
        tri_pt1 = np.append(tri_pt1x, tri_pt1y, axis=1)
        tri_scale = np.random.uniform(TRI_SCALE[0], TRI_SCALE[1], n_trainagle)
        for idx in range(0, n_circle):
            make_circle(circ_c[idx, :], circ_r[idx], choice(colors, 1)[0], idx, file_path_train, dpi, filewriter, size)
        print("{} test circles done".format(n_circle))
        ofset = n_circle
        for idx in range(ofset, n_circle + n_rect):
            make_rectangle(rect_c[idx - ofset, :], rect_w[idx - ofset], rect_w[idx - ofset], choice(colors, 1)[0], 0.0,
                           idx, file_path_train, dpi, filewriter, size)
        print("{} test rectangles done".format(n_rect))
        ofset += n_rect
        for idx in range(n_circle + n_rect, n_circle + n_rect + n_trainagle):
            corners, angle = rand_right_triangle(tri_scale[idx - ofset], pt1=tri_pt1[idx - ofset, :])
            make_triangle(corners, angle, choice(colors, 1)[0], idx, file_path_train, dpi, filewriter, size)
        print("{} test triangles done".format(n_trainagle))


def create_val():
    with open(file_path_val + '/geom_data.csv', 'w') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',')
        dpi = 120
        size = 64
        n_circle = n_obj_val[0]
        n_rect = n_obj_val[1]
        n_trainagle = n_obj_val[2]
        colors = COLORS
        circ_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_circle, 1))
        circ_cy = np.random.uniform(CENTER_Y[0], CENTER_Y[1], (n_circle, 1))
        circ_c = np.append(circ_cx, circ_cy, axis=1)
        circ_r = np.random.uniform(CIRC_R[0], CIRC_R[1], n_circle)
        rect_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_rect, 1))
        rect_cy = np.random.uniform(CENTER_Y[0], CENTER_Y[0], (n_rect, 1))
        rect_c = np.append(rect_cx, rect_cy, axis=1)
        rect_w = np.random.uniform(RECT_W[0], RECT_W[1], n_rect)
        tri_pt1x = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_trainagle, 1))
        tri_pt1y = np.random.uniform(np.max([CENTER_Y[0], IMRANGE[0] + TRI_SCALE[1]]),
                                     np.min([CENTER_Y[1], IMRANGE[1] - TRI_SCALE[1]]), (n_trainagle, 1))
        tri_pt1 = np.append(tri_pt1x, tri_pt1y, axis=1)
        tri_scale = np.random.uniform(TRI_SCALE[0], TRI_SCALE[1], n_trainagle)
        for idx in range(0, n_circle):
            make_circle(circ_c[idx, :], circ_r[idx], choice(colors, 1)[0], idx, file_path_val, dpi, filewriter, size)
        print("{} validation circles done".format(n_circle))
        ofset = n_circle
        for idx in range(ofset, n_circle + n_rect):
            make_rectangle(rect_c[idx - ofset, :], rect_w[idx - ofset], rect_w[idx - ofset], choice(colors, 1)[0], 0.0,
                           idx, file_path_val, dpi, filewriter, size)
        print("{} validation rectangles done".format(n_rect))
        ofset += n_rect
        for idx in range(n_circle + n_rect, n_circle + n_rect + n_trainagle):
            corners, angle = rand_right_triangle(tri_scale[idx - ofset], pt1=tri_pt1[idx - ofset, :])
            make_triangle(corners, angle, choice(colors, 1)[0], idx, file_path_val, dpi, filewriter, size)
        print("{} validation triangles done".format(n_trainagle))


def create_train_split():
    with open(file_path_train + '/geom_data_bottom.csv', 'w') as csv_bottom:
        with open(file_path_train + '/geom_data_all.csv', 'w') as csv_all:
            fw_all = csv.writer(csv_all, delimiter=',')
            fw_bottom = csv.writer(csv_bottom, delimiter=',')
            dpi = 120
            size = 64
            n_circle = n_obj_train[0]
            n_rect = n_obj_train[1]
            n_trainagle = n_obj_train[2]
            colors = COLORS
            circ_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_circle, 1))
            circ_cy = np.random.uniform(CENTER_Y_BOTTOM[0], CENTER_Y_BOTTOM[1], (n_circle, 1))
            circ_c = np.append(circ_cx, circ_cy, axis=1)
            circ_r = np.random.uniform(CIRC_R[0], CIRC_R[1], n_circle)
            rect_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_rect, 1))
            rect_cy = np.random.uniform(CENTER_Y_BOTTOM[0], CENTER_Y_BOTTOM[1], (n_rect, 1))
            rect_c = np.append(rect_cx, rect_cy, axis=1)
            rect_w = np.random.uniform(RECT_W[0], RECT_W[1], n_rect)
            tri_pt1x = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_trainagle, 1))
            tri_pt1y = np.random.uniform(np.max([CENTER_Y_BOTTOM[0], IMRANGE[0] + TRI_SCALE[1]]),
                                         np.min([CENTER_Y_BOTTOM[1], IMRANGE[1] - TRI_SCALE[1]]), (n_trainagle, 1))
            tri_pt1 = np.append(tri_pt1x, tri_pt1y, axis=1)
            tri_scale = np.random.uniform(TRI_SCALE[0], TRI_SCALE[1], n_trainagle)
            for idx in range(0, n_circle):
                make_circle(circ_c[idx, :], circ_r[idx], choice(colors, 1)[0], idx, file_path_train, dpi, fw_bottom,
                            size, fw2=fw_all)
                if idx % 1000 == 0: print(idx)
            print("{} test circles done".format(n_circle))
            ofset = n_circle
            for idx in range(ofset, n_circle + n_rect):
                make_rectangle(rect_c[idx - ofset, :], rect_w[idx - ofset], rect_w[idx - ofset], choice(colors, 1)[0],
                               0.0, idx, file_path_train, dpi, fw_bottom, size, fw2=fw_all)
                if idx % 1000 == 0: print(idx)
            print("{} test rectangles done".format(n_rect))
            ofset += n_rect
            for idx in range(n_circle + n_rect, n_circle + n_rect + n_trainagle):
                corners, angle = rand_right_triangle(tri_scale[idx - ofset], pt1=tri_pt1[idx - ofset, :])
                make_triangle(corners, angle, choice(colors, 1)[0], idx, file_path_train, dpi, fw_bottom, size,
                              fw2=fw_all)
                if idx % 1000 == 0: print(idx)
            ofset += n_trainagle
            print("{} test triangles done".format(n_trainagle))

            # upper part save only to fw_all
            dpi = 120
            size = 64
            n_circle = n_obj_train[0]
            n_rect = n_obj_train[1]
            n_trainagle = n_obj_train[2]
            colors = COLORS
            circ_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_circle, 1))
            circ_cy = np.random.uniform(CENTER_Y_TOP[0], CENTER_Y_TOP[1], (n_circle, 1))
            circ_c = np.append(circ_cx, circ_cy, axis=1)
            circ_r = np.random.uniform(CIRC_R[0], CIRC_R[1], n_circle)
            rect_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_rect, 1))
            rect_cy = np.random.uniform(CENTER_Y_TOP[0], CENTER_Y_TOP[0], (n_rect, 1))
            rect_c = np.append(rect_cx, rect_cy, axis=1)
            rect_w = np.random.uniform(RECT_W[0], RECT_W[1], n_rect)
            tri_pt1x = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_trainagle, 1))
            tri_pt1y = np.random.uniform(np.max([CENTER_Y_TOP[0], IMRANGE[0] + TRI_SCALE[1]]),
                                         np.min([CENTER_Y_TOP[1], IMRANGE[1] - TRI_SCALE[1]]), (n_trainagle, 1))
            tri_pt1 = np.append(tri_pt1x, tri_pt1y, axis=1)
            tri_scale = np.random.uniform(TRI_SCALE[0], TRI_SCALE[1], n_trainagle)
            for idx in range(ofset, ofset + n_circle):
                make_circle(circ_c[idx - ofset, :], circ_r[idx - ofset], choice(colors, 1)[0], idx, file_path_train,
                            dpi, fw_all, size)
                if idx % 1000 == 0: print(idx)
            print("{} test circles done".format(n_circle))
            ofset += n_circle
            for idx in range(ofset, ofset + n_rect):
                make_rectangle(rect_c[idx - ofset, :], rect_w[idx - ofset], rect_w[idx - ofset], choice(colors, 1)[0],
                               0.0, idx, file_path_train, dpi, fw_all, size)
                if idx % 1000 == 0: print(idx)
            print("{} test rectangles done".format(n_rect))
            ofset += n_rect
            for idx in range(ofset, ofset + n_trainagle):
                corners, angle = rand_right_triangle(tri_scale[idx - ofset], pt1=tri_pt1[idx - ofset, :])
                make_triangle(corners, angle, choice(colors, 1)[0], idx, file_path_train, dpi, fw_all, size)
                if idx % 1000 == 0: print(idx)
            print("{} test triangles done".format(n_trainagle))


def create_val_split():
    with open(file_path_val + '/geom_data_bottom.csv', 'w') as csv_bottom:
        with open(file_path_val + '/geom_data_all.csv', 'w') as csv_all:
            fw_all = csv.writer(csv_all, delimiter=',')
            fw_bottom = csv.writer(csv_bottom, delimiter=',')
            dpi = 120
            size = 64
            n_circle = n_obj_val[0]
            n_rect = n_obj_val[1]
            n_triangle = n_obj_val[2]
            colors = COLORS
            circ_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_circle, 1))
            circ_cy = np.random.uniform(CENTER_Y_BOTTOM[0], CENTER_Y_BOTTOM[1], (n_circle, 1))
            circ_c = np.append(circ_cx, circ_cy, axis=1)
            circ_r = np.random.uniform(CIRC_R[0], CIRC_R[1], n_circle)
            rect_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_rect, 1))
            rect_cy = np.random.uniform(CENTER_Y_BOTTOM[0], CENTER_Y_BOTTOM[1], (n_rect, 1))
            rect_c = np.append(rect_cx, rect_cy, axis=1)
            rect_w = np.random.uniform(RECT_W[0], RECT_W[1], n_rect)
            tri_pt1x = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_triangle, 1))
            tri_pt1y = np.random.uniform(np.max([CENTER_Y_BOTTOM[0], IMRANGE[0] + TRI_SCALE[1]]),
                                         np.min([CENTER_Y_BOTTOM[1], IMRANGE[1] - TRI_SCALE[1]]), (n_triangle, 1))
            tri_pt1 = np.append(tri_pt1x, tri_pt1y, axis=1)
            tri_scale = np.random.uniform(TRI_SCALE[0], TRI_SCALE[1], n_triangle)
            for idx in range(0, n_circle):
                make_circle(circ_c[idx, :], circ_r[idx], choice(colors, 1)[0], idx, file_path_val, dpi, fw_bottom,
                            size, fw2=fw_all)
            print("{} val circles done".format(n_circle))
            ofset = n_circle
            for idx in range(ofset, n_circle + n_rect):
                make_rectangle(rect_c[idx - ofset, :], rect_w[idx - ofset], rect_w[idx - ofset], choice(colors, 1)[0],
                               0.0, idx, file_path_val, dpi, fw_bottom, size, fw2=fw_all)
            print("{} val rectangles done".format(n_rect))
            ofset += n_rect
            for idx in range(n_circle + n_rect, n_circle + n_rect + n_triangle):
                corners, angle = rand_right_triangle(tri_scale[idx - ofset], pt1=tri_pt1[idx - ofset, :])
                make_triangle(corners, angle, choice(colors, 1)[0], idx, file_path_val, dpi, fw_bottom, size,
                              fw2=fw_all)
            ofset += n_triangle
            print("{} val triangles done".format(n_triangle))

            # upper part save only to fw_all
            dpi = 120
            size = 64
            n_circle = n_obj_val[0]
            n_rect = n_obj_val[1]
            n_triangle = n_obj_val[2]
            colors = COLORS
            circ_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_circle, 1))
            circ_cy = np.random.uniform(CENTER_Y_TOP[0], CENTER_Y_TOP[1], (n_circle, 1))
            circ_c = np.append(circ_cx, circ_cy, axis=1)
            circ_r = np.random.uniform(CIRC_R[0], CIRC_R[1], n_circle)
            rect_cx = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_rect, 1))
            rect_cy = np.random.uniform(CENTER_Y_TOP[0], CENTER_Y_TOP[0], (n_rect, 1))
            rect_c = np.append(rect_cx, rect_cy, axis=1)
            rect_w = np.random.uniform(RECT_W[0], RECT_W[1], n_rect)
            tri_pt1x = np.random.uniform(CENTER_X[0], CENTER_X[1], (n_triangle, 1))
            tri_pt1y = np.random.uniform(np.max([CENTER_Y_TOP[0], IMRANGE[0] + TRI_SCALE[1]]),
                                         np.min([CENTER_Y_TOP[1], IMRANGE[1] - TRI_SCALE[1]]), (n_triangle, 1))
            tri_pt1 = np.append(tri_pt1x, tri_pt1y, axis=1)
            tri_scale = np.random.uniform(TRI_SCALE[0], TRI_SCALE[1], n_triangle)
            for idx in range(ofset, ofset + n_circle):
                make_circle(circ_c[idx - ofset, :], circ_r[idx - ofset], choice(colors, 1)[0], idx, file_path_val, dpi,
                            fw_all, size)
            print("{} val circles done".format(n_circle))
            ofset += n_circle
            for idx in range(ofset, ofset + n_rect):
                make_rectangle(rect_c[idx - ofset, :], rect_w[idx - ofset], rect_w[idx - ofset], choice(colors, 1)[0],
                               0.0, idx, file_path_val, dpi, fw_all, size)
            print("{} val rectangles done".format(n_rect))
            ofset += n_rect
            for idx in range(ofset, ofset + n_triangle):
                corners, angle = rand_right_triangle(tri_scale[idx - ofset], pt1=tri_pt1[idx - ofset, :])
                make_triangle(corners, angle, choice(colors, 1)[0], idx, file_path_val, dpi, fw_all, size)
            print("{} val triangles done".format(n_triangle))


def load_my_data(path_to_folder, csvfile, drop_last=True):
    resize = transforms.Resize(64)
    crop = transforms.CenterCrop(64)
    to_tensor = transforms.ToTensor()
    dataset = GeomDataset(csvfile, path_to_folder, resize, crop, to_tensor)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True, num_workers=5,
                                                 drop_last=drop_last, pin_memory=True)
    print('loaded dataset_iter, number of samples: {}'.format(len(dataset)))
    return dataset_loader


def create_conv(data_loader, device, model, output_file):
    conv_out = np.asarray([])
    labels_out = np.asarray([])
    for i, batch in enumerate(data_loader):
        # print('batch {}'.format(i))
        # Forward pass
        with torch.no_grad():
            images = batch[0]
            labels = batch[1]
            images = images.to(device)
            labels = labels.to(device)
            labels_out = np.append(labels_out, labels.data.numpy())
            outputs = model(images, True).data.numpy()
            conv_out = outputs if i == 0 else np.append(conv_out, outputs, axis=0)
    np.save(output_file + '/conv', conv_out)
    np.save(output_file + '/labels', labels_out)


if __name__ == '__main__':
    # training dataset_iter
    create_train()
    # create_train_split()
    # validation dataset_iter
    create_val()
    # create_val_split()
    # print('path_to_folder: {}, CSV_FILE_TRAIN_TRAINSET: {}, OUT_DIR: {}'.format(path_to_folder,CSV_FILE_TRAIN_TRAINSET,OUT_DIR))
    # data_loader = load_my_data(path_to_folder, CSV_FILE_TRAIN_TRAINSET,drop_last=False)
    # model = Model()
    # create_conv(data_loader,device,model,OUT_DIR)
